function check_skill(initial_value, modifier)
    local roll = rnd(10)

    if not modifier then
        modifier = 0
    end
    
    if cfg.debug then
        pn(txtu("I: "..initial_value.." | M: "..modifier.." | R: "..roll))
    end

    if roll <= initial_value + modifier or roll == 1 then
        return 1
    else
        return nil
    end
end

function check_strength(modifier)
    return check_skill( me().skills.strength, modifier )
end

function check_markmanship(modifier)
    return check_skill( me().skills.markmanship, modifier )
end

function check_dexterity(modifier)
    return check_skill( me().skills.dexterity, modifier )
end

function check_intellect(modifier)
    return check_skill( me().skills.intellect, modifier )
end

function check_willpower(modifier)
    return check_skill( me().skills.willpower, modifier )
end

function check_lore(modifier)
    return check_skill( me().skills.lore, modifier )
end

function check_luck(modifier)
    return check_skill( me().skills.luck, modifier )
end

function injured(object, amount)
    object.trauma = object.trauma + amount
end

function stunned(object, amount)
    object.stun = object.stun + amount
end

function is_killed(object)
    if cfg.debug then
        pn(txtu("OBJ: "..stead.tostring(object).." | Trauma: "..object.trauma..' | Health: '..object.health))
    end
    return object.trauma >= object.health
end




function place_random(object, room_list)
    local count = 0
    for _ in pairs(room_list) do
        count = count + 1
    end
    local room = room_list[rnd(count)]
    place(object, room)
    
    if cfg.debug then
        pn(txtu("Object: "..object.nam.." placed to "..room))
    end

end

function rprint(array)
    local count = 0
    for _ in pairs(array) do
        count = count + 1
    end
    p( array[rnd(count)] )
end