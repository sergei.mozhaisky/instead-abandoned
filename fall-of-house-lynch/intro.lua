function player_setup(role)

me().role = role

-- TODO: start inventory for players

if role == 'detective' then
    me().id = 'Детектив'
    me().fullname = 'Меня зовут Джо Даймонд. Я частный детектив, владелец собственного агентства.'
    me().photo = 'gfx/pl_detective.jpg'

    me().health = 10
    me().sanity = 10
    me().skillpoints = 3

    if rnd(2) == 1 then
        me().skills.strength = 6
        me().skills.markmanship = 6
        me().skills.dexterity = 6
    else
        me().skills.strength = 5
        me().skills.markmanship = 6
        me().skills.dexterity = 5
    end

    if rnd(2) == 1 then
        me().skills.intellect = 5
        me().skills.willpower = 7
        me().skills.lore = 1
        me().skills.luck = 2
    else
        me().skills.intellect = 6
        me().skills.willpower = 5
        me().skills.lore = 3
        me().skills.luck = 3
    end

elseif role == 'gangster' then 
    me().id = 'Гангстер'
    me().fullname = 'Меня зовут Майкл МакГленн. Я гангстер, глава одного из подпольных синдикатов.'
    me().photo = 'gfx/pl_gangster.jpg'

    me().health = 14
    me().sanity = 6
    me().skillpoints = 3

    if rnd(2) == 1 then
        me().skills.strength = 8
        me().skills.markmanship = 7
        me().skills.dexterity = 4
    else
        me().skills.strength = 7
        me().skills.markmanship = 7
        me().skills.dexterity = 2
    end

    if rnd(2) == 1 then
        me().skills.intellect = 3
        me().skills.willpower = 7
        me().skills.lore = 1
        me().skills.luck = 2
    else
        me().skills.intellect = 2
        me().skills.willpower = 8
        me().skills.lore = 1
        me().skills.luck = 4
    end

elseif role == 'professor' then
    me().id = 'Профессор'
    me().fullname = 'Меня зовут Гарри Уолтерс. Я профессор Мискатоникского университета.'
    me().photo = 'gfx/pl_professor.jpg'

    me().health = 6
    me().sanity = 14
    me().skillpoints = 4

    if rnd(2) == 1 then
        me().skills.strength = 4
        me().skills.markmanship = 4
        me().skills.dexterity = 3
    else
        me().skills.strength = 3
        me().skills.markmanship = 3
        me().skills.dexterity = 2
    end

    if rnd(2) == 1 then
        me().skills.intellect = 7
        me().skills.willpower = 6
        me().skills.lore = 7
        me().skills.luck = 3
    else
        me().skills.intellect = 6
        me().skills.willpower = 5
        me().skills.lore = 8
        me().skills.luck = 4
    end
end

me().trauma = 0
me().horror = 0

end


intro_dialog = dlg {
    hideinv = true,
    nam = 'Падение дома Линчей',
    pic = 'gfx/locations/office.jpg',
    entered = function(s)
        p([[
        Офис в сигаретном дыме. Шкафы вдоль стен и куча журналов,
        торчащие из-под дешевого деревянного стола. За столом сидит
        пухлый лысый человек, и он сильно нервничает.^
        Как только я вошел, он тут же обратился ко мне с вопросом:^
        - Так значит, вы и есть тот самый...^
        Я нетерпеливо перебил его:
        ]])
    end,
    dsc = nil,
    phr = {
    {
        always = true, 
        "Джо Даймонд, частный детектив. Давайте к делу.^", 
        function(s)
            player_setup('detective')
            p([[
            - Да, конечно. Я наслышан о вашей репутации - вы расследуете
            особые случаи, которые большинство людей сочли бы 
            сверхъестественной ерундой. И это дело - оно... скажем так,
            весьма и весьма загадочно. Вас уж точно заинтересует.
            ]]);
            pjump('case')
        end
    },
    {
        always = true, 
        "Да, я гангстер Майкл МакГленн. Не тяни резину.^",
        function(s)
            player_setup('gangster')
            p [[
            - Да уж, вы умеете решать проблемы, причем весьма радикальными методами.
            А наша проблема не только сложная, но еще и крайне загадочна. Я знаю, вы
            интересуетесь подобными случаями с момента пропажи вашего друга Луи.
            ]];
            pjump('case')
        end
    },
    {
        always = true, 
        "Гарри Уолтерс, профессор. Я к вашим услугам.^",
        function(s)
            player_setup('professor')
            p [[
            - Прошу вас, проходите. Вы весьма известны в нашем городе как
            специалист по сверхъестественным случаям, темным ритуалам и
            загадочным артефактам. Именно поэтому я пригласил вас
            как консультанта, поскольку наше дело, скажем так, весьма странное.
            ]];
            pjump('case')
        end
    },
    { tag = 'case' },
    {
        always = true, 
        "Итак, зачем же вы меня пригласили?",
        function(s)
            p [[
            - Мне нужна ваша помощь в поиске нашего пропавшего партнера по бизнесу.^ 
            Порывшись в ящиках, хозяин офиса бросает на стол грязную желто-коричневую папку.^
            - Нашего пропавшего зовут Уолтер Линч. Он всегда был мозгом в деловых операциях,
            без него наше коммерческое предприятие просто развалится. Уолтер перестал
            отвечать на письма, и все наши попытки его найти были...^
            Человек за столом сделал паузу, тщательно подбирая слова.^
            – ...неудачными.^
            Он смотрит на папку.^
            - Теперь у вас есть все, что мы знаем. Верните его нам, на карту поставлено
            больше, чем вы думаете.
            ]];
            pjump('finish')
        end
    },
    { tag = 'finish' },
    {
        always = true, 
        "Надеюсь, смогу вам помочь.",
        function(s)
            p [[
            Я взял со стола папку и вышел из офиса.^
            На улице лил дождь, и я с облегчением вздохнул, усевшись в свой автомобиль.
            Теперь можно изучить содержимое папки.
            ]]
            walk('intro_car')
        end
    }
    }, -- end phr
}




intro_folder_diary = obj {
    nam = "Дневник",
    dsc = function (s,w)
        p[[На сиденье рядом со мной разложены бумаги из папки: вырванная из дневника {страница}, ]]
    end,
    act = function (s,w)
        p[[
        «8 июня, 1918. Почти год с тех пор, как болезнь пришла за моим сыном.
        Врачи удивляются изменениям его плоти. Я человек действия, и не могу сидеть
        сложа руки и ждать, когда положение моего наследника ухудшится.^
        Девять месяцев я провел, путешествуя по миру в поисках лекарства. От глубин 
        джунглей и до мудростей Востока. Я ничего не нашел. Я продолжал поиски, все
        углубляясь и углубляясь в потерянные места в мире и за его пределами.
        Я был свидетелем многих тайн и ужасов. Однако найти лекарство мне не удавалось.^
        Я не знал, что было уже слишком поздно. Дома, вдали от меня, мой сын покинул этот мир, и рядом не было отца, чтобы утешить его.^
        Я вернулся домой к моей жене Эдит, намереваясь навсегда остаться. Жизнь медленно
        возвращалась в норму, и я вернулся к работе. Я начал новый бизнес, мое состояние росло,
        но страдания моей души остались прежними. Это было нелегко, но забота об Эдит 
        помогала мне не обращать внимания на собственную боль.»^
        На обороте листа вторая запись:^
        «21 января, 1919. Как это могло случиться? Почему это случилось?^
        Я пытался защитить себя и свою семью, но ничего не смог сделать.^
        О, Эдит, ты все, что у меня есть. Как это могло случиться с тобой?^
        Я ДОЛЖЕН копать глубже. Я обращусь к камням, к которым не осмелился прикоснуться. Я НАЙДУ ответы!»
        ]]
    end
}

intro_folder_map = obj {
    nam = "Карта",
    dsc = function (s,w)
        p[[{помятый лист бумаги} с каким-то рисунком, ]]
    end,
    act = function (s,w)
        p[[На листке была примитивная карта - такое впечатление, что ее рисовал ребенок.
        Карта указывала на особняк недалеко от Аркхэма в штате Массачусетс.]]
        path_prologue:enable()
    end
}

intro_folder_photo1 = obj {
    nam = "Фото 1",
    dsc = function (s,w)
        p[[{фотография} какого-то здания ]]
    end,
    act = function (s,w)
        p[[Фото.]]
    end
}

intro_folder_photo2 = obj {
    nam = "Фото 2",
    dsc = function (s,w)
        p[[и {семейное фото}.]]
    end,
    act = function (s,w)
        p[[Фото.]]
    end
}

intro_folder = obj {
    nam = "Папка",
    dsc = function (s,w)
        p[[Я держу в руках грязную желто-коричневую {папку}.]]
    end,
    act = function (s,w)
        p[[Я раскрыл папку и выложил на сиденье её содержимое. В ней оказался листок
        бумаги с чертежом, обрывок какого-то дневника и пара фотографий.]]
        intro_folder_map:enable()
        intro_folder_diary:enable()
        intro_folder_photo1:enable()
        intro_folder_photo2:enable()
        intro_folder:disable()
    end
}

path_prologue = wroom("Отправиться в особняк^", 'prologue')

intro_car = room {
    nam = "Падение дома Линчей",
    pic = 'gfx/locations/car.jpg',
    dsc = function(s, w)

    end,
    obj = {
        intro_folder,
        intro_folder_diary:disable(),
        intro_folder_map:disable(),
        intro_folder_photo1:disable(),
        intro_folder_photo2:disable(),
    },
    way = {
        path_prologue:disable(),
    },
}

prologue = room {
    forcedsc = true,
    hideinv = true,
    nam = "Особняк Линчей",
    pic = 'gfx/locations/mansion.jpg',
    forcedsc = true,
    dsc = function(s, w)
        p([[Мелкий дождь капал мне на лицо, когда я вышел из своего автомобиля перед зданием.
           Дом огромный и заброшенный, все окутано жуткой тишиной.
           Я постучал в дверь, но ответа не было. Через пару секунд дверь со скрипом открылась сама.]])
    end,
    obj = {
        obj { nam = '', dsc=[[{Я вошел в фойе огромного особняка.}]], act=code[[walk(loc_foyer)]] },
    },
    exit = function(s,w)
        init_story()
        take(investigator)
        --take(status)

        -- TODO: remove this
        take(axe)
        player_setup('gangster')
    end
}