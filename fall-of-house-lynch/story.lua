function init_story()
if cfg.debug then
    pn("A"..story.a.." | B"..story.b.." | C"..story.c)
end

local rooms = {'loc_dinner', 'loc_entrance', 'loc_hallway', 'loc_cellar', 'loc_foyer'}

if story.b==1 then
    place(cutscene_1a_trigger, 'loc_basement')
    lock_room(loc_basement, magical_lock)
    place(axe, 'loc_garden')
    lock_room(loc_garden, jammed_door)
    place_random(lantern, rooms)
elseif story.b==2 then
    place(lantern, 'loc_basement')
    lock_room(loc_basement, jammed_door)
    place(cutscene_1b_trigger, 'loc_garden')
    lock_room(loc_garden, magical_lock)
    place_random(axe, rooms)
end

if story.c==1 then
    place(evidence, 'loc_guestbedroom')
    place(cutscene_2b_trigger, 'loc_masterbedroom')
    lock_room(loc_masterbedroom, locked_door)
    place(magic_phrase, 'loc_masterbedroom')
    place(firefighter, 'loc_operatingroom')
    place(shotgun, 'loc_freezer')
    place(elder_sign, 'loc_storage')
    place(cutscene_3a_trigger, 'loc_kitchen')
elseif story.c==2 then
    place(cutscene_2a_trigger, 'loc_guestbedroom')
    lock_room(loc_guestbedroom, locked_door)
    place(evidence, 'loc_masterbedroom')
    place(cutscene_3b_trigger, 'loc_operatingroom')
    place(elder_sign, 'loc_freezer')
    place(shotgun, 'loc_storage')
    place(firefighter, 'loc_kitchen')
end

-- TODO: put to random room
place_random(sedative, rooms)
place_random(whiskey, rooms)

end

cutscene_3a_trigger = obj {
    nam = "Пепел на кухонном столе",
    dsc = function (s,w)
        p[[На кухонном столе {странные следы от сажи}.]]
    end,
    act = function (s,w)
        if visited(cutscene_3a) then
            p([[Следы от сажи напоминают контуры человеческого тела.]])
        else
            walk(cutscene_3a)
        end
    end,
}

cutscene_3a = room {
    forcedsc = true,
    hideinv = true,
    nam = "",
    pic = "gfx/story.png",
    dsc = function(s, w)
        story.clue3 = 1
        p([[Сожженный человеческий контур
           на кухонном столе. При близком рассмотрении вы находите серебряный
           ключ, скрытый в саже. Почерневшие следы ведут от лестницы фойе до холла.]])
        place(silver_key, 'loc_kitchen')
    end,
    obj = {
        obj { nam = '', dsc=[[{Продолжить}]], act=code[[walkback()]] },
    },
};

cutscene_3b_trigger = obj {
    nam = "Тело в операционной",
    dsc = function (s,w)
        p[[На операционном столе лежит {обезображенное тело}.]]
    end,
    act = function (s,w)
        if visited(cutscene_3b) then
            p([[Я не могу больше смотреть на это жуткое зрелище.]])
        else
            walk(cutscene_3b)
        end
    end,
}

cutscene_3b = room {
    forcedsc = true,
    hideinv = true,
    nam = "",
    pic = "gfx/story.png",
    dsc = function(s, w)
        story.clue3 = 1
        p([[Кровавое месиво человеческого тела лежит на операционном столе.
           Ржавые инструменты разбросаны вокруг него. Человек без кожи шепчет
           вам в ужасе своим безгубым ртом: «О Боже, он хотел мою плоть…»
           На своем последнем вздохе он шепчет «Он позволил мне остаться на
           ночь» и протягивает вам ключ.]])
        place(silver_key, 'loc_operatingroom')
    end,
    obj = {
        obj { nam = '', dsc=[[{Продолжить}]], act=code[[walkback()]] },
    },
};


cutscene_2a_trigger = obj {
    nam = "Дневник",
    dsc = function (s,w)
        p[[На столике возле кровати лежит {дневник}.]]
    end,
    act = function (s,w)
        if visited(cutscene_2a) then
            p([[Запись в дневнике от 5 июля 1918. «Состояние моей жены стало ухудшаться
               в последнее время. Я знаю, что это всего лишь вопрос времени, и приготовил место
               для нее вместе с другими телами глубоко в земле».]])
        else
            walk(cutscene_2a)
        end
    end,
}

cutscene_2a = room {
    forcedsc = true,
    hideinv = true,
    nam = "",
    pic = "gfx/story.png",
    dsc = function(s, w)
        story.clue2 = 1
        p([[Вы нашли запись в дневнике от 5 июля 1918. «Состояние моей жены стало ухудшаться
               в последнее время. Я знаю, что это всего лишь вопрос времени, и приготовил место
               для нее вместе с другими телами глубоко в земле».]])
    end,
    obj = {
        obj { nam = '', dsc=[[{Продолжить}]], act=code[[walkback()]] },
    },
};


cutscene_2b_trigger = obj {
    nam = "Лопата",
    dsc = function (s,w)
        p[[Рядом со шкафом стоит {лопата} с налипшими комьями земли.]]
    end,
    act = function (s,w)
        if visited(cutscene_2b) then
            p([[Не хочу ее трогать -- воспоминания о пережитом видении слишком свежи.]])
        else
            walk(cutscene_2b)
        end
    end,
}


cutscene_2b = room {
    forcedsc = true,
    hideinv = true,
    nam = "",
    pic = "gfx/story.png",
    dsc = function(s, w)
        story.clue2 = 1
        p([[Ваше зрение становится размытым, и вы падаете на землю, в то время как вас
               посещает видение. Человек свистит веселый мотив и тащит обмякшее тело женщины
               по полу. Вы дрожите, как тело, вытащенное под дождь через грязные лужи. Лопата
               опускается на голову, и видение заканчивается.]])
    end,
    obj = {
        obj { nam = '', dsc=[[{Продолжить}]], act=code[[walkback()]] },
    },
};

cutscene_1a_trigger = obj {
    nam = "Тело",
    dsc = function (s,w)
        p[[На полу подвала {куча земли}.]]
    end,
    act = function (s,w)
        if visited(cutscene_1) then
            p([[Похоже, что Эдит Линч похоронена здесь.]])
        else
            walk(cutscene_1)
        end
    end,
}

cutscene_1b_trigger = obj {
    nam = "Тело",
    dsc = function (s,w)
        p[[В саду {могила}.]]
    end,
    act = function (s,w)
        if visited(cutscene_1) then
            p([[Похоже, что Эдит Линч похоронена здесь.]])
        else
            walk(cutscene_1)
        end
    end,
}


cutscene_1 = room {
    forcedsc = true,
    hideinv = true,
    nam = "",
    pic = "gfx/story.png",
    dsc = function(s, w)
        story.clue1 = 1
        if story.a == 1 then
            p([[Вас посещает видение Уолтера, ведущего сеанс общения с его мертвым сыном.
               Вы видите, как незваный дух обвивает его ум, приказывая «Накорми меня душами!»]])
            p([[Вы слышите топот бегущих ног и маниакальный смех. Уолтер Линч где-то рядом,
              и совершенно ясно, что нет смысла тратить время на переговоры с ним. Его нужно
              остановить, пока он не продолжил убивать.]])
                -- Откройте цель. Хранитель размещает Уолтера на расстоянии не меньше 2 клеток
                -- от каждого сыщика. Уолтер представлен фигурой маньяка, имеющего +4 здоровья за сыщика
        elseif story.a == 2 then
            p([[Где-то в доме Уолтер пытается использовать свою плоть и кровь, чтобы призвать
               древнего. Земля трясется, книги и различные безделушки падают с полок.]])
            p([[Кровь стынет в ваших жилах, когда вы наконец узнаете о том, что Уолтер собирается
              призвать какое-то чудовищное существо. Вы не можете позволить этому произойти.
              Если Уолтер призовет нечто из Ада, то ваша личная цель вернуть это обратно, или погибнуть пытаясь.]])
                -- Откройте цель. Затем оглушите каждого шоггота в игре.
        elseif story.a == 3 then
            pn([[Голос Уолтера разлетается по всему особняку: «Мы должны накормить его!».
               Болезненные стоны заполняют поместье. Это почти, но не совсем, человеческий звук.]])
            p([[Мертвые бродят по коридорам этого проклятого дома, и с нарастающим ужасом вы
              понимаете, что вас заманили сюда, чтобы накормить этих отвратительных чудовищ.
              Совершенно ясно, что Уолтеру Линчу уже не помочь, и остается только бежать.
              Стены сада высокие, и другие выходы из дома наверняка перекрыты, я в этом уверен.
              Единственный шанс выбраться отсюда - идти тем же путем, которым я вошел.]])
            -- Откройте цель. Хранитель размещает 1 зомби за каждого исследователя в разных пустых комнатах.
        end
    end,
    obj = {
        obj { nam = '', dsc=[[{Продолжить}]], act=code[[walkback()]] },
    },
};


function incr_action()
    story.action_count = story.action_count + 1
end

function incr_time()
    story.time_count = story.time_count + 1
end


function room_enter_trigger(s,f)
    if cfg.debug then
        pn(txtu('walk: '..stead.tostring(s)..' <= '..stead.tostring(f)..' | '..story.action_count))
    end
    if door_is_locked(s,f) then
        return false
    end
    incr_action()
    if story.action_count > 2 then
        keeper_action()
        story.action_count = 0
        incr_time()
        if story.time_count > 2 then
            play_timed_event()
            story.time_count = 0
        end
    end
    return true
end

function play_timed_event()
    if story.timed_event == 1 then
        walk('timed_event_1')
    elseif story.timed_event == 2 then
        walk('timed_event_2')
    elseif story.timed_event == 3 then
        walk('timed_event_3')
    elseif story.timed_event == 4 then
        walk('timed_event_4')
    else
        walk('timed_event_5')
    end
    story.timed_event = story.timed_event + 1
end

function keeper_action()
    if cfg.debug then
        pn(txtu('KEEPER ACTION'))
    end
end


function door_is_locked(s,f)
    if s.is_locked then
        p('Дверь '..s.where..' заперта.')
        place(s.lock_type,f)
        return true
    end
    return false
end

function lock_room(room,lock)
    if cfg.debug then
        pn(txtu('Lock '..room.nam..' with '..lock.nam))
    end
    room.is_locked = 1
    room.lock_type = lock
    lock.room = room
end

function unlock_room(room,lock,key)
    room.is_locked = nil
    remove(lock)
    if key then
        remove(key, inv())
    end
end