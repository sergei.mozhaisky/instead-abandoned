investigator = obj {
    var {
        is_weapon = 1,
        is_onetime = 0,
        weapon_type = 'unarmed',
        damage = 0,
    },
    nam = "Investigator",
    disp = function (s,w)
        pn(img(me().photo))
        pn(me().id)
    end,
    inv = function (s,w)
        p(me().fullname)
    end,
    use = function(s,w)
        investigator_use(s,w)
    end,
    used = function(s,w)
        investigator_use(s,w)
    end,
}

function investigator_use(s,w)

end



axe = obj {
    -- Острое оружие ближнего боя.
    -- Действие: Атакуй монстра в своей клетке или пройди проверку Силы.
    -- Удача: Автоматическое решение головоломки-замка в твоей комнате.
    var {
        is_weapon = 1,
        is_onetime = 0,
        weapon_type = 'melee',
        weapon_subtype = 'sharp',
        damage = 4,
    },
    nam = "Топор",
    dsc = function (s,w)
        p[[Неподалеку валяется {топор}.]]
    end,
    tak = function (s,w)
        p[[Я поднял топор.]]
    end,
    inv = function (s,w)
        p([[Хорошее оружие для ближнего боя. А еще им удобно сбивать замки.]])
    end
}


elder_sign = obj {
    -- Действие: Пройди проверку Знаний.
    -- Удача: Хранитель должен отодвинуть каждого монстра в твоей комнате на 2 клетки от тебя.
    -- Провал: Получи 1 ужас.
    nam = "Знак Древних",
    dsc = function (s,w)
        p[[Здесь {камень со звездой}.]]
    end,
    tak = function (s,w)
        p[[Я взял странный камень.]]
    end,
    inv = function (s,w)
        p([[Камень с выцарапанной на нем пятиугольной звездой - Знаком Древних.]])
    end
}

firefighter = obj {
    -- Снаряжение. Тупое оружие ближнего боя.
    -- Действие: Атакуй монстра в своей клетке или сбрось эту карту, чтобы сбросить жетон огня из своей комнаты.
    var {
        is_weapon = 1,
        is_onetime = 0,
        weapon_type = 'melee',
        weapon_subtype = 'blunt',
        damage = 2,
    },
    nam = "Огнетушитель",
    dsc = function (s,w)
        p[[На стене висит {огнетушитель}.]]
    end,
    tak = function (s,w)
        p[[Я снял со стены огнетушитель.]]
    end,
    inv = function (s,w)
        p([[Этой штукой можно и пламя потушить, и по голове кому-то бахнуть.]])
    end
}


lantern = obj {
    -- Сыщики могут игнорировать темноту в твоей комнате.
    -- Действие: Сбрось эту карту, чтобы нанести 3 урона монстру на расстоянии до 1 клетки.
    var {
        is_weapon = 1,
        is_onetime = 1,
        weapon_type = "lantern",
        damage = 3,
    },
    nam = "Фонарь",
    dsc = function (s,w)
        p[[На полу стоит керосиновый {фонарь}.]]
    end,
    tak = function (s,w)
        p[[Я взял керосиновый фонарь.]]
    end,
    inv = function (s,w)
        p([[С фонарем как-то поспокойнее.]])
    end
}


magic_phrase = obj {
    -- Сбрось эту карту, когда ты пытаешься пройти через «Магический замок», чтобы сбросить карту замка.
    var {
        is_weapon = 0,
    },
    nam = "Магическая формула",
    dsc = function (s,w)
        p[[{Лист бумаги} с какими-то знаками.]]
    end,
    tak = function (s,w)
        p[[Я взял листок.]]
    end,
    inv = function (s,w)
        p([[Это магическая формула для открытия замка - достаточно прочесть заклинание, и дверь откроется. По крайней мере так написано на листке.]])
    end
}


sedative = obj {
    -- Действие: Сбрось эту карту, чтобы вылечить 1 травму с любого сыщика в твоей клетке
    -- (включая тебя). Он не может двигаться в свой следующий ход.
    var {
        is_weapon = 0,
    },
    nam = "Лекарства",
    dsc = function (s,w)
        p[[Это {чемоданчик с лекарствами}.]]
    end,
    tak = function (s,w)
        p[[Я взял чемоданчик с лекарствами.]]
    end,
    inv = function (s,w)
        p([[Если вдруг меня ранят, я смогу обработать и перевязать рану.]])
    end,
    use = function (s,w)
        if w == investigator then
            if me().trauma == 0 then
                p([[Я совершенно здоров, это сейчас ни к чему.]])
            else
                p([[Я перевязал свои раны.]])
                me().trauma = me().trauma - 1
                remove(s,inv())
            end
        end
    end,
}


shotgun = obj {
    -- Действие: Атакуй монстра на расстоянии до 1 клетки (ты можешь стрелять через дверь).
    -- Нанеси 2 дополнительных урона, если атакуешь монстра в твоей клетке.
    var {
        is_weapon = 1,
        is_onetime = 0,
        weapon_type = 'ranged',
        damage = 4,
    },
    nam = "Дробовик",
    dsc = function (s,w)
        p[[У стены стоит {дробовик}.]]
    end,
    tak = function (s,w)
        p[[Я взял дробовик.]]
    end,
    inv = function (s,w)
        p([[Весьма эффективное оружие дальнего боя.]])
    end
}


silver_key = obj {
    var {
        is_weapon = 0,
    },
    nam = "Серебряный ключ",
    dsc = function (s,w)
        p[[Рядом лежит {серебряный ключ}.]]
    end,
    tak = function (s,w)
        p[[Я взял серебряный ключ со стола.]]
    end,
    inv = function (s,w)
        p([[Ключ от какой-то двери.]])
    end
}


evidence = obj {
    -- Действие: Сбрось эту карту и получи 2 ужаса. Хранитель должен сказать, в какой комнате находится следующая улика.
    var {
        is_weapon = 0,
    },
    nam = "Поразительные доказательства",
    dsc = function (s,w)
        p[[Это {Поразительные Доказательства}.]]
    end,
    tak = function (s,w)
        p[[Я взял Поразительные Доказательства.]]
    end,
    inv = function (s,w)
        p([[Какая-то тетрадь со странными знаками на обложке. Я уверен, что под обложкой скрывается нечто за пределами разума.]])
    end,
    use = function (s,w)
        if w == investigator then
            if story.clue3 == 0 then
                if story.c == 1 then
                    p('Иди на кухню.')
                else
                    p('Иди в операционную.')
                end
                me().horror = me().horror + 2
                remove(s,inv())
            elseif story.clue2 == 0 then
                if story.c == 1 then
                    p('Иди в большую спальню.')
                else
                    p('Иди в гостевую спальню.')
                end
                me().horror = me().horror + 2
                remove(s,inv())
            elseif story.clue1 == 0 then
                if story.b == 1 then
                    p('Иди в подвал.')
                else
                    p('Иди в сад.')
                end
                me().horror = me().horror + 2
                remove(s,inv())
            else
                p('Я уже и так знаю все, что мне нужно.')
            end
        end
    end,
}


whiskey = obj {
    -- Получи +1 к проверкам Силы воли.
    -- Действие: Сбрось эту карту, чтобы вылечить до 2 ужаса.
    var {
        is_weapon = 0,
    },
    nam = "Виски",
    dsc = function (s,w)
        p[[На полу валяется {бутылка виски}.]]
    end,
    tak = function (s,w)
        p[[Я взял виски.]]
    end,
    inv = function (s,w)
        p([[В самый раз, чтобы поправить пошатнувшийся рассудок!]])
    end,
    use = function (s,w)
        if w == investigator then
            if me().horror == 0 then
                p([[Пока что я сохраняю ясный рассудок, это ни к чему.]])
            else
                p([[Я открыл бутылку и выпил. Вскоре я почувствовал, что ужас отступает.]])
                me().horror = me().horror - 2
                if me().horror < 0 then
                    me().horror = 0
                end
            end
            remove(s,inv())
        end
    end,
}


-- Locks

locked_door = obj {
    var {
        room = nil,
    },
    nam = "Закрытая дверь",
    dsc = function (s,w)
        p([[^{Дверь} ]]..s.room.where..[[ закрыта на ключ.]])
    end,
    act = function (s,w)
        p[[Нужен ключ, чтобы ее открыть.]]
    end,
    used = function(s,w)
        if w == silver_key then
            p("Я вставил серебряный ключ в замок и повернул его. Раздался щелчок, и дверь открылась.")
            unlock_room(s.room,s,w)
        elseif w == axe then
            if check_strength() then
                p([[Я размахнулся и ударил топором по двери. Щепки полетели в разные стороны. Мне удалось попасть в замок,
                  и дверь, жалобно скрипнув, приоткрылась.]])
                unlock_room(s.room,s)
            else
                rprint({
                [[Я размахнулся и попытался ударить топором по двери. К сожалению, я промахнулся, и топор вонзился в пол.]],
                [[Я выхватил топор и ударил им по двери. Увы, моих сил не хватило на то, чтобы хоть как-то повредить дверь.]]
                })
            end
            incr_action()
        end
    end
}

magical_lock = obj {
    var {
        room = nil,
    },
    nam = "Магический замок",
    dsc = function (s,w)
        p([[^На {двери} ]]..s.room.where..[[ нарисованы какие-то руны.]])
    end,
    act = function (s,w)
        p[[Похоже, что дверь можно открыть с помощью какого-то заклинания.]]
    end,
    used = function(s,w)
        if w == magic_phrase then
            p([[Я достал листок с заклинанием и громко прочел его. Сначала ничего не происходило, но затем руны на двери
              вспыхнули ярким светом и пропали. Похоже, мне удалось открыть дверь.]])
            unlock_room(s.room,s,w)
        elseif w == axe then
            if check_strength() then
                p([[Я размахнулся и несколько раз что есть силы ударил топором по двери. Через некоторое время в двери
                  образовался пролом, войти через который не составит никакого труда.]])
                unlock_room(s.room,s)
            else
                rprint({
                [[Я размахнулся и попытался ударить топором по двери. К сожалению, я промахнулся, и топор вонзился в пол.]],
                [[Я выхватил топор и ударил им по двери. Увы, моих сил не хватило на то, чтобы хоть как-то повредить дверь.]]
                })
            end
            incr_action()
        end
    end
}

jammed_door = obj {
    var {
        room = nil,
    },
    nam = "Заклинившая дверь",
    dsc = function (s,w)
        p([[^{Дверь} ]]..s.room.where..[[ заклинило.]])
    end,
    act = function (s,w)
        if check_strength() then
            p[[Я навалился всем телом на дверь, пытаясь ее открыть. Но мне удалось приоткрыть ее настолько, чтобы я смог пройти.]]
            unlock_room(s.room,s)
        else
            p[[Я со всей силы навалился на дверь. Бесполезно - она не сдвинулась даже на миллиметр.]]
        end
        incr_action()
    end,
    used = function(s,w)
        if w == axe then
            if check_strength() then
                p([[Я вставил топор в дверную щель и, действуя им как рычагом, приоткрыл дверь. Похоже, теперь я смогу пролезть.]])
                unlock_room(s.room,s)
            else
                rprint({
                [[Вставив топор в дверную щель, я что есть сил налег на него, пытаясь открыть дверь. Безрезультатно.]],
                [[Я вставил топор в дверную щель и попытался открыть дверь, действуя топором как рычагом. Увы, моих сил не хватило,
                чтобы приоткрыть дверь хоть на миллиметр.]]
                })
            end
            incr_action()
        end
    end
}