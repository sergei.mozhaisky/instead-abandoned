-- $Name:Падение дома Линчей X$
-- $Version:1.0$
-- $Author:Сергей Можайский$
-- $E-mail:sergei.mozhaisky@gmail.com$
instead_version "1.9.0"
game.codepage="UTF-8";
require "xact"
require "para"
require "dash"
require "wroom"

dofile "functions.lua"
dofile "house.lua"
dofile "objects.lua"
dofile "story.lua"
dofile "intro.lua"
dofile "events.lua"
dofile "battle.lua"

-- ---------------------------------------------------------

function start()
    stead.phrase_prefix = '-- '
end

global {
    cfg = {
        debug = 1,
    },
    story = {
        a = rnd(3),
        b = rnd(2),
        c = rnd(2),
        clue1 = 0,
        clue2 = 0,
        clue3 = 0,
        action_count = 0,
        time_count = 0,
        timed_event = 1,
    }
}


pl = player {
    nam = "Investigator",
    where = 'main',
    var {
        id = '',
        fullname = '',
        photo = '',
        role = '',
        health = 1,
        sanity = 1,
        skillpoints = 1,
        horror = 0,
        trauma = 0,
        stun = 0,
        skills = {
            strength = 1,
            markmanship = 1,
            dexterity = 1,
            intellect = 1,
            willpower = 1,
            lore = 1,
            luck = 1,
        },
    },
    obj = { },
}

status = stat {
    nam = 'статус';
    disp = function(s)
        for i=1,(me().health-me().trauma) do
            p(imgl('gfx/body.png'))
        end
        pn ''
        for i=1,(me().sanity-me().horror) do
            p(imgl('gfx/mind.png'))
        end
        pn ''
    end
}


main = room {
    forcedsc = true,
    hideinv = true,
    nam = "Падение дома Линчей",
    pic = "gfx/logo.png",
    dsc = nil,
    obj = {
        obj { nam = '', dsc=[[{НАЧАТЬ ИГРУ}^^]], act=code[[walk(intro_dialog)]] },
        obj { nam = '', dsc=[[{быстрый старт}^^]], act=code[[walk(prologue)]] },
        obj { nam = '', dsc=[[{проверка боя}^^]], act=code[[init_story();take(investigator);take(status);player_setup('professor');take(axe);take(firefighter);take(shotgun);walk(test_battle_room)]] },
    
        obj { nam = '', dsc=[[{Об игре} ^]], act=code[[walk(game_details)]] },
    },
}



game_details = room {
    forcedsc = true,
    nam = "Об игре",
    pic = "gfx/logo.png",
    dsc = [[
        TODO: Информация об игре
        ^
        Можайский Сергей ^
        http://technix.in.ua/
    ]],
    obj = {
        obj { nam = '', dsc=[[{Назад}]], act=code[[walk(main)]] },
    },
}



