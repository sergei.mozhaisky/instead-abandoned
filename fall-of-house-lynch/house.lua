
loc_foyer = room {
    var {
        is_locked = nil,
        lock_type = nil,
        is_dark = nil,
        is_fire = nil,
        where = 'в фойе',
    },
    forcedsc = true,
    nam = "Фойе",
    pic = "gfx/locations/foyer.png",
    dsc = function(s, w)
        if visits() < 2 then
            if story.c==1 then
                pn(txtem([[Войдя в дом, я почувствовал слабый запах горелой пищи.
                Может быть, кто-то забыл выключить плиту?]]))
            elseif story.c==2 then
                pn(txtem([[Хотя дом старый, он не издает ни единого звука. Такое ощущение, что дом,
               затаив дыхание, ждет, когда случится что-то ужасное. Тишину вдруг разорвал
               душераздирающий крик, эхо донеслось со стороны лаборатории. Дом трясется,
               как будто началось землетрясение, и пыльные картины падают со стен.]]))
            end
        end
        p[[Двери справа и слева. Лестница ведет на второй этаж. Под лестницей видна небольшая дверь.]]
    end,
    way = {
        wroom("Дверь справа", "Столовая", 'loc_dinner'),
        wroom("Дверь слева", "Лаборатория", 'loc_laboratory'),
        wroom("Вверх по лестнице", "Коридор второго этажа", 'loc_hallway'),
        wroom("Дверь под лестницей", "Вход в подвал", 'loc_downstairs'),
    },
    enter = function(s,w)
        return room_enter_trigger(s,w)
    end,
}



loc_laboratory = room {
    var {
        is_locked = nil,
        lock_type = nil,
        is_dark = nil,
        is_fire = nil,
        where = 'в лабораторию',
    },
    forcedsc = true,
    nam = "Лаборатория",
    pic = "gfx/locations/laboratory.jpg",
    dsc = function(s, w)
        p[[Впереди белая дверь.]]
    end,
    obj = {

    },
    way = {
        wroom("Фойе", 'loc_foyer'),
        wroom("Белая дверь", "Операционная", 'loc_operatingroom'),
    },
    enter = function(s,w)
        return room_enter_trigger(s,w)
    end,
}



loc_operatingroom = room {
    var {
        is_locked = nil,
        lock_type = nil,
        is_dark = nil,
        is_fire = nil,
        where = 'в операционную',
    },
    forcedsc = true,
    nam = "Операционная",
    pic = "gfx/locations/operating_room.jpg",
    dsc = function(s, w)
        p[[Слева металлическая дверь. Справа деревянная дверь, покрытая узором.]]
    end,
    obj = {

    },
    way = {
        wroom("Лаборатория", 'loc_laboratory'),
        wroom("Металлическая дверь", "Холодильник", 'loc_freezer'),
        wroom("Дверь с узором", "Веранда", 'loc_entrance'),
    },
    enter = function(s,w)
        return room_enter_trigger(s,w)
    end
}


loc_freezer = room {
    var {
        is_locked = nil,
        lock_type = nil,
        is_dark = nil,
        is_fire = nil,
        where = 'в холодильник',
    },
    forcedsc = true,
    nam = "Холодильник",
    pic = "gfx/locations/freezer.jpg",
    dsc = function(s, w)

    end,
    obj = {

    },
    way = {
        wroom("Операционная", 'loc_operatingroom'),
    },
    enter = function(s,w)
        return room_enter_trigger(s,w)
    end,
}


loc_entrance = room {
    var {
        is_locked = nil,
        lock_type = nil,
        is_dark = nil,
        is_fire = nil,
        where = 'на веранду',
    },
    forcedsc = true,
    nam = "Веранда",
    pic = "gfx/room.png",
    dsc = function(s, w)
        p[[Впереди дверь в сад.]]
    end,
    obj = {

    },
    way = {
        wroom("Операционная", 'loc_operatingroom'),
        wroom("Сад", 'loc_garden'),
    },
    enter = function(s,w)
        return room_enter_trigger(s,w)
    end,
}

loc_garden = room {
    var {
        is_locked = nil,
        lock_type = nil,
        is_dark = nil,
        is_fire = nil,
        where = 'в сад',
    },
    forcedsc = true,
    nam = "Сад",
    pic = "gfx/locations/garden.jpg",
    dsc = function(s, w)

    end,
    obj = {

    },
    way = {
        wroom("Веранда", 'loc_entrance'),
    },
    enter = function(s,w)
        return room_enter_trigger(s,w)
    end,
}



loc_dinner = room {
    var {
        is_locked = nil,
        lock_type = nil,
        is_dark = nil,
        is_fire = nil,
        where = 'в столовую',
    },
    forcedsc = true,
    nam = "Столовая",
    pic = "gfx/locations/dinner.jpg",
    dsc = function(s, w)
        p[[В конце комнаты - массивная дверь.]]
    end,
    obj = {

    },
    way = {
        wroom("Фойе", 'loc_foyer'),
        wroom("Массивная дверь", "Кухня", 'loc_kitchen'),
    },
    enter = function(s,w)
        return room_enter_trigger(s,w)
    end,
}

loc_kitchen = room {
    var {
        is_locked = nil,
        lock_type = nil,
        is_dark = nil,
        is_fire = nil,
        where = 'на кухню',
    },
    forcedsc = true,
    nam = "Кухня",
    pic = "gfx/locations/kitchen.png",
    dsc = function(s, w)
        p[[Справа - небольшая дверь.]]
    end,
    obj = {

    },
    way = {
        wroom("Столовая", 'loc_dinner'),
        wroom("Небольшая дверь", "Кладовка", 'loc_storage'),
    },
    enter = function(s,w)
        return room_enter_trigger(s,w)
    end,
}

loc_storage = room {
    var {
        is_locked = nil,
        lock_type = nil,
        is_dark = nil,
        is_fire = nil,
        where = 'в кладовую',
    },
    forcedsc = true,
    nam = "Кладовая",
    pic = "gfx/locations/storage.jpg",
    dsc = function(s, w)

    end,
    obj = {

    },
    way = {
        wroom("Кухня", 'loc_kitchen'),
    },
    enter = function(s,w)
        return room_enter_trigger(s,w)
    end,
}



loc_downstairs = room {
    var {
        is_locked = nil,
        lock_type = nil,
        is_dark = nil,
        is_fire = nil,
        where = 'входа в подвал',
    },
    forcedsc = true,
    nam = "Лестница",
    pic = "gfx/locations/downstairs.jpg",
    dsc = function(s, w)

    end,
    obj = {

    },
    way = {
        wroom("Фойе", 'loc_foyer'),
        wroom("Подвал", 'loc_basement'),
    },
    enter = function(s,w)
        return room_enter_trigger(s,w)
    end,
}

loc_basement = room {
    var {
        is_locked = nil,
        lock_type = nil,
        is_dark = nil,
        is_fire = nil,
        where = 'в подвал',
    },
    forcedsc = true,
    nam = "Подвал",
    pic = "gfx/locations/basement.jpg",
    dsc = function(s, w)

    end,
    obj = {

    },
    way = {
        wroom("Лестница", 'loc_downstairs'),
        wroom("Погреб", 'loc_cellar'),
    },
    enter = function(s,w)
        return room_enter_trigger(s,w)
    end,
}

loc_cellar = room {
    var {
        is_locked = nil,
        lock_type = nil,
        is_dark = nil,
        is_fire = nil,
        where = 'в погреб',
    },
    forcedsc = true,
    nam = "Погреб",
    pic = "gfx/locations/cellar.jpg",
    dsc = function(s, w)

    end,
    obj = {

    },
    way = {
        wroom("Подвал", 'loc_basement'),
        wroom("Незаметная дверца","Церемониальный зал", 'loc_ceremonyroom'),
    },
    enter = function(s,w)
        return room_enter_trigger(s,w)
    end,
}

loc_ceremonyroom = room {
    var {
        is_locked = nil,
        lock_type = nil,
        is_dark = nil,
        is_fire = nil,
    },
    forcedsc = true,
    nam = "Церемониальный зал",
    pic = "gfx/room.png",
    dsc = function(s, w)

    end,
    obj = {

    },
    way = {
        wroom("Погреб", 'loc_cellar'),
    },
    enter = function(s,w)
        return room_enter_trigger(s,w)
    end,
}



loc_hallway = room {
    var {
        is_locked = nil,
        lock_type = nil,
        is_dark = nil,
        is_fire = nil,
        where = 'в коридор',
    },
    forcedsc = true,
    nam = "Коридор второго этажа",
    pic = "gfx/locations/hallway.jpg",
    dsc = function(s, w)
        p[[Здесь две спальни.]]
    end,
    obj = {

    },
    way = {
        wroom("Фойе", 'loc_foyer'),
        wroom("Дверь в спальню", "Спальня для гостей", 'loc_guestbedroom'),
        wroom("Дверь в спальню", "Большая спальня", 'loc_masterbedroom'),
    },
    enter = function(s,w)
        room_enter_trigger(s,w)
    end,
}

loc_guestbedroom = room {
    var {
        is_locked = nil,
        lock_type = nil,
        is_dark = nil,
        is_fire = nil,
        where = 'в спальню для гостей',
    },
    forcedsc = true,
    nam = "Спальня для гостей",
    pic = "gfx/locations/guest_bedroom.jpg",
    dsc = function(s, w)

    end,
    obj = {

    },
    way = {
        wroom("Коридор", 'loc_hallway'),
    },
    enter = function(s,w)
        return room_enter_trigger(s,w)
    end,
}

loc_masterbedroom = room {
    var {
        is_locked = nil,
        lock_type = nil,
        is_dark = nil,
        is_fire = nil,
        where = 'в большую спальню',
    },
    forcedsc = true,
    nam = "Большая спальня",
    pic = "gfx/locations/master_bedroom.jpg",
    dsc = function(s, w)

    end,
    obj = {

    },
    way = {
        wroom("Коридор", 'loc_hallway'),
    },
    enter = function(s,w)
        return room_enter_trigger(s,w)
    end,
}