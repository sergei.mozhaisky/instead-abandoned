function attack_eldrich(monster,weapon)
    local attack = 0
    if weapon.weapon_type == 'unarmed' then
        -- ###  UNARMED  #####################
        attack = rnd(4)
        if attack == 1 then
            p([[Ты наматываешь щупальце создания себе на руку и со всех сил дергаешь на себя.]])
            if check_strength(-1) then
                p([[С ужасным чавканьем ты выдергиваешь щупальце из монстра.]])
                injured(monster, 2)
            else
                p([[Монстр кидает тебя через всю комнату.]])
                injured(me(), 1)
            end
        elseif attack == 2 then
            p([[Своей рукой ты пробиваешь мягкую кожу монстра и ищешь какой-нибудь жизненно-важный орган в теле монстра.]])
            if check_willpower() then
                p([[вои пальцы нащупывают что-то, и ты сжимаешь ладонь.]])
                injured(monster,1)
            else
                p([[Скользкие внутренности твари обжигают твою плоть.]])
                injured(me(), 1)
                horrored(1)
            end
        elseif attack == 3 then
            p([[Ты бросаешь в монстра все, что попадается под руку.]])
            --  TODO: Если у тебя нет карт Снаряжения, ты автоматически проваливаешь атаку.
            if check_luck(3) then
                p([[Предметы застревают в отвратительном монстре.]])
                injured(monster,2)
                -- TODO: drop equipment
            else
                p([[Такое ощущение, что тварь смеется.]])
                -- no effect
            end
        elseif attack == 4 then
            p([[Ты пытаешься сломить монстра ударами своих кулаков.]])
            if check_strength() then
                p([[Существо отступает от тебя под градом ударов.]])
                injured(monster,1)
                -- TODO: Ты можешь передвинуть монстра на 1 клетку.
            else
                p([[Твои кулаки бессильны против его шкуры.]])
                injured(me(), 1)
            end
        else
            p('Ошибка! Атака unarmed = '..attack)
            return false
        end
    elseif weapon.weapon_type == 'ranged' then
        -- ###  RANGED  #####################
        attack = rnd(8)
        if attack == 1 then
            p([[Ты кричишь, выпуская в приближающегося монстра пулю за пулей.]])
            if check_markmanship() then
                p([[Твои выстрелы замедляют монстра.]])
                injured(monster, weapon.damage - 1 )
                stunned(monster,1)
            else
                p([[Монстр остается невредим.]])
                -- no effect
            end
        elseif attack == 2 then
            p([[Ты целишься туда, где тебе кажется, у монстра уязвимое место.]])
            if check_markmanship() then
                p([[Ты вгоняешь пулю туда, что более всего напоминает его глаз.]])
                injured(monster, weapon.damage)
                stunned(monster, 1)
            else
                p([[Такое ощущение, что он неуязвим!]])
                horrored(1)
            end
        elseif attack == 3 then
            p([[Ты бесстрашно стоишь и выпускаешь в монстра одну пулю за другой.]])
            if check_markmanship() then
                p([[Тварь реагирует на каждый выстрел, попадающий в цель, криком боли.]])
                injured(monster, weapon.damage + 1)
            else
                p([[Существо даже не замечает, что по нему ведется огонь.]])
                horrored(1)
            end
        elseif attack == 4 then
            p([[Ты целишься в керосиновую лампу, стоящую неподалеку от монстра.]])
            if check_markmanship() then
                p([[Взрыв освещает комнату, поджаривая монстра.]])
                injured(monster, weapon.damage)
                -- TODO: Помести жетон Пожара в эту комнату.
            else
                p([[Твоя пуля застревает в стене.]])
                -- no effect
            end
        elseif attack == 5 then
            p([[Ты припоминаешь, что где-то в древнем томе натыкался на описание монстра и его слабостей.]])
            if check_intellect() then
                p([[Твои воспоминания не подводят тебя! Существо извивается от выстрела, направленного в уязвимое место.]])
                injured(monster, weapon.damage + 1 )
            else
                p([[Ты пытаешься что-то вспомнить, но память подводит тебя.]])
                -- no effect
            end
        elseif attack == 6 then
            p([[Ты закрываешь глаза и стреляешь в приближающегося монстра.]])
            if check_luck(1) then
                p([[Открыв глаза, ты с удивлением обнаруживаешь, что выстрел был более чем удачен.]])
                injured(monster, weapon.damage + 1)
            else
                p([[Открыв глаза, ты видишь, что монстр остается невредим.]])
                horrored(1)
            end
        elseif attack == 7 then
            p([[Монструозная тварь скользит к тебе.]])
            if check_markmanship() then
                p([[Ты быстро стреляешь, и монстр орошает комнату слизью.]])
                injured(monster, weapon.damage - 1)
            else
                p([[Осечка! Я отбросил бесполезное оружие в сторону.]])
                drop(weapon, here().battle_room)
            end
        elseif attack == 8 then
            p([[Ты поливаешь врага шквалом пуль.]])
            if check_markmanship() then
                p([[Тебе удалось нашинковать тварь свинцом.]])
                injured(monster, weapon.damage)
            else
                p([[Твои пули отскакивают от шкуры монстра и рикошетят по всей комнате.]])
                injured(me(),1)
            end
        else
            p('Ошибка! Атака ranged = '..attack)
            return false
        end
    elseif weapon.weapon_type == 'melee' then
        -- ###  MELEE  #####################
        attack = rnd(6)
        if attack == 1 then
            p([[Щупальца монстра окружают тебя, но ты пытаешься отбиваться. ]])
            if check_strength() then
                p([[Каждый твой удар наносит монстру рану.]])
                injured(monster, weapon.damage + 2)
            else
                p([[Ты измотан, но так ничего и не добился.]])
                horrored(1)
            end
        elseif attack == 2 then
            p([[Ты ударяешь по самой большой части монстра.]])
            if check_strength() then
                p([[Твой удар отбрасывает тварь в сторону.]])
                injured(monster, 2)
            else
                p([[Орудие рикошетит от монстра и бьет тебя.]])
                injured(me(),1)
            end
        elseif attack == 3 then
            p([[Ты наносишь град ударов по монстру так быстро как можешь.]])
            if check_dexterity() then
                p([[Скорость и напор атаки возымели действие.]])
                injured(monster, weapon.damage + 1 )
            else
                p([[Ты так спешишь, что оружие выскальзывает у тебя из рук.]])
                drop(weapon, here().battle_room)
            end
        elseif attack == 4 then
            p([[Ты стараешься сломать один из отростков монстра.]])
            if check_strength() then
                p([[Твое оружие размалывает его в кашу.]])
                injured(monster, weapon.damage)
            else
                p([[Оружие застревает в коже врага и выскальзывает у тебя из рук.]])
                drop(weapon, here().battle_room)
            end
        elseif attack == 5 then
            if weapon.weapon_subtype == 'blunt' then
                p([[Ты заносишь оружие над головой и бьешь по монстру сверху.]])
                if check_strength() then
                    p([[Удар раскалывает жесткую оболочку создания.]])
                    injured(monster, weapon.damage)
                else
                    p([[Противник предугадывает удар.]])
                    injured(monster, 1)
                end
            elseif weapon.weapon_subtype == 'sharp' then
                p([[Ты делаешь выпад, надеясь вогнать лезвие оружия в плоть врага.]])
                if check_dexterity() then
                    p([[Лезвие пробивает его тело, высвобождая поток слизи.]])
                    injured(monster, weapon.damage + 1)
                    horrored(1)
                else
                    p([[Лезвие оружия не может пробить его кожу.]])
                    injured(me(), 1 )
                end
            end
        elseif attack == 6 then
            if weapon.weapon_subtype == 'blunt' then
                p([[Ты подпрыгиваешь и опускаешь оружие на монстра.]])
                if check_strength() then
                    p([[Оружие под твоим весом проделывает большую дыру в монстре.]])
                    injured(monster, weapon.damage + 1)
                else
                    p([[Ты промахиваешься и неаккуратно приземляешься на пол.]])
                    stunned(me(), 1)
                end
            elseif weapon.weapon_subtype == 'sharp' then
                p([[Щупальце направляется к твоему лицу, и ты планируешь отрезать его.]])
                if check_dexterity() then
                    p([[Щупальце падает на пол и уползает в щель.]])
                    injured(monster, weapon.damage - 1)
                else
                    p([[Он сжимает твой череп.]])
                    injured(me(), 1 )
                end
            end
        else
            p('Ошибка! Атака melee = '..attack)
            return false
        end
    elseif weapon.weapon_type == 'lantern' then
        p([[Я бросил фонарь во врага.]])
        injured(monster, 3)
        remove(weapon,inv())
    else
        p("Это не оружие!")
        return false
    end
    return true
end


function response_eldrich( monster )
    local attack = rnd(12)
    if attack == 1 then
        p([[Монстр направляется к тебе со скоростью локомотива.]])
        if check_dexterity() then
            p([[Ты быстро уходишь с его пути.]])
            -- no effect
        else
            p([[Со всей силы он врезается в тебя.]])
            injured(me(), monster.damage )
            --TODO: Хранитель может передвинуть тебя на 1 клетку.
        end
    elseif attack == 2 then
        p([[С удивительной скоростью, тварь старается навалиться на тебя всем своим весом.]])
        if check_dexterity() then
            p([[К своему облегчению ты избегаешь атаки.]])
            -- no effect
        else
            p([[Монстр подминает тебя под своей тушей. ]])
            injured(me(), monster.damage)
            stunned(me(),1)
        end
    elseif attack == 3 then
        p([[Монстр атакует, используя щупальце в качестве кнута.]])
        if check_dexterity() then
            p([[Ты уклоняешься от этой атаки.]])
            -- no effect
        else
            p([[Атака подбрасывает тебя в воздух.]])
            injured(me(), 2 )
            --TODO: Хранитель может передвинуть тебя на 1 клетку.
        end
    elseif attack == 4 then
        p([[Монстр обрушивается на землю, комната трясется, и ты падаешь на пол. ]])
        injured(me(), 1)
    elseif attack == 5 then
        p([[Ужасная тварь движется к тебе.]])
        if check_strength() then
            p([[Ты отскакиваешь в сторону, задевая монстра.]])
            horrored(1)
        else
            p([[Щупальце обвивается вокруг твоей ноги.]])
            horrored(1)
            injured(me(), monster.damage )
        end
    elseif attack == 6 then
        p([[Чудовищная тварь хватает тебя по рукам и ногам своими скользкими щупальцами.]])
        if check_dexterity() then
            p([[Ты освобождаешься из хватки.]])
            -- no effect
        else
            p([[Он бросает тебя на пол.]])
            injured(me(),1)
            stunned(me(),1)
        end
    elseif attack == 7 then
        p([[Невыразимый ужас протягивает свои щупальца к тебе и сжимает тебя в кольцо.]])
        if check_strength() then
            p([[У тебя хватает силы, чтобы оттолкнуть создание от себя.]])
            injured(me(),1)
        else
            p([[Ты чувствуешь, как ломаются твои ребра.]])
            injured(me(),3)
            stunned(me(),1)
        end
    elseif attack == 8 then
        p([[Монстр пытается проникнуть в твой разум.]])
        if check_willpower(-1) then
            p([[Ты остаешься в здравом уме и изгоняешь монстра из своей головы.]])
            -- TODO: Ты можешь передвинуть монстра на 1 клетку.
        else
            p([[Ты корчишься в экстазе, пока монстр пытается проглотить тебя.]])
            injured(me(), monster.damage )
        end
    else 
        -- TODO: special attack
        p([[Специальная атака монстра.]])
    end
end