function attack_human(monster,weapon)
    local attack = 0
    if weapon.weapon_type == 'unarmed' then
        -- ###  UNARMED  #####################
        attack = rnd(5)
        if attack == 1 then
            p([[Ты вступаешь в бой с ]].. monster.mnam.wi ..[[ и хватаешь его за голову.]])
            if check_dexterity() then
                p([[Его шея поворачивается с отвратительным щелчком.]])
                injured(monster, math.ceil( me().skills.strength / 2 ) )
            else
                p([[Он избегает твоего захвата.]])
                -- no effect
            end
        elseif attack == 2 then
            p([[Ты издаешь отчаянный вопль и бросаешься на противника.]])
            if check_strength() then
                p([[Ты наносишь сокрушающий удар, который опрокидывает противника на землю.]])
                injured(monster,3)
            else
                p([[Ты промахиваешься и неудачно падаешь на землю. Ты оглушен.]])
                stunned(me(),1)
            end
        elseif attack == 3 then
            p([[С разворота ты бьешь противника по ногам.]])
            if check_strength() then
                p([[Противник падает на землю, хватаясь за колени. Он оглушен]])
                injured(monster,1)
                stunned(monster,1)
            else
                p([[Ты чувствуешь, как твой удар пришелся на стену.]])
                injured(me(),1)
            end
        elseif attack == 4 then
            p([[Ты целишься в правую скулу врага.]])
            if check_strength() then
                p([[Твой кулак попадает точно в цель.]])
                injured(monster,1)
            else
                p([[Ты сложил слишком много силы в удар и, промахнувшись, потерял равновесие.]])
                -- no effect
            end
        elseif attack == 5 then
            p([[В паническом припадке ты набрасываешься на врага с голыми руками.]])
            if check_strength() then
                p([[Ты целишься в глаза монстра, и твой удар находит цель. Монстр оглушен.]])
                injured(monster,2)
                stunned(monster,1)
            else
                p([[Он смеется, вытирая кровь с лица.]])
                horrored(1)
            end
        else
            p('Ошибка! Атака unarmed = '..attack)
            return false
        end
    elseif weapon.weapon_type == 'ranged' then
        -- ###  RANGED  #####################
        attack = rnd(10)
        if attack == 1 then
            p([[Ты ловишь монстра в прицел.]])
            if check_markmanship() then
                p([[Выстрел пробивает ему легкое.]])
                injured(monster, weapon.damage + 2 )
            else
                p([[Ты попадаешь монстру в плечо, привлекая его внимание.]])
                injured(monster,1)
                -- TODO: Хранитель может передвинуть любого монстра на расстояние до 2х клеток
            end
        elseif attack == 2 then
            p([[Решая не останавливаться, ты делаешь выстрел.]])
            if check_dexterity() then
                p([[Выстрел попадает в цель.]])
                injured(monster,2)
                -- TODO: ты можешь передвинуться на 1 клетку.
            else
                p([[Ты спотыкаешься, и пуля летит мимо цели.]])
                injured(me(),1)
            end
        elseif attack == 3 then
            p([[Ты быстро выхватываешь оружие и стреляешь.]])
            if check_markmanship() then
                p([[Твои инстинкты тебя не подводят.]])
                injured(monster, weapon.damage )
            else
                p([[Пуля даже рядом с монстром не пролетела.]])
                -- no effect
            end
        elseif attack == 4 then
            p([[Ты целишься монстру прямо в сердце.]])
            if check_markmanship(-1) then
                p([[Его грудная клетка взрывается брызгами крови.]])
                injured(monster, weapon.damage + 3 )
            else
                p([[Твой выстрел попадает в плечо монстра.]])
                injured(monster,1)
            end
        elseif attack == 5 then
            p([[Ты слышишь, как оружие выстрелило, но к своему удивлению не помнишь, как нажал на курок.]])
            if check_markmanship() then
                p([[Твоя реакция не подводит тебя.]])
                injured(monster, weapon.damage - 1 )
            else
                p([[Ты выстрелил в землю.]])
                -- no effect
            end
        elseif attack == 6 then
            p([[Ты целишься в монстра.]])
            if check_markmanship() then
                p([[Выстрел подбрасывает монстра в воздух, отбрасывая его в дальний конец комнаты.]])
                injured(monster, weapon.damage )
            else
                p([[Каким-то невообразимым образом все пули проходят мимо цели, а монстр нападает на тебя.]])
                injured(me(),1)
            end
        elseif attack == 7 then
            p([[Ты аккуратно целишься и стреляешь.]])
            if check_markmanship() then
                p([[Верхняя часть черепа монстра полностью снесена.]])
                injured(monster, weapon.damage + 2 )
            else
                p([[Оружие ослепляет тебя своей вспышкой. Ты оглушен.]])
                stunned(me(),1)
            end
        elseif attack == 8 then
            p([[На чистом инстинкте и адреналине ты выхватываешь оружие и стреляешь.]])
            if check_markmanship() then
                p([[Монстр кричит, и пуля вонзается ему в ногу.]])
                injured(monster, weapon.damage - 1 )
                stunned(monster,1)
            else
                p([[Порох обжигает твою кожу.]])
                injured(me(),1)
                drop(weapon, here().battle_room)
            end
        elseif attack == 9 then
            p([[Прямо перед тем, как ты жмешь на курок, твой противник начинает кричать и устремляется к тебе.]])
            if check_willpower() then
                p([[Ты хладнокровно стреляешь в грудь монстра.]])
                injured(monster, weapon.damage )
            else
                p([[Он сбивает тебя с толку, и ты промахиваешься. ]])
                -- no effect
            end
        elseif attack == 10 then
            p([[Пока ты взводишь курок, твои руки нервно дрожат.]])
            if check_dexterity() then
                p([[Ты успокаиваешься и уверенно делаешь выстрел.]])
                injured(monster, weapon.damage - 1 )
            else
                p([[Оружие падает на землю, прежде чем ты успеваешь выстрелить.]])
                drop(weapon, here().battle_room)
            end
        else
            p('Ошибка! Атака ranged = '..attack)
            return false
        end
    elseif weapon.weapon_type == 'melee' then
        -- ###  MELEE  #####################
        attack = rnd(8)
        if attack == 1 then
            p([[На секунду запаниковав, ты кидаешь свое оружие в соперника.]])
            if check_markmanship() then
                p([[Меткий бросок направляет оружие прямо между глаз врага.]])
                injured(monster, weapon.damage )
            else
                p([[Оружие пролетает прямо над головой противника.]])
            end
            drop(weapon, here().battle_room)
        elseif attack == 2 then
            p([[Ты бежишь на врага, целясь своим оружием ему в голову.]])
            if check_strength(-1) then
                p([[Ты кричишь в тот момент, когда твое оружие обезглавливает соперника.]])
                injured(monster, weapon.damage + 4 )
            else
                p([[Он уклоняется от атаки и хватает тебя за ногу.]])
                injured(me(),1)
            end
        elseif attack == 3 then
            p([[Ты получаешь заряд мужества и набрасываешься на монстра.]])
            if check_strength() then
                p([[Оружие застревает глубоко в черепе врага.]])
                injured(monster, weapon.damage + 1 )
            else
                p([[Он отпрыгивает в последний момент, и твой удар уходит в пустоту.]])
                -- no effect
            end
        elseif attack == 4 then
            p([[Ты размахиваешься и направляешь оружие прямо в грудь оппонента.]])
            if check_strength() then
                p([[Он хрипит и извивается от боли.]])
                injured(monster, weapon.damage + 1 )
            else
                p([[Он перехватывает оружие и бросает его на пол.]])
                drop(weapon, here().battle_room)
            end
        elseif attack == 5 then
            p([[Ты выхватываешь оружие и втыкаешь его в грудь монстра.]])
            if check_strength() then
                p([[Твой противник отступает, ошарашенный атакой.]])
                injured(monster, 2 )
            else
                p([[Твоя слабая атака делает тебя уязвимым.]])
                injured(me(),1)
            end
        elseif attack == 6 then
            p([[Ты приседаешь, целясь по вражеским коленям.]])
            if check_markmanship() then
                p([[Враг падает на пол, сбитый твоим ударом.]])
                injured(monster, weapon.damage )
                stunned(monster,1)
            else
                p([[Враг успевает вовремя подпрыгнуть.]])
                -- no effect
            end
        elseif attack == 7 then
            if weapon.weapon_subtype == 'blunt' then
                p([[Ты стискиваешь зубы, занося оружие над головой.]])
                if check_strength(-1) then
                    p([[Оружие болезненно бьет врага по голове.]])
                    injured(monster, weapon.damage + 2 )
                    stunned(monster,1)
                else
                    p([[Противник стоически выдерживает удар.]])
                    stunned(monster,1)
                end
            elseif weapon.weapon_subtype == 'sharp' then
                p([[Ты заносишь оружие над головой и идешь на убийство. ]])
                if check_dexterity() then
                    p([[Твое оружие рассекает артерию и из нее вырывается фонтан крови.]])
                    injured(monster, weapon.damage + 2 )
                    horrored(1)
                else
                    p([[Монстр блокирует удар своей рукой.]])
                    injured(monster, 1 )
                end
            end
        elseif attack == 8 then
            if weapon.weapon_subtype == 'blunt' then
                p([[Ты приготовился без раздумий ударить врага. ]])
                if check_strength() then
                    p([[Твой удар вырубает монстра.]])
                    injured(monster, weapon.damage )
                    stunned(monster,1)
                else
                    p([[Твои потуги почти не увенчались успехом.]])
                    injured(monster, 1 )
                end
            elseif weapon.weapon_subtype == 'sharp' then
                p([[Ты направляешь оружие на запястье врага.]])
                if check_dexterity() then
                    p([[Ты начисто отрубаешь одну из его рук.]])
                    injured(monster, weapon.damage )
                    stunned(monster,1)
                else
                    p([[Он вовремя убирает руку из-под удара.]])
                    -- no effect
                end
            end
        else
            p('Ошибка! Атака melee = '..attack)
            return false
        end
    elseif weapon.weapon_type == 'lantern' then
        p([[Я бросил фонарь во врага.]])
        injured(monster, 3)
        remove(weapon,inv())
    else
        p("Это не оружие!")
        return false
    end
    return true
end


function response_human( monster )
    local attack = rnd(15)
    if attack == 1 then
        p([[Враг пытается прижать тебя к полу.]])
        if check_strength() then
            p([[Сопротивляясь, ты попадаешь локтем по горлу противника.]])
            stunned(monster,1)
        else
            p([[Твое сопротивление подавлено.]])
            injured(me(), monster.damage )
        end
    elseif attack == 2 then
        p([[Без чувства самосохранения, враг кидается на тебя.]])
        if check_dexterity() then
            p([[Тебе удается не только избежать атаки, но и ранить монстра.]])
            injured(monster, 1 )
        else
            p([[Ты ранишь своего противника, но сам получаешь удар гораздо сильнее. ]])
            injured(monster, 1 )
            injured(me(), 2 )
        end
    elseif attack == 3 then
        p([[Монстр опускает оба кулака на твою голову.]])
        if check_dexterity() then
            p([[Ты едва успеваешь увернуться.]])
            -- no effect
        else
            p([[Его кулаки наносят сокрушающий удар.]])
            injured(me(), monster.damage )
        end
    elseif attack == 4 then
        p([[Монстр медленно приближается к тебе с безумным выражением лица.]])
        if check_strength() then
            p([[Ты опрокидываешь монстра на пол.]])
            -- no effect
        else
            p([[Его кулаки наносят сокрушающий удар.]])
            injured(me(), monster.damage )
        end
    elseif attack == 5 then
        p([[Монстр набрасывается на тебя, выкрикивая какой-то бред.]])
        if check_strength() then
            p([[С усилием ты отталкиваешь монстра.]])
            -- no effect
        else
            p([[Он подавляет сопротивление и начинает грызть твое лицо.]])
            horrored(1)
            injured(me(), monster.damage )
        end
    elseif attack == 6 then
        p([[Твой противник пытается вонзить свои зубы тебе в плечо.]])
        if check_dexterity() then
            p([[Ты выскальзываешь из его хватки.]])
            -- no effect
        else
            p([[Он кусает тебя, и боль пронзает твое плечо.]])
            injured(me(), monster.damage )
        end
    elseif attack == 7 then
        p([[Противник движется с ужасающей скоростью, угадывая каждое твое движение.]])
        if check_dexterity() then
            p([[Ты делаешь непредсказуемое движение и уходишь от атаки.]])
            -- no effect
        else
            p([[Как может быть, что он знает, что у меня в голове?]])
            injured(me(), monster.damage )
        end
    elseif attack == 8 then
        p([[Монстр тянется к твоему горлу.]])
        if check_dexterity() then
            p([[Ты успешно уходишь от захвата.]])
            -- no effect
        else
            p([[Его пальцы обвивают твое горло.]])
            injured(me(), 2 )
            stunned(me(),1)
        end
    elseif attack == 9 then
        p([[Враг делает дикий выпад в сторону твоего лица.]])
        if check_dexterity() then
            p([[Ты падаешь на пол, избегая атаки монстра.]])
            -- no effect
        else
            p([[Он хватает твое лицо и пытается выцарапать твои глаза.]])
            injured(me(), 2 )
            stunned(me(),1)
        end
    elseif attack == 10 then
        p([[Враг наваливается на тебя.]])
        if check_dexterity() then
            p([[Ты отступаешь от атаки.]])
            -- no effect
        else
            p([[Тебя положили на лопатки.]])
            injured(me(), 1 )
            stunned(me(),1)
        end
    else 
        -- TODO: special attack
        p([[Специальная атака монстра.]])
    end
end