investigator = obj {
    var {
        is_weapon = 1,
        is_onetime = 0,
        weapon_type = 'unarmed',
        damage = 0,
    },
    nam = "Investigator",
    disp = function (s,w)
        pn(img(me().photo))
        pn(txtb(me().id))
    end,
    inv = function (s,w)
        local health = (me().health - me().trauma)*100/me().health
        if health > 75 then
            p("Я в порядке. ")
        elseif health > 50 then
            p("Я слегка ранен. ")
        elseif health > 25 then
            p("Я тяжело ранен. ")
        else
            p("Я очень тяжело ранен. ")
        end
        local sanity = (me().sanity - me().horror)*100/me().sanity
        if sanity > 75 then
            p("Мой рассудок в норме.")
        elseif health > 50 then
            p("Голова кружится.")
        elseif health > 25 then
            p("Остатки рассудка медленно покидают меня.")
        else
            p("Я на грани сумасшествия.")
        end
    end,
    use = function(s,w)
        investigator_use(s,w)
    end,
    used = function(s,w)
        investigator_use(s,w)
    end,
}

function investigator_use(s,w)

end


function player_setup(role)

me().role = role

-- TODO: start inventory for players

if role == 'detective' then
    me().id = 'Детектив'
    me().fullname = 'Меня зовут Джо Даймонд. Я частный детектив, владелец собственного агентства.'
    me().photo = 'gfx/pl_detective.jpg'

    me().health = 10
    me().sanity = 10
    me().skillpoints = 3

    if rnd(2) == 1 then
        me().skills.strength = 6
        me().skills.markmanship = 6
        me().skills.dexterity = 6
    else
        me().skills.strength = 5
        me().skills.markmanship = 6
        me().skills.dexterity = 5
    end

    if rnd(2) == 1 then
        me().skills.intellect = 5
        me().skills.willpower = 7
        me().skills.lore = 1
        me().skills.luck = 2
    else
        me().skills.intellect = 6
        me().skills.willpower = 5
        me().skills.lore = 3
        me().skills.luck = 3
    end

elseif role == 'gangster' then 
    me().id = 'Гангстер'
    me().fullname = 'Меня зовут Майкл МакГленн. Я гангстер, глава одного из подпольных синдикатов.'
    me().photo = 'gfx/pl_gangster.jpg'

    me().health = 14
    me().sanity = 6
    me().skillpoints = 3

    if rnd(2) == 1 then
        me().skills.strength = 8
        me().skills.markmanship = 7
        me().skills.dexterity = 4
    else
        me().skills.strength = 7
        me().skills.markmanship = 7
        me().skills.dexterity = 2
    end

    if rnd(2) == 1 then
        me().skills.intellect = 3
        me().skills.willpower = 7
        me().skills.lore = 1
        me().skills.luck = 2
    else
        me().skills.intellect = 2
        me().skills.willpower = 8
        me().skills.lore = 1
        me().skills.luck = 4
    end

elseif role == 'professor' then
    me().id = 'Профессор'
    me().fullname = 'Меня зовут Гарри Уолтерс. Я профессор Мискатоникского университета.'
    me().photo = 'gfx/pl_professor.jpg'

    me().health = 6
    me().sanity = 14
    me().skillpoints = 4

    if rnd(2) == 1 then
        me().skills.strength = 4
        me().skills.markmanship = 4
        me().skills.dexterity = 3
    else
        me().skills.strength = 3
        me().skills.markmanship = 3
        me().skills.dexterity = 2
    end

    if rnd(2) == 1 then
        me().skills.intellect = 7
        me().skills.willpower = 6
        me().skills.lore = 7
        me().skills.luck = 3
    else
        me().skills.intellect = 6
        me().skills.willpower = 5
        me().skills.lore = 8
        me().skills.luck = 4
    end
end

me().trauma = 0
me().horror = 0

end