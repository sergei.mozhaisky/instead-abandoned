battle = room {
    var {
        battle_room = '',
    },
    forcedsc = true,
    nam = "Бой",
    pic = "gfx/battle.jpg",
    dsc = function(s, w)

    end,
    obj = {},
    entered = function(s, f)
        s.battle_room = f
    end,
    exit = function(s, f)
        objs():zap()
        s.battle_room = ''
    end,
}


function battle_player_attack(monster,weapon)
    if monster.mtype == 'human' then
        if not attack_human( monster, weapon ) then
            return false
        end
    elseif monster.mtype == 'beast' then
        -- TODO: not implemented
        -- attack_beast( monster,weapon ) 
    elseif monster.mtype == 'eldrich' then
        if not attack_eldrich( monster,weapon ) then
            return false
        end
    end
    
    if battle_health_check(monster) then
        return true
    end
    return false
end

function battle_monster_attack(monster)
    if monster.mtype == 'human' then
        response_human( monster )
    elseif monster.mtype == 'beast' then
        -- TODO: not implemented
        -- response_beast( monster )
    elseif monster.mtype == 'eldrich' then
        response_eldrich( monster )
    end
    
    if battle_health_check(monster) then
        return true
    end
    return false
end


function battle_health_check(monster)
    if is_killed(me()) then
        walk('death_battle_defeat') -- player 'killed'
        return false
    end
    if is_killed(monster) then
        p( monster.msg.killed )
        remove(monster)     -- kill monster and remove from scene
        objs(here().battle_room):del('monster_token') -- TODO: delete any monster token
        walkout()           -- TODO: add object with link to previous scene
        return false
    end
    return true
end

function monster_activity(s,w)
    if w == "monster_try_evade" then
        if check_dexterity(s.monster_ref.awareness) then
            p( s.monster_ref.msg.evade_succeeded )
            -- TODO: enable ways
        else
            p( s.monster_ref.msg.evade_failed )
            battle_monster_attack( s.monster_ref )
            objs(battle):add( s.monster_ref )
            walk('battle')
        end
    elseif w == "monster_try_battle" then
        p( s.monster_ref.msg.battle_started )
        objs(battle):add( s.monster_ref )
        walk('battle')
    end 
end

generic_monster = function(v)
    v.generic_monster = true

    v.nam = 'Типовой монстр'
    
    v.dsc = function (s)
        if s.pic then
            p(imgl(s.pic))
        end
        rprint(s.xdescr)
    end

    v.used = function(monster,weapon)
        debugmsg("Monster: "..stead.tostring(monster).." | Weapon: "..stead.tostring(weapon))
        -- perform attack
        if not battle_player_attack(monster, weapon) then
            return false
        end
    
        if monster.stun > 0 then
            -- monster is stunned and can't attack
            stunned(monster,-1)
        else
            if not battle_monster_attack(monster) then
                return false
            end
            if me().stun > 0 then
                -- player stunned - can't attack, monster attacks twice
                stunned(me(),-1)
                if not battle_monster_attack(monster) then
                    return false
                end
            end
        end
    end
    
    return obj(v)
end


monster_zombie = function(monster_name)
    local v = generic_monster {
        is_monster = 1,     -- для определения объектов - не монстров
        mtype = 'human',    -- тип
        awareness = 1,      -- бдительность
        horror = -1,        -- модификатор уровня ужаса
        damage = 2,         -- урон при атаке
        health = 7,         -- здоровье
        trauma = 0,         -- повреждения
        stun = 0,           -- оглушение
        pic = nil,
        xnam = " Здесь бродит зомби.",
        mnam = {
            ng = 'Зомби', -- Монстр
            n  = 'зомби', -- монстр
            to = 'зомби', -- кому: монстру
            wh = 'зомби', -- кого: монстра
            wi = 'зомби', -- кем: монстром
        },
        xdescr = {
            "{Зомби} готовится к атаке.",
            "Вид {зомби} внушает страх.",
            "{Зомби} сейчас нападет."
        },
        msg = {
            evade_succeeded = "К счастью, зомби не заметил меня.",
            evade_failed = "Я попытался спрятаться, но бесполезно. Зомби напал на меня.",
            killed = "Зомби убит."
        }
    }
    if monster_name then
        v.nam = monster_name
    else
        v.nam = 'Зомби'
    end

    return v
end

monster_maniac = function()
    local v = generic_monster {
        is_monster = 1,     -- для определения объектов - не монстров
        mtype = 'human',    -- тип
        awareness = 0,      -- бдительность
        horror = 1,         -- модификатор уровня ужаса
        damage = 1,         -- урон при атаке
        health = 5,         -- здоровье
        trauma = 0,         -- повреждения
        stun = 0,           -- оглушение
        pic = nil,
        xnam = " Здесь бродит маньяк.",
        mnam = {
            ng = 'Маньяк', -- Монстр
            n  = 'маньяк', -- монстр
            to = 'маньяку', -- кому: монстру
            wh = 'маньяка', -- кого: монстра
            wi = 'маньяком', -- кем: монстром
        },
        xdescr = {
            "{Маньяк} готовится к атаке.",
            "Вид {маньяка} внушает страх.",
            "{Маньяк} сейчас нападет."
        },
        msg = {
            evade_succeeded = "К счастью, маньяк не заметил меня.",
            evade_failed = "Я попытался спрятаться, но бесполезно. Маньяк напал на меня.",
            killed = "Маньяк убит."
        }
    }
    if monster_name then
        v.nam = monster_name
    else
        v.nam = 'Маньяк'
    end

    return v
end

monster_token = function(monster)
    -- local monster = stead.ref(monster_name)
    local v = {}
    stead.add_var(v, { monster_ref = monster, monster_token = true })
    v.nam = monster.nam
    v.obj = {
        vobj("monster_is_here", v.monster_ref.xnam),
        vobj("monster_try_evade", "Что делать - попробовать {скрыться}"),
        vobj("monster_try_battle", "или {напасть}?"),
    }
    v.act = monster_activity
    
    return obj(v)
end




zombie1 = monster_zombie('Зомби1')
token1 = monster_token(zombie1)


death_battle_defeat = room {
    forcedsc = true,
    nam = "Финал",
    pic = nil,
    dsc = function(s, w)
        pn("Я погиб в бою.")
    end,
}






test_battle_room = room {
    var {
        monster_ref = '',
    },
    forcedsc = true,
    nam = "Локация с монстром",
    pic = nil,
    dsc = function(s, w)
        p([[Убогость здешней обстановки поражала. По углам было разбросано грязное
белье, стояла колченогая кровать и уродливый комод, за которым валялась огромных размеров тарелка  с присохшими к  ней  остатками
пищи. В комнате стоял невыносимый гнилостный запах. Внушительные залежи пыли и густая сеть паутины на стенах и потолке
довершали картину отвратительного запустения.]])
    end,
    obj = {

    },
    entered = function(s, f)
        objs():add(token1)
        --s.monster_ref = stead.ref('zombie1')
        --ways():disable_all()
    end,

    exit = function(s, f)
        objs():del(token1)
        --objs():del('monster_is_here')
        --objs():del('monster_try_evade')
        --objs():del('monster_try_battle')
        --ways():enable_all()
    end,
}