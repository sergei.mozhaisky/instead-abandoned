function debugmsg(text)
    if cfg.debug then
        pn(txtb("[ "..text.." ]"))
    end
end


function check_skill(initial_value, modifier)
    local roll = rnd(10)

    if not modifier then
        modifier = 0
    end
    
    debugmsg("I: "..initial_value.." | M: "..modifier.." | R: "..roll)
    
    if roll <= initial_value + modifier or roll == 1 then
        return 1
    else
        return nil
    end
end

function check_strength(modifier)
    return check_skill( me().skills.strength, modifier )
end

function check_markmanship(modifier)
    return check_skill( me().skills.markmanship, modifier )
end

function check_dexterity(modifier)
    return check_skill( me().skills.dexterity, modifier )
end

function check_intellect(modifier)
    return check_skill( me().skills.intellect, modifier )
end

function check_willpower(modifier)
    return check_skill( me().skills.willpower, modifier )
end

function check_lore(modifier)
    return check_skill( me().skills.lore, modifier )
end

function check_luck(modifier)
    return check_skill( me().skills.luck, modifier )
end


function injured(object, amount)
    object.trauma = object.trauma + amount
end

function stunned(object, amount)
    object.stun = object.stun + amount
end

function horrored(amount)
    me().horror = me().horror + amount
end

function is_killed(object)
    debugmsg("OBJ: "..stead.tostring(object).." | Trauma: "..object.trauma..' | Health: '..object.health)
    return object.trauma >= object.health
end

-- increase action counter
function incr_action()
    story.action_count = story.action_count + 1
end

-- increase timed events counter
function incr_time()
    story.time_count = story.time_count + 1
end


-- place object to random room from list
function place_random(object, room_list)
    local count = 0
    for _ in pairs(room_list) do
        count = count + 1
    end
    local room = room_list[rnd(count)]
    place(object, room)
    
    debugmsg("Object: "..object.nam.." placed to "..room)
    
end

-- print random string from array
function rprint(array)
    local count = 0
    for _ in pairs(array) do
        count = count + 1
    end
    p( array[rnd(count)] )
end

-- test for locked door
function door_is_locked(s,f)
    if s.is_locked then
        p('Дверь '..s.where..' заперта.')
        place(s.lock_type,f)
        return true
    end
    return false
end

-- lock defined room with chosen lock
function lock_room(room,lock)
    debugmsg("Lock "..room.nam..' with '..lock.nam)
    room.is_locked = 1
    room.lock_type = lock
    lock.room = room
end

-- unlock defined room (possibly with key)
function unlock_room(room,lock,key)
    room.is_locked = nil
    remove(lock)
    if key then
        remove(key, inv())
    end
end

-- trigger for all rooms in house
function room_enter_trigger(s,f)
    debugmsg('walk: '..stead.tostring(f)..' => '..stead.tostring(s)..' | '..story.action_count)

    if door_is_locked(s,f) then
        return false
    end
    incr_action()
    if story.action_count > 2 then
        keeper_action()
        story.action_count = 0
        incr_time()
        if story.time_count > 2 then
            play_timed_event()
            story.time_count = 0
        end
    end
    return true
end