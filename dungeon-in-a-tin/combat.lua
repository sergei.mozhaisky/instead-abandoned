set_life = function(diff)
  me().life = me().life + diff
end

get_modifier = function(atk_enemy, atk_hero, power)
    local mod = 0;
    if atk_hero == 'Swipe' and atk_enemy == 'Thrust'
    or atk_hero == 'Thrust' and atk_enemy == 'Overhead'
    or atk_hero == 'Overhead' and atk_enemy == 'Swipe'
    then
        mod = power
    end    
    if atk_hero == 'Swipe' and atk_enemy == 'Overhead'
    or atk_hero == 'Thrust' and atk_enemy == 'Swipe'
    or atk_hero == 'Overhead' and atk_enemy == 'Thrust'
    then
        mod = 0 - power
    end
    return mod
end

simulate_combat = function()
  local eid = scene_combat.enemy_id
  local etype = scene_combat.spawned_enemies[eid]
  -- roll dice
  local roll = roll_dice();
  local atk = scene_combat.attack_card;
  local mod = get_modifier(scene_combat.enemy_attack, atk[1], atk[2])
  pn('Roll: ' .. roll)
  pn('Atk: ' .. atk[1] .. '|' .. atk[2])
  pn('Modifier: ' .. mod)
  
  -- orcs have modifier -1
  local enemy_mod = 0
  if etype >= 3 then
    enemy_mod = -1
  end
  

  pn('Enemy mod: ' .. enemy_mod)
  
  local success = false
  if roll == 1 then 
    pn('Меч лишь слегка оцарапал противника.')
    set_life(-1)
  elseif roll == 6 then
    pn('Отличная атака! Враг убит!')
    success = true
  else        
    local result = roll + mod + enemy_mod;
    pn('Result: ' .. result)
    if result >= 4 then
      pn('Враг убит!')
      success = true
    else
      pn('Промах.')
      set_life(-1)
    end
  end

  
  
  if success then
    scene_combat.active_enemies[eid] = 0
  else
    scene_combat.active_enemies[eid] = 2
  end
  
  if me().life == 0 then
    walk(game_over)
  end
  
  -- round is over
  scene_combat.enemies = scene_combat.enemies - 1
  scene_combat.enemy_id = 0
  obj_combat_controls:disable()
  obj_enemies:enable()
end



obj_combat_card = function(card_id)
  local v = {}
  v.combat_card = true
  v._id = card_id
  v.nam = 'combat_card'
  v.dsc = function(s)
    local c = me().combat_cards[s._id]
    if c then
      local mod = get_modifier(scene_combat.enemy_attack, c[1], c[2])
      local modstr = ''
      if mod > 0 then
        modstr = '[+' .. mod .. ']'
      elseif mod < 0 then
        modstr = '[' .. mod .. ']'
      end
      pn('{' .. c[1] .. '} ' .. modstr)
    end
  end
  v.act = function(s)
    scene_combat.attack_card = me().combat_cards[s._id]
    table.remove(me().combat_cards, s._id)
    simulate_combat()
  end
  return obj(v)
end

obj_card1 = obj_combat_card(1)
obj_card2 = obj_combat_card(2)
obj_card3 = obj_combat_card(3)
obj_card4 = obj_combat_card(4)
obj_card5 = obj_combat_card(5)

obj_combat_controls = obj {
   nam = 'combat controls',
   dsc = '',
   obj = {
      obj {
        nam = 'combat_improvise',
        dsc = '^{Импровизировать}^',
        act = function(s) 
          scene_combat.attack_card = trnd(combat_deck)
          simulate_combat()
        end
      },
      obj_card1,
      obj_card2,
      obj_card3,
      obj_card4,
      obj_card5
   }
}

obj_combat_exit = obj {
  nam = 'combat exit',
  dsc = function(s)
    if scene_combat.enemies == 0 then
      pn('^^{Бой окончен.}')  
    end
  end,
  act = function(s)
    walk(scene_combat.return_to)
  end
}

obj_combat_descr = obj {
  nam = 'combat descr',
  dsc = function(s)
    -- monster is alive and attacking
    if scene_combat.enemy_id ~= 0 then
      local attack_descr = ''
      if scene_combat.enemy_attack == 'Swipe' then
        attack_descr = trnd({'замахнулся мечом.', 'размахнулся для удара мечом.', 'отвел меч для рубящего удара.'});
      elseif scene_combat.enemy_attack == 'Thrust' then
        attack_descr = trnd({'бросается на меня с мечом.', 'целится мечом мне в грудь.', 'направил острие меча на меня.'});
      elseif scene_combat.enemy_attack == 'Overhead' then      
        attack_descr = trnd({'занес меч над головой.', 'собирается рубануть меня мечом сверху.', 'схватил обоими руками меч и занес над головой.'});
      end
      pn('^^' .. scene_combat.enemy_name .. ' ' .. attack_descr)
    end
  end
}


obj_enemy_npc = function(eid)
  local v = {}
  v.obj_enemy_npc = true
  v.nam = 'enemy_npc'
  v._id = eid
  v._dscr = function(s)
    local dscr = ''
    local s_e = scene_combat.spawned_enemies
    if #s_e then
      local etype = s_e[s._id]
      if etype == 1 or etype == 2 then
        dscr = 'Гоблин'
      elseif etype == 3 or etype == 4 then
        dscr = 'Орк'
      end
    end
    return dscr
  end
  v.dsc = function(s)
    local dscr = s._dscr(s)
    if dscr ~= '' then
      if scene_combat.active_enemies[s._id] == 1 then
        p('{' .. dscr .. '} ___ ')
      elseif scene_combat.active_enemies[s._id] == 0 then
       p(' [###] ___ ')
      else
        p('' .. dscr .. ' ___ ')
      end
    end
  end
  v.act = function(s)
    scene_combat.enemy_id = s._id
    scene_combat.enemy_attack = trnd(attacks)
    scene_combat.enemy_name = s._dscr(s)
    obj_combat_controls:enable()
    obj_enemies:disable()
    p(' ')
  end
  return obj(v)
end

obj_enemies = obj {
nam = 'obj_enemies',
obj = {
 obj_enemy_npc(1),
 obj_enemy_npc(2),
 obj_enemy_npc(3),
 obj_enemy_npc(4),
 obj_enemy_npc(5)
}
}


activate_enemies = function(room)
  room.active_enemies = {}
  scene_combat.enemies = 0
  for i=1,#room.spawned_enemies do
    table.insert( room.active_enemies, 1 )
    scene_combat.enemies = scene_combat.enemies + 1
  end
end


scene_combat = room {
    forcedsc = true;
    nam = 'Combat';
    var {
      return_to = nil,
       -- populated by local functions
       enemies = 0,
       enemy_id = 0,
       enemy_attack = '',
       enemy_name = '',
       attack_card = {},
       active_enemies = {},
       -- copied from source room
       spawned_enemies = {},
    },
    entered = function(s,f)
      s.return_to = f
      s.spawned_enemies = f.spawned_enemies
      activate_enemies(s)
      p(' ')
    end,
    left = function(s,t)
      local survived_enemies = {}
      for i=1,#s.spawned_enemies do
        if s.active_enemies[i] ~= 0 then
          table.insert(survived_enemies, s.spawned_enemies[i])
        end
      end
      s.return_to.spawned_enemies = survived_enemies
    end,
    dsc = function(s)

    end;
    obj = {
       obj_enemies,
       obj_combat_descr,
       obj_combat_controls:disable(),
       obj_combat_exit
    }, 
};


loc_combat_test = room {
  nam = 'COMBAT TEST';
  var {
    enemies = {2,3},
    spawned_enemies = {},
    treasure = 5
  };
  enter = function(s,f)
    handle_room_enter(s,f)
  end,
  dsc = function(s)
    pn([[COMBAT TEST ROOM]])
    for k,v in pairs(s.spawned_enemies) do
      pn(k .. ' -- ' .. v)
    end
  end;
  obj = {
    
  };
  way = {
    'scene_combat'
  }
}
