-- $Name:Dungeon in a tin$
instead_version "2.2.0"
require "dash"
require "para"
require "quotes"
require "theme"
require "wroom"

require "functions"
require "game"
require "combat"


global {
  attacks = {
    'Thrust',
    'Swipe',
    'Overhead'
  },
  combat_deck = {
    {'Thrust', 1},
    {'Thrust', 2},
    {'Thrust', 2},
    {'Thrust', 3},
    {'Swipe', 1},
    {'Swipe', 2},
    {'Swipe', 2},
    {'Swipe', 3},
    {'Overhead', 1},
    {'Overhead', 2},
    {'Overhead', 2},
    {'Overhead', 3},
  },
  -- treasure chest coins
  chest = {1,2,3,3,3,3,4,4,4,4,4,5,5,10},
  level_max = 4,
};


status = stat {
  nam = 'stat';
	disp = function(s)
		pn ('Жизнь: ', me().life)
    pn ('Уровень: ', me().level)
    pn ('XP: ', me().xp)
    pn ('Gold: ', me().gold)
	end
};

pl = player {
  var {
    level = 1,
    xp = 0,
    life = 5,
    gold = 0,
    combat_cards = {},
  },
  nam = "Incognito";
  where = 'main';
  obj = { status };
};



init_game = function()
me().level = 1;
me().xp = 0;
me().life = 5;

me().combat_cards = trndc(me().level + 1, combat_deck)
end




start = function()
	--math.randomseed( 1234 )
  init_game()
end

main = room {
  forcedsc = true;
  nam = 'GAME';
--  pic = "house.png";
  dsc = function(s)
    pn 'Start game';    
  end;
  obj = {
		vway('START', '{START}^^', 'loc_village'),
    vway('START', '{combat}', 'loc_combat_test'),
  };
}



game_over = room {
	forcedsc = true;
	nam = "GAME OVER";
	dsc = function(s)
    pn('Game over.')
	end;
	way = {
		'main'
	};
};


