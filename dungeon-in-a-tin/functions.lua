-- increment variable
-- usage: incr(table, key, value)
function incr(t,k,v)
    t[k] = t[k] + (v or 1)
end

-- decrement variable
-- usage: decr(table, key, value)
function decr(t,k,v)
    t[k] = t[k] - (v or 1)
end

-- return random value from given table
-- usage: trnd({'aaa','bbb','ccc'})
function trnd(tbl)
	return tbl[ math.random(#tbl) ];
end

-- return arbitrary number of random values from given table
-- usage: trndc(num,{'aaa','bbb','ccc'})
function trndc(num,tbl)
  local itbl = {}
  for i=1, #tbl do
    itbl[i] = i
  end
	local rtbl = {};
	for t = 1, num do
		local idx = math.random(#itbl);
		table.insert(rtbl, tbl[ itbl[idx] ]);
		table.remove(itbl, idx);
	end
	return rtbl;
end

-- return shuffled table
-- usage: shuffle({'aaa','bbb','ccc'})
function shuffle(tbl)
	return trndc(#tbl, tbl);
end

-- return special dice roll
function roll_dice()
  return trnd({ 1, 2, 2, 3, 3, 4, 4, 5, 5, 6 })
end

-- Случайный текст из [вариант1/вариант2/вариант3]
-- Оригинальный код (с) Николай Коновалов
function divBy( text, command_regex, exclusive ) 
	local start, finish;
	local ordinary, commands = {}, {}
	while text ~= nil do
		start, finish = string.find( text, command_regex )
		if start == nil or finish == nil then
			break
		end
		table.insert( ordinary, string.sub( text, 1, start-1 ))
		if exclusive then
			table.insert( commands, string.sub( text, start+1, finish-1 ))
		else
			table.insert( commands, string.sub( text, start+1, finish ))
		end
		text = string.sub( text, finish+1 )
	end
	if text ~= nil then
		table.insert( ordinary, text )
	end
	return ordinary, commands
end

function rtxt( desc )
	local ordinary_text, random_phrases = divBy( desc, '%[[^%[]*%]', true )		
	for i=1, #random_phrases do
		local variants, _ = divBy( random_phrases[i], '/' )
		random_phrases[i] = variants
	end
	local handler = ""
	for i = 1, #random_phrases do
		handler = handler .. ordinary_text[i] .. trnd(random_phrases[i])
	end
	if #ordinary_text ~= #random_phrases then
		handler = handler .. ordinary_text[#ordinary_text]
	end
	return handler
end
