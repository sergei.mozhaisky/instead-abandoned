spawn_enemies = function(room)
  -- enemy types
  -- 1: goblin
  -- 2: goblin + chest
  -- 3: orc (-1 mod)
  -- 4: orc (-mod) + chest
  local e1 = 0 + room.enemies[1]
  local e2 = 0 + room.enemies[2]
  if visited(room) then
    local total = math.floor((e1 + e2) / 2)
    if total <= room.enemies[2] then
      e2 = total
      e1 = 0
    else
      e2 = 0 + room.enemies[2]
      e1 = total - e2
    end
  end  
  room.spawned_enemies = {}
  for i=1,e1 do
    table.insert( room.spawned_enemies, trnd({1,1,1,1,1,4}) )
  end
  for i=1,e2 do
    table.insert( room.spawned_enemies, trnd({1,1,1,2,3,4}) )
  end  
end


list_rooms = {}
for k,v in pairs(shuffle({'loc_armory', 'loc_fountain', 'loc_furnace', 'loc_kitchen', 'loc_jail'})) do
  table.insert(list_rooms, v)
end
for k,v in pairs(shuffle({'loc_barracks', 'loc_laboratory', 'loc_chasm', 'loc_storage', 'loc_crypt'})) do
  table.insert(list_rooms, v)
end
table.insert(list_rooms, 'loc_princess')


function handle_room_enter(s,f)
  -- refresh combat cards
  me().combat_cards = trndc(me().level + 1, combat_deck)
  
  -- spawn enemies if required
  if s.enemies then
    if #s.spawned_enemies == 0 then
      pn('>> spawn enemies call')
      spawn_enemies(s)
    end
  end
  
  --if s.is_locked or visited(s) then
  if visited(s) then
    
  else
    ways(s):add(f)
    local r = stead.deref(s)
    if r ~= 'loc_storage' and r ~= 'loc_crypt' and r ~= 'loc_princess' then
      ways(s):add(table.remove(list_rooms,1))
      if r == 'loc_fountain' or r == 'loc_kitchen' then
        ways(s):add(table.remove(list_rooms,1))
      end
    end
  end
end




loc_village = room {
  nam = 'Village';
  dsc = function(s)
    pn([[Village]])
    for i,v in pairs(list_rooms) do
			pn(i .. " : " .. v);
		end
  end;
  obj = {
    
  };
  way = {
    'loc_dungeon',
  }
}


loc_dungeon = room {
  nam = 'Dungeon';
  enter = function(s,f)
    handle_room_enter(s,f)
  end,
  dsc = function(s)
    pn([[Dungeon entrance]])
  end;
  obj = {
    
  };
  way = {
    
  }
}

-- Level A

loc_armory = room {
  nam = 'Armory';
  var {
    enemies = {1,0},
    spawned_enemies = {},
    treasure = 1
  },
  enter = function(s,f)
    handle_room_enter(s,f)
  end,
  dsc = function(s)
    pn([[Armory]])
  end;
  obj = {
    
  };
  way = {
    
  }
}

loc_fountain = room {
  nam = 'Fountain';
  var {
    enemies = {2,0},
    spawned_enemies = {},
    treasure = 1
  },
  enter = function(s,f)
    handle_room_enter(s,f)
  end,
  dsc = function(s)
    pn([[Fountain]])
  end;
  obj = {
    
  };
  way = {
    
  }
}

loc_furnace = room {
  nam = 'Furnace';
  var {
    enemies = {1,0},
    spawned_enemies = {}
  },
  enter = function(s,f)
    handle_room_enter(s,f)
  end,
  dsc = function(s)
    pn([[Furnace]])
  end;
  obj = {
    
  };
  way = {
    
  }
}


loc_kitchen = room {
  nam = 'Kitchen';
  var {
    enemies = {2,0},
    spawned_enemies = {}
  },
  enter = function(s,f)
    handle_room_enter(s,f)
  end,
  dsc = function(s)
    pn([[Kitchen]])
  end;
  obj = {
    
  };
  way = {
    
  }
}


loc_jail = room {
  nam = 'Jail';
  var {
    enemies = {2,0},
    spawned_enemies = {}
  },
  enter = function(s,f)
    handle_room_enter(s,f)
  end,
  dsc = function(s)
    pn([[Jail]])
  end;
  obj = {
    
  };
  way = {
    
  }
}

-- level B

loc_barracks = room {
  nam = 'Barracks';
  var {
    enemies = {1,2},
    spawned_enemies = {}
  },
  enter = function(s,f)
    handle_room_enter(s,f)
  end,
  dsc = function(s)
    pn([[Barracks]])
  end;
  obj = {
    
  };
  way = {
    
  }
}


loc_laboratory = room {
  nam = 'Laboratory';
  var {
    enemies = {0,2},
    spawned_enemies = {}
  },
  enter = function(s,f)
    handle_room_enter(s,f)
  end,
  dsc = function(s)
    pn([[Laboratory]])
  end;
  obj = {
    
  };
  way = {
    
  }
}


loc_chasm = room {
  nam = 'Chasm';
  var {
    enemies = {0,1},
    spawned_enemies = {}
  },
  enter = function(s,f)
    handle_room_enter(s,f)
  end,
  dsc = function(s)
    pn([[Chasm]])
  end;
  obj = {
    
  };
  way = {
    
  }
}


loc_storage = room {
  nam = 'Storage';
  var {
    enemies = {0,2},
    spawned_enemies = {},
    treasure = 3
  },
  enter = function(s,f)
    handle_room_enter(s,f)
  end,
  var {
    is_locked = true
  };
  dsc = function(s)
    pn([[Storage]])
  end;
  obj = {
    
  };
  way = {
    
  }
}


loc_crypt = room {
  nam = 'Crypt';
  var {
    enemies = {0,2},
    spawned_enemies = {},
    treasure = 2
  },
  enter = function(s,f)
    handle_room_enter(s,f)
  end,
  var {
    is_locked = true
  };
  dsc = function(s)
    pn([[Crypt]])
  end;
  obj = {
    
  };
  way = {
    
  }
}


loc_princess = room {
  nam = 'Princess';
  var {
    enemies = {2,3},
    spawned_enemies = {},
    treasure = 5,
    is_locked = true
  };
  enter = function(s,f)
    handle_room_enter(s,f)
  end,
  dsc = function(s)
    pn([[Princess room]])
  end;
  obj = {
    
  };
  way = {
    
  }
}



