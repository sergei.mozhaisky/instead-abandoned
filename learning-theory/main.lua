-- $Name:Теория обучения$
-- $Version:0.0.1$
instead_version "1.8.0"
game.codepage="UTF-8";
require "xact"
require "para"
require "wroom"

main = room {
    forcedsc = true,
    nam = "Теория обучения",
    pic = "",
    dsc = [[Начало игры ^ 
    По мотивам рассказа Джеймса Макконнелла "Теория обучения".
    ]],
    obj = {
        obj { nam = '', dsc=[[> {НАЧАТЬ ИГРУ}^^]], act=code[[walk(game_start)]] },
        obj { nam = '', dsc=[[> {2}^^]], act=code[[walk(game_part_2)]] },
    },
};

game_start= room {
    forcedsc = true,
    nam = "???",
    pic = "gfx/lt__white_room.jpg",
    dsc = [[
        Я гулял в рощице возле моего загородного дома, а в следующую секунду оказался
        в небольшой пустой комнате, голый, как птенец, и только способность
        логически рассуждать спасает меня от безумия
    ]],
    obj = {
        obj { nam = '', dsc=[[{Осмотреть комнату}]], act=code[[walk(white_room)]] },
    },
};


white_room = room {
    forcedsc = true,
    nam = "???",
    pic = "gfx/lt__white_room.jpg",
    dsc = [[
    ]],
    obj = {
        'white_room_table',
        'white_room_chair',
    },
};









white_room_table = obj {
    var {
        whiteroom_table_visited = 1
    },
    nam = "Стол",
    dsc = [[В комнате стоит {стол}, белый, как и стены этой комнаты.]],
    act = function(s)
        if s.whiteroom_table_visited == 1 then
            put('white_room_something', 'white_room')
            p([[Присмотревшись, я заметил, что на краю стола что-то лежит.]])
            s.whiteroom_table_visited = 2
        else
            p[[Белый стол из материала наподобие пластика.]]
        end
    end,    
};



white_room_chair = obj {
    nam = "Стул",
    dsc = [[Рядом с ним стоит белый {стул}. ^]],
    act = [[Белый стул. На ощупь напоминает пластик. На нем довольно удобно сидеть.]],
};

white_room_paper = obj {
    var {
        paper_on_table = 1
    },
    nam = "Бумага",
    dsc = function(s, w)
        if s.paper_on_table == 1 then
            p "На столе лежит {бумага}."
        else
            p "На стуле лежит {бумага}."
        end
    end,
    tak = function(s)
        p("Я взял бумагу.")
    end,
	use = function(s, w)
		if w == white_room_paper then
			p 'Что-то наподобие бумаги, но грубее и толще обычной.'
		elseif w == white_room_pencil then
			p 'Я подумал, что рисовать на бумаге было бы удобнее, если положить ее на какую-то твердую поверхность.'
		elseif w == white_room_chair then
        	p 'Я положил бумагу на стул. Хотя на столе, пожалуй, рисовать было бы удобнее.'
            drop('white_room_paper')
            s.paper_on_table = 0
        elseif w == white_room_table then
        	p 'Я положил бумагу на стол.'
            drop('white_room_paper')
            s.paper_on_table = 1
        else
            p 'Бумага тут не пригодится.'
		end
	end        
};
    
white_room_pencil = obj {
    var {
        white_room_pencil_usage_counter = 1
    },
    nam = "Карандаш",
    dsc = "На столе лежит {карандаш}.",
    tak = function(s)
        p("Я взял карандаш.")
    end,
	use = function(s, w)
		if w == white_room_pencil then
			p 'Кусок черного вещества, похож на карандаш.'
		elseif w == white_room_paper then
            if have(white_room_paper) then
                p 'Я подумал, что рисовать на бумаге было бы удобнее, если положить ее на какую-то твердую поверхность.'
            elseif exist(white_room_paper) then
                if s.white_room_pencil_usage_counter == 1 then
                    p [[Я нарисовал на бумаге человечка. ^]]
                    put('white_room_sleep_place', 'white_room')
                    p 'Вдруг пол в углу изменил форму, образовав нечто наподобие кровати.'
                elseif s.white_room_pencil_usage_counter == 2 then
                    p 'Подумав, я написал на бумаге свое имя. Несколько раз.'
                elseif s.white_room_pencil_usage_counter == 3 then
                    p [[Я написал на бумаге большими буквами "ВЫПУСТИТЕ МЕНЯ". Затем
                    повторил эту надпись с обратной стороны на английском, немецком
                    и французском. Других языков, увы, я не знал.]]
                else
                    p 'На листе бумаги закончилось место. Возможно, мне дадут еще листы?'
                end
                s.white_room_pencil_usage_counter = s.white_room_pencil_usage_counter + 1
            end
		elseif w == white_room_chair then
			p 'Я попробовал порисовать на стуле, но на его поверхности не оставалось никаких следов.'
        elseif w == white_room_table then
        	p 'Карандаш не оставлял на столе никаких следов. Бесполезно.'
        else
            p 'Лучше я оставлю карандаш себе.'
		end
	end    
};
    
white_room_something = obj {
    nam = "Что-то",
    dsc = [[На столе {что-то} лежит. ^]],
    act = function(s)
        put('white_room_paper', 'white_room')
        put('white_room_pencil', 'white_room')
        remove('white_room_something')
        p([[На столе я обнаружил лист чего-то наподобие бумаги и черную палочку, похожую на карандаш.]])
    end,
};

white_room_sleep_place = obj {
    nam = "Спать",
    dsc = [[В углу стоит кровать, на которой можно {поспать}. ^]],
    act = function(s)

        p([[Я лег спать и довольно быстро заснул. Спал я плохо, мне снились кошмары.]])
        walk('game_part_2')
    end,
};




-- ACT 2: Skinner box

game_part_2= room {
    enter = function(s)
        drop('white_room_paper')
        remove('white_room_paper')
        drop('white_room_pencil')
        remove('white_room_pencil')
        put('white_room_2_pencil', 'game_part_2')
        take('white_room_2_pencil')
    end,
    forcedsc = true,
    nam = "Белая комната",
    pic = "gfx/lt__white_room.jpg",
    dsc = [[
        Проснувшись, я обнаружил, что мой лист бумаги куда-то пропал.
        Взглянув на стол, я увидел на нем несколько новых предметов. 
        Где я? Что все это значит?
    ]],
    obj = {
        obj { nam = '', dsc=[[{Осмотреть комнату}]], act=code[[walk(white_room_2)]] },
    },
};


white_room_2 = room {
    nam = "Белая комната 2",
    pic = "gfx/lt__white_room.jpg",
    dsc = [[
    
    ]],
    obj = {
        'white_room_2_table',
        'white_room_chair',
    },
    way = {
        wroom("К ближней стене", "Поилка", 'white_room_2_drink'),
        wroom("К дальней стене", "Рычаг", 'white_room_2_skinnerbox')
    },
};

white_room_2_drink = room {
    nam = "Поилка",
    pic = "gfx/lt__white_room.jpg",    
    dsc = "Из стены торчал",
    way = {
        vroom("В центр комнаты", 'white_room_2'),
    }
};


white_room_2_skinnerbox = room {
    nam = "Рычаг",
    pic = "gfx/lt__white_room.jpg",    
    dsc = "Из этой стены торчал рычаг, а слева от него было какое-то отверстие.",
    way = {
        vroom("В центр комнаты", 'white_room_2'),
    },
    obj = {
        'skinnerbox_pull',
        'skinnerbox_box',
    },
};

global {
    skinnerbox_pull_done = 0
}




skinnerbox_pull = obj {
    var {
        pull_counter = 0
    },
    nam = "Рычаг",
    dsc = "Из стены торчит {рычаг}.",
    act = function(s)
        if rnd(4) == 1 or skinnerbox_pull_done == 1 or s.pull_counter > 10 then
            p([[Я дернул за рычаг, что-то в стене щелкнуло, но ничего не произошло.]])
        else
            p[[Я дернул за рычаг, раздался щелчок, и в корзинку под отверстием упал шарик.]]
            skinnerbox_pull_done = 1
        end
        s.pull_counter = s.pull_counter + 1
        if s.pull_counter == 3 then
            p [[^ 
            Все это показалось мне удивительно знакомым. И вдруг я разразился истерическим хохотом.
            Комната превратилась в гигантскую коробку Скиннера! В течение многих лет я исследовал
            процесс обучения животных, помещая белых крыс в коробку Скиннера и наблюдая за
            изменениями в их поведении. Крысы должны были научиться нажимать на рычаг, чтобы
            получить съедобный шарик, выбрасываемый точно таким же аппаратом, как тот, который
            появился на стене моей темницы.]]
        elseif s.pull_counter > 3 then
            if rnd(2) == 1 then
                p [[^Похоже, я стал подопытным животным для внеземного психолога!]]
            end
        end
    end,    
};

skinnerbox_box = obj {
    nam = "Отверстие",
    dsc = "Рядом с ним находится отверстие, а под ним {корзинка}.",
    act = function(s)
        if skinnerbox_pull_done == 1 then
            p([[В корзинке лежал съедобный шарик, точно такой же, как был в миске. Я вытащил его из корзинки и съел.]])
            skinnerbox_pull_done = 0
        else
            p[[В корзинке было пусто.]]
        end
    end,    
};





white_room_2_table = obj {
    var {
        whiteroom_table_visited = 1
    },
    nam = "Стол",
    dsc = [[Белый {стол} все так же стоит посреди комнаты.]],
    act = function(s)
        if s.whiteroom_table_visited == 1 then
            put('white_room_2_paper', 'white_room_2')
            put('white_room_2_food', 'white_room_2')
            p([[На столе лежал чистый лист бумаги. Недалеко от него стояла миска с какими-то шариками.]])
            s.whiteroom_table_visited = 2
        else
            p[[Белый стол из материала наподобие пластика.]]
        end
    end,    
};

white_room_2_food = obj {
    var {
        food_empty = 0
    },
    nam = "Еда",
    dsc = function(s)
        if s.food_empty == 0 then
            p [[На столе стоит {миска} с какими-то шариками.]]
        else
            p [[На столе стоит пустая {миска}.]]
        end
    end,
    act = function(s)
        if s.food_empty == 0 then
            p([[Я взял один из шариков и попробовал его. Нельзя сказать, чтобы это было вкусно, но лучше уж такая еда,
            чем вообще никакой. Со вчерашнего дня я сильно проголодался, поэтому быстро съел все шарики из миски.]])
            s.food_empty = 1
        else
            p([[В миске больше нет съедобных шариков.]])
        end
    end,
};

white_room_2_paper = obj {
    var {
        paper_on_table = 1
    },
    nam = "Бумага",
    dsc = function(s, w)
        if s.paper_on_table == 1 then
            p "На столе лежит {бумага}."
        else
            p "На стуле лежит {бумага}."
        end
    end,
    tak = function(s)
        p("Я взял бумагу.")
    end,
	use = function(s, w)
		if w == white_room_2_paper then
			p 'Что-то наподобие бумаги, но грубее и толще обычной.'
		elseif w == white_room_2_pencil then
			p 'Я подумал, что рисовать на бумаге было бы удобнее, если положить ее на какую-то твердую поверхность.'
		elseif w == white_room_chair then
        	p 'Я положил бумагу на стул. Хотя на столе, пожалуй, рисовать было бы удобнее.'
            drop('white_room_paper')
            s.paper_on_table = 0
        elseif w == white_room_2_table then
        	p 'Я положил бумагу на стол.'
            drop('white_room_2_paper')
            s.paper_on_table = 1
        elseif w == white_room_2_food then
            drop('white_room_2_paper')
            remove('white_room_2_paper')
            p 'Я скомкал бумагу и положил ее в миску. Может, хоть теперь на меня обратят внимание? Что им нужно от меня, в конце концов?'
        else
            p 'Бумага тут не пригодится.'
		end
	end        
};


white_room_2_pencil = obj {
    var {
        white_room_pencil_usage_counter = 1
    },
    nam = "Карандаш",
    dsc = "На столе лежит {карандаш}.",
    tak = function(s)
        p("Я взял карандаш.")
    end,
	use = function(s, w)
		if w == white_room_2_pencil then
			p 'Кусок черного вещества, похож на карандаш.'
		elseif w == white_room_2_paper then
            if have(white_room_2_paper) then
                p 'Я подумал, что рисовать на бумаге было бы удобнее, если положить ее на какую-то твердую поверхность.'
            elseif exist(white_room_2_paper) then
                if s.white_room_pencil_usage_counter == 1 then
                    p [[Я нарисовал на листе бумаги схематичное изображение комнаты, в которой я находился.]]
                elseif s.white_room_pencil_usage_counter == 2 then
                    p 'В нижней части листа я нарисовал миску и написал "СПАСИБО".'
                elseif s.white_room_pencil_usage_counter == 3 then
                    p [[С обратной стороны листа я кратко описал все произошедшее вчера. Может, они хотят, чтобы я вел дневник?]]
                else
                    p 'На листе бумаги закончилось место. Возможно, мне дадут еще листы?'
                end
                s.white_room_pencil_usage_counter = s.white_room_pencil_usage_counter + 1
            end
		elseif w == white_room_chair then
			p 'Я попробовал порисовать на стуле, но на его поверхности не оставалось никаких следов.'
        elseif w == white_room_2_table then
        	p 'Карандаш не оставлял на столе никаких следов. Бесполезно.'
        else
            p 'Лучше я оставлю карандаш себе.'
		end
	end    
};



