-- Arkham

game_start = room {
    forcedsc = true,
    nam = "???",
    pic = "",
    dsc = function(s, w)
        num_s, results = dices(10,5)
        for i,v in ipairs(results) do
            pn(i, ' - ' .. v)
        end
        pn("Successes: " .. num_s)
    end,
    obj = {
        obj { nam = '', dsc=[[{Начало} ^ ]], act=code[[walk(loc_train_station)]] },
        obj { nam = '', dsc=[[{Подготовка} ^ ]], act=code[[walk(investigator_prepare)]] },
        obj { nam = '', dsc=[[{Бой} ^ ]], act=code[[walk(test_battle_room)]] },
    },
};

investigator_prepare = room {
    forcedsc = true,
    nam = "Подготовка",
    pic = "",
    dsc = function(s, w)
        pn ('Скрытность: ', me().skills.agility)
        pn ('Сила: ', me().skills.strength)
        pn ('Воля: ', me().skills.will)
        pn ('Знания: ', me().skills.lore)
        pn ('Удача: ', me().skills.luck)
    end,
    obj = {
        obj { nam = '', dsc=[[{Увеличить скрытность}]], act=code[[ pl.skills.agility = pl.skills.agility + 1; walk('investigator_prepare') ]] },
    },
};