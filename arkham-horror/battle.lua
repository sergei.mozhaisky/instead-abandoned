function monster_activity(s,w)
    if w == "monster_try_evade" then
        if check_skill(me().skills.agility, s.monster_ref.evade, 1) then
            p( s.monster_ref.msg.evade_succeeded )
            objs():del('monster_is_here')
            objs():del('monster_try_evade')
            objs():del('monster_try_battle')
            ways():enable_all()
        else
            p( s.monster_ref.msg.evade_failed )
            my_health(0 - s.monster_ref.body_damage) -- monster deals damage
            if me().health <= 0 then
                walk('battle_defeat_health') -- player 'killed'
            else
                if monster_sanity_check(s.monster_ref) then
                    walk('battle')
                else
                    walk('battle_defeat_sanity')
                end                    
            end
        end
    elseif w == "monster_try_battle" then
        p( s.monster_ref.msg.battle_started )
        if monster_sanity_check(s.monster_ref) then
            walk('battle')
        else
            walk('battle_defeat_sanity')
        end
    end 
end

function monster_handler_attack(monster,w)
    local weapon = stead.deref(w);
    p(monster.msg.attacked[weapon]) -- monster is attacked
    if check_skill(me().skills.strength + w.damage, monster.body_level, monster.hp) then
        p(monster.msg.killed[weapon])
        remove(monster) -- kill monster and remove from scene
        walkout() -- return to previous scene
    else
        p(monster.msg.missed[weapon])
        my_health(0 - monster.body_damage) -- monster deals damage
        if me().health <= 0 then
            walk('battle_defeat_health') -- player 'killed'
        end
    end
end

function monster_sanity_check(monster)
    if check_skill(me().sanity, monster.mind_level, 1) then
        rprint({
                "Я ничем не выказал своего испуга и приготовился к бою.",
                "Я уже успел навидаться всяких тварей, так что совсем не удивился."
               })
    else
        my_sanity(0 - monster.mind_damage)
        if me().sanity > 0 then
            rprint({
                    "Кровь застыла в моих жилах при виде этого монстра. Однако я превозмог свой страх.",
                    "Мне еще никогда не было так страшно. Но все же я собрался с духом и приготовился к атаке."
                   })
        end
    end
    if me().sanity > 0 then
        return 1
    else
        return nil
    end
end



test_battle_room = room {
    var {
        monster_ref = '',
    },
    forcedsc = true,
    nam = "Локация с монстром",
    pic = "gfx/logo.jpg",
    dsc = function(s, w)
        p([[Убогость здешней обстановки поражала. По углам было разбросано грязное
белье, стояла колченогая кровать и уродливый комод, за которым валялась огромных размеров тарелка  с присохшими к  ней  остатками
пищи. В комнате стоял невыносимый гнилостный запах. Внушительные залежи пыли и густая сеть паутины на стенах и потолке
довершали картину отвратительного запустения.]])
    end,
    obj = {

    },
    way = {
        'game_start'
    },
    act = monster_activity,
    entered = function(s, f)
        s.monster_ref = stead.ref('monster_dark_druid')
        objs():add(vobj("monster_is_here", s.monster_ref.xnam))
        objs():add(vobj("monster_try_evade", "Что делать - попробовать {скрыться}"))
        objs():add(vobj("monster_try_battle", "или {напасть}?"))
        ways():disable_all()
    end,

    exit = function(s, f)
        objs():del('monster_is_here')
        objs():del('monster_try_evade')
        objs():del('monster_try_battle')    
        ways():enable_all()
    end,
};

monster_fight = obj {
    nam = "",
    xdsc = [[ Попытаться {do_hide|скрыться} или {do_fight|напасть}? ]],
    act = function (s)
        pn(">> MONSTER <<")
    end,
    obj = {
        xdsc()
    },
};




battle = room {
    forcedsc = true,
    nam = "Бой",
    pic = "gfx/battle.jpg",
    dsc = function(s, w)

    end,
    obj = {},
    entered = function(s, f)
        objs():add(f.monster_ref)
        inv_weapons:enable()
    end,
    exit = function(s, f)
        inv_weapons:disable()
        objs():del(f.monster_ref)
    end,
};

battle_defeat_health = room {
    forcedsc = true,
    nam = "Бой",
    pic = "gfx/battle.jpg",
    dsc = function(s, w)
        pn("Я упал в обморок и плохо помню что было дальше.")
    end,
    entered = function(s, f)
        me().health = 1 -- automatically recover 1 health
    end,    
    way = {
        'game_start'
    }
};

battle_defeat_sanity = room {
    forcedsc = true,
    nam = "Бой",
    pic = "gfx/battle.jpg",
    dsc = function(s, w)
        pn("Я схожу с ума! Нет, только не это!!! Не-е-е-е-е-ет!!!.")
    end,
    entered = function(s, f)
        me().sanity = 1 -- automatically recover 1 health
    end,    
    way = {
        'game_start'
    }
};




monster_dark_druid = obj {
    var {
        evade = -2,
        mind_level  = -1, -- modifier for mind
        mind_damage = 1, -- if mind check is not passed
        body_level  = -2, -- modifier to battle
        body_damage = 1, -- if monster deals damage
        hp = 2,          -- how many successes to kill monster
        xnam = imgl('gfx/monsters/dark_druid.jpg') .." Здесь бродит темный друид.",
        xdescr = {
            "{Темный друид} готовится к магической атаке.",
            "Вид {темного друида} внушает страх.",
            "{Темный друид} сейчас нападет."
        },   
        msg = {
            evade_succeeded = "К счастью, друид не заметил меня",
            evade_failed = "Я попытался спрятаться, но бесполезно. Друид напал на меня.",
            battle_started = "Решив, что прятаться не стоит, я вышел навстречу друиду.",
            attacked = {
                knife = "Я ткнул ножом друида.",
                pistol = "Я выстрелил в друида несколько раз.",
                dynamite = "Я поджег динамит и бросил в друида.",
            },
            killed = {
                knife = "Друид мертв.",
                pistol = "Друид свалился.",
                dynamite = "Взрыв разнес тело несчастного на кусочки.",
            },
            missed = {
                knife = "К сожалению, я промахнулся, и друид ударил в ответ.",
                pistol = "Похоже, ни одна из моих пуль не попала в цель.",
                dynamite = "Друид ловко отскочил, и его не задело взрывом.",
            },                        
        }
    },
    nam = "Темный друид",
    dsc = function (s)
        p(imgl('gfx/monsters/dark_druid.jpg'))
        rprint(s.xdescr)
        --pn(.. " Это {темный друид}.")
    end,
    used = monster_handler_attack,
    --act = function (s)
    --end,
};








inv_weapons = stat {
    nam = "^-- Оружие",
    obj = { 'knife', 'pistol', 'dynamite' }
}

inv_things = stat {
    nam = "^-- Вещи",
    obj = { 'elder_sign' }
}


knife = obj {
    var {
        damage = 1,
        is_weapon = 1,
    },
    nam = "Нож",
    inv = function (s, w)
        p("Нож с удобной рукоятью.")        
    end,
}


pistol = obj {
    var {
        damage = 3,
        is_weapon = 1,
        nam = "Револьвер",        
    },
    inv = function (s, w)
        p("Револьвер.")         
    end,
}

dynamite = obj {
    var {
        damage = 8,
        is_weapon = 1,
    },
    nam = "Динамит",
    inv = function (s, w)
        p("Динамитная шашка. Мощное оружие, но использовать его выйдет всего один раз.")        
    end,
    use = function (s,w)
        remove(s, me()) -- one-time
    end,
}

elder_sign = obj {
    var = {
    },
    nam = "Знак Древних",
    inv = function (s, w)
        p("Печать со Знаком Древних.")
    end,
}