-- $Name:Ужас Аркхема$
-- $Version:0.0.1$
instead_version "1.8.0"
game.codepage="UTF-8";
require "xact"
require "para"
require "dash"
require "wroom"
require "hideinv"
require "counters"
require "nouse"
-- require "dbg"

dofile "functions.lua"
dofile "lib/dices.lua"
-- dofile "game/smartphone/main.lua"
dofile "battle.lua"
dofile "arkham.lua"
-- locations
dofile "game/locations/arkham_downtown.lua"
dofile "game/locations/arkham_easttown.lua"
dofile "game/locations/arkham_french_hill.lua"
dofile "game/locations/arkham_merchant.lua"
dofile "game/locations/arkham_miskatonik.lua"
dofile "game/locations/arkham_northside.lua"
dofile "game/locations/arkham_rivertown.lua"
dofile "game/locations/arkham_southside.lua"
dofile "game/locations/arkham_uptown.lua"
-- encounters
dofile "game/encounters/arkham_asylum.lua"
dofile "game/encounters/arkham_bank.lua"
dofile "game/encounters/arkham_black_cave.lua"
dofile "game/encounters/arkham_cemetery.lua"
dofile "game/encounters/arkham_gibbs_roadhouse.lua"
dofile "game/encounters/arkham_historical_society.lua"
dofile "game/encounters/arkham_independence_square.lua"
dofile "game/encounters/arkham_ma_house.lua"
dofile "game/encounters/arkham_misk_admin_building.lua"
dofile "game/encounters/arkham_misk_library.lua"
dofile "game/encounters/arkham_misk_science_building.lua"
dofile "game/encounters/arkham_nameless.lua"
dofile "game/encounters/arkham_newspaper_office.lua"
dofile "game/encounters/arkham_old_magic_shop.lua"
dofile "game/encounters/arkham_old_shop.lua"
dofile "game/encounters/arkham_police.lua"
dofile "game/encounters/arkham_river_island.lua"
dofile "game/encounters/arkham_river_port.lua"
dofile "game/encounters/arkham_shop.lua"
dofile "game/encounters/arkham_south_church.lua"
dofile "game/encounters/arkham_st_mary_hospital.lua"
dofile "game/encounters/arkham_train_station.lua"
dofile "game/encounters/arkham_twilight_lodge.lua"
dofile "game/encounters/arkham_velma_diner.lua"
dofile "game/encounters/arkham_witch_house.lua"
dofile "game/encounters/arkham_woods.lua"




global {
    cfg = {
        debug = 1,
    },

}




pl = player {
    nam = "Player",
    where = 'main',
    var {
        max_health = 8,
        max_sanity = 8,
        health = 3,
        sanity = 2,
        skills = {
            speed = 3,
            agility = 3,
            strength = 3,
            will = 5,
            lore = 3,
            luck = 3,
        },
        success = 5,
    },
    obj = { },
};

status = stat {
    nam = 'статус';
    disp = function(s)
        for i=1,me().health do
            p(imgl('gfx/body.png'))
        end        
        pn ''
        for i=1,me().sanity do
            p(imgl('gfx/mind.png'))
        end
        --pn (imgl('gfx/mind.png'), me().sanity)
        pn ''
        pn ('Скорость: ', me().skills.speed)
        pn ('Скрытность: ', me().skills.agility)
        pn ('Сила: ', me().skills.strength)
        pn ('Воля: ', me().skills.will)
        pn ('Знания: ', me().skills.lore)
        pn ('Удача: ', me().skills.luck)
    end
};

take(status);
take(inv_weapons);
take(inv_things);
inv_weapons:disable(); -- weapons available only in battle

-- ---------------------------------------------------------

main = room {
    forcedsc = true,
    nam = "Ужас Аркхема",
    pic = "gfx/logo.jpg",
    dsc = nil,
    obj = {
        obj { nam = '', dsc=[[{НАЧАТЬ ИГРУ}^^]], act=code[[walk(game_start)]] },
    
        obj { nam = '', dsc=[[{Об игре} ^]], act=code[[walk(game_details)]] },        
    },
};


game_details = room {
    forcedsc = true,
    nam = "Об игре",
    pic = "gfx/logo.jpg",
    dsc = [[
        TODO: Информация об игре
        ^
        Можайский Сергей ^
        http://technix.in.ua/
    ]],
    obj = {
        obj { nam = '', dsc=[[{Назад}]], act=code[[walk(main)]] },
    },
};


game_end = room {
    nam = "",
    pic = "gfx/logo.jpg",
    forcedsc = true,
    dsc = [[

    ]],
    obj = {
        obj { nam = '', dsc=[[{Меню}]], act=code[[walk(main)]] },
    },    
};


