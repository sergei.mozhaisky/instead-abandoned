function dice()
    return rnd(6)
end

function dices(num_dices, success)
    results = {}
    num_successes = 0
    for i=1,num_dices do
        results[i] = rnd(6)
        if results[i] >= success then
            num_successes = num_successes + 1
        end
    end
    return num_successes, results
end

function check_skill(initial_value, modifier, r_successes)
    req_successes = (r_successes or 1)
    -- automatically fail check if result is 0
    if initial_value + modifier <= 0 then
        return nil
    end

    num_successes, results = dices(initial_value + modifier, me().success)
    if cfg.debug then
        pn("I: "..initial_value.." | M: "..modifier.." | Rs: "..req_successes.." || S: "..num_successes)
    end

    if num_successes >= req_successes then
        return 1
    else
        return nil
    end
end