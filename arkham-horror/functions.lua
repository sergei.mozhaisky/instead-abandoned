-- functions

function exec(s)
  assert(loadstring(s))();
end

function incr(t,k,v)
    t[k] = t[k] + (v or 1)
end

function decr(t,k,v)
    t[k] = t[k] - (v or 1)
end

function rprint(array)
    local count = 0
    for _ in pairs(array) do
        count = count + 1
    end
    p( array[rnd(count)] .. " " )
end

-- -------------------------------------------------------------

function my_health(v)
    if me().health + v <= me().max_health then
        me().health = me().health + v
    end
end

function my_sanity(v)
    if me().sanity + v <= me().max_sanity then
        me().sanity = me().sanity + v
    end
end

-- -------------------------------------------------------------

function location_encounter(room)
    if room.last_encounter == 0 or walk_count() - room.last_encounter > 7 then
        objs(room):add("encounter_"..room.loc_id)
        room.last_encounter = walk_count()
    end
end

