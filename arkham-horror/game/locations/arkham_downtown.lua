
loc_downtown = room {
    forcedsc = true,
    nam = "Даунтаун",
    pic = "gfx/arkham.jpg",
    dsc = function(s, w)

    end,
    obj = {
        vway("Банк", [[Большое здание с массивными колоннами - это местный {банк}.]], 'loc_bank'),
        vway("Аркхемская лечебница", [[Неподалеку за кованым забором виднеется здание {Аркхемской лечебницы}.^]], 'loc_asylum'),
        vway("Площадь Независимости", [[Рядом находится {Площадь Независимости}.^]], 'loc_independence_square'),

    },
    way = {
        wroom("Нортсайд", 'loc_northside'),
        wroom("Торговый район", 'loc_merchant'),
        wroom("Исттаун", 'loc_easttown'),
    },
};

loc_bank = room {
    var {
        loc_id = 'bank',
        last_encounter = 0
    },
    forcedsc = true,
    nam = "Банк",
    pic = "",
    dsc = function(s, w)

    end,
    obj = {

    },
    way = {
        wroom("На улицы Даунтауна", 'loc_downtown'),
    },
    enter = function(s, f)
        location_encounter(s)
    end,
    exit = function(s, f)
        objs():del('encounter')
    end,    
};

loc_asylum = room {
    var {
        loc_id = 'asylum',
        last_encounter = 0
    },
    forcedsc = true,
    nam = "Аркхемская лечебница",
    pic = "",
    dsc = function(s, w)

    end,
    obj = {

    },
    way = {
        wroom("На улицы Даунтауна", 'loc_downtown'),
    },
    enter = function(s, f)
        location_encounter(s)
    end,
    exit = function(s, f)
        objs():del('encounter')
    end,    
};

loc_independence_square = room {
    var {
        loc_id = 'independence_square',
        last_encounter = 0
    },
    forcedsc = true,
    nam = "Площадь Независимости",
    pic = "",
    dsc = function(s, w)

    end,
    obj = {

    },
    way = {
        wroom("На улицы Даунтауна", 'loc_downtown'),
    },
    enter = function(s, f)
        location_encounter(s)
    end,
    exit = function(s, f)
        objs():del('encounter')
    end,    
};
