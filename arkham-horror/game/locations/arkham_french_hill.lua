
loc_french_hill = room {
    forcedsc = true,
    nam = "Френч-Хилл",
    pic = "gfx/arkham.jpg",
    dsc = function(s, w)

    end,
    obj = {
        vway("Ложа Серебряных Сумерек", [[ {Ложа Серебряных Сумерек} ^ ]], 'loc_twilight_lodge'),
        vway("Ведьмин дом", [[ {Ведьмин дом} ^ ]], 'loc_witch_house'),
    },
    way = {
        wroom("Ривертаун", 'loc_rivertown'),
        wroom("Мискатоникский университет", 'loc_miskatonik'),
        wroom("Саутсайд", 'loc_southside'),
    },
};

loc_twilight_lodge = room {
    var {
        loc_id = 'twilight_lodge',
        last_encounter = 0
    },
    forcedsc = true,
    nam = "Ложа Серебряных Сумерек",
    pic = "",
    dsc = function(s, w)

    end,
    obj = {

    },
    way = {
        wroom("На улицы Френч-Хилла", 'loc_french_hill'),
    },
    enter = function(s, f)
        location_encounter(s)
    end,
    exit = function(s, f)
        objs():del('encounter')
    end,
};

loc_witch_house = room {
    var {
        loc_id = 'witch_house',
        last_encounter = 0
    },
    forcedsc = true,
    nam = "Ведьмин дом",
    pic = "",
    dsc = function(s, w)

    end,
    obj = {

    },
    way = {
        wroom("На улицы Френч-Хилла", 'loc_french_hill'),
    },
    enter = function(s, f)
        location_encounter(s)
    end,
    exit = function(s, f)
        objs():del('encounter')
    end,    
};