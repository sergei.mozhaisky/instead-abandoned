
loc_merchant = room {
    forcedsc = true,
    nam = "Торговый район",
    pic = "",
    dsc = function(s, w)

    end,
    obj = {
        vway("Речной порт", [[ {Речной порт} ^ ]], 'loc_river_port'),
        vway("Безлюдный остров", [[ {Безлюдный остров} ^ ]], 'loc_river_island'),
        vway("Неименуемое", [[ {Неименуемое} ^ ]], 'loc_nameless'),
    },
    way = {
        wroom("Нортсайд", 'loc_northside'),
        wroom("Даунтаун", 'loc_downtown'),
        wroom("Ривертаун", 'loc_rivertown'),
        wroom("Мискатоникский университет", 'loc_miskatonik'),
    },
};

loc_river_port = room {
    var {
        loc_id = 'river_port',
        last_encounter = 0
    },
    forcedsc = true,
    nam = "Речной порт",
    pic = "",
    dsc = function(s, w)

    end,
    obj = {

    },
    way = {
        wroom("На улицы Торгового района", 'loc_merchant'),
    },
    enter = function(s, f)
        location_encounter(s)
    end,
    exit = function(s, f)
        objs():del('encounter')
    end,    
};

loc_river_island = room {
    var {
        loc_id = 'river_island',
        last_encounter = 0
    },
    forcedsc = true,
    nam = "Безлюдный остров",
    pic = "",
    dsc = function(s, w)

    end,
    obj = {

    },
    way = {
        wroom("На улицы Торгового района", 'loc_merchant'),
    },
    enter = function(s, f)
        location_encounter(s)
    end,
    exit = function(s, f)
        objs():del('encounter')
    end,    
};

loc_nameless = room {
    var {
        loc_id = 'nameless',
        last_encounter = 0
    },
    forcedsc = true,
    nam = "Неименуемое",
    pic = "",
    dsc = function(s, w)

    end,
    obj = {

    },
    way = {
        wroom("На улицы Торгового района", 'loc_merchant'),
    },
    enter = function(s, f)
        location_encounter(s)
    end,
    exit = function(s, f)
        objs():del('encounter')
    end,    
};
