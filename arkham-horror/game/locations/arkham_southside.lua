
loc_southside = room {
    forcedsc = true,
    nam = "Саутсайд",
    pic = "",
    dsc = function(s, w)

    end,
    obj = {
        vway("Пансион Мамаши", [[ {Пансион Мамаши} ^ ]], 'loc_ma_house'),
        vway("Историческое общество", [[ {Историческое общество} ^ ]], 'loc_historical_society'),
        vway("Южная церковь", [[ {Южная церковь} ^ ]], 'loc_south_church'),
    },
    way = {
        wroom("Аптаун", 'loc_uptown'),
        wroom("Френч-Хилл", 'loc_french_hill'),
    },
};

loc_ma_house = room {
    var {
        loc_id = 'ma_house',
        last_encounter = 0
    },
    forcedsc = true,
    nam = "Пансион Мамаши",
    pic = "",
    dsc = function(s, w)

    end,
    obj = {

    },
    way = {
        wroom("На улицы Саутсайда", 'loc_southside'),
    },
    enter = function(s, f)
        location_encounter(s)
    end,
    exit = function(s, f)
        objs():del('encounter')
    end,     
};

loc_historical_society = room {
    var {
        loc_id = 'historical_society',
        last_encounter = 0
    },
    forcedsc = true,
    nam = "Историческое общество",
    pic = "",
    dsc = function(s, w)

    end,
    obj = {

    },
    way = {
        wroom("На улицы Саутсайда", 'loc_southside'),
    },
    enter = function(s, f)
        location_encounter(s)
    end,
    exit = function(s, f)
        objs():del('encounter')
    end,     
};

loc_south_church = room {
    var {
        loc_id = 'south_church',
        last_encounter = 0
    },
    forcedsc = true,
    nam = "Южная церковь",
    pic = "",
    dsc = function(s, w)

    end,
    obj = {

    },
    way = {
        wroom("На улицы Саутсайда", 'loc_southside'),
    },
    enter = function(s, f)
        location_encounter(s)
    end,
    exit = function(s, f)
        objs():del('encounter')
    end,     
};
