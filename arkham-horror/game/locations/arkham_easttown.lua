
loc_easttown = room {
    forcedsc = true,
    nam = "Исттаун",
    pic = "gfx/arkham.jpg",
    dsc = function(s, w)

    end,
    obj = {
        vway("Закусочная Гиббса", [[ {Закусочная Гиббса} ^ ]], 'loc_gibbs_roadhouse'),
        vway("Ресторанчик Вельмы", [[ {Ресторанчик Вельмы} ^ ]], 'loc_velma_diner'),
        vway("Полицейский участок", [[ {Полицейский участок} ^ ]], 'loc_police'),
    },
    way = {
        wroom("Даунтаун", 'loc_downtown'),
        wroom("Ривертаун", 'loc_rivertown'),        
    },
};

loc_gibbs_roadhouse = room {
    var {
        loc_id = 'gibbs_roadhouse',
        last_encounter = 0
    },
    forcedsc = true,
    nam = "Закусочная Гиббса",
    pic = "",
    dsc = function(s, w)

    end,
    obj = {

    },
    way = {
        wroom("На улицы Исттауна", 'loc_easttown'),
    },
    enter = function(s, f)
        location_encounter(s)
    end,
    exit = function(s, f)
        objs():del('encounter')
    end,     
};

loc_velma_diner = room {
    var {
        loc_id = 'velma_diner',
        last_encounter = 0
    },
    forcedsc = true,
    nam = "Ресторанчик Вельмы",
    pic = "",
    dsc = function(s, w)

    end,
    obj = {

    },
    way = {
        wroom("На улицы Исттауна", 'loc_easttown'),
    },
    enter = function(s, f)
        location_encounter(s)
    end,
    exit = function(s, f)
        objs():del('encounter')
    end,     
};

loc_police = room {
    var {
        loc_id = 'police',
        last_encounter = 0
    },
    forcedsc = true,
    nam = "Полицейский участок",
    pic = "",
    dsc = function(s, w)

    end,
    obj = {

    },
    way = {
        wroom("На улицы Исттауна", 'loc_easttown'),
    },
    enter = function(s, f)
        location_encounter(s)
    end,
    exit = function(s, f)
        objs():del('encounter')
    end,     
};