-- Northside

loc_northside = room {
    forcedsc = true,
    nam = "Нортсайд",
    pic = "gfx/arkham_northside.jpg",
    dsc = function(s, w)
        pn [[]]
        p [[ Район Нортсайда. ]]
    end,
    obj = {
        vway("Вокзал", [[Неподалеку виднеется здание железнодорожного {вокзала}.^]], 'loc_train_station'),
        vway("Офис газеты", [[В этом невзрачном здании, судя по вывеске, расположен {офис местной газеты}.^]], 'loc_newspaper_office'),
        vway("Лавка древностей", [[Между домами расположен магазинчик. Вывеска гласит: '{Лавка древностей}'.^]], 'loc_old_shop'),
    },
    way = {
        wroom("Даунтаун", 'loc_downtown'),
        wroom("Торговый район", 'loc_merchant'),
    },
};

loc_train_station = room {
    var {
        loc_id = 'train_station',
        last_encounter = 0
    },
    forcedsc = true,
    nam = "Вокзал",
    pic = "gfx/arkham_train_station.jpg",
    dsc = function(s, w)
        p[[Вокзал.]]
    end,
    obj = {

    },
    way = {
        wroom("На улицы Нортсайда", 'loc_northside'),
    },
    enter = function(s, f)
        location_encounter(s)
    end,
    exit = function(s, f)
        objs():del('encounter')
    end,    
};

loc_newspaper_office = room {
    var {
        loc_id = 'newspaper_office',
        last_encounter = 0
    },
    forcedsc = true,
    nam = "Офис газеты",
    pic = "",
    dsc = function(s, w)

    end,
    obj = {

    },
    way = {
        wroom("На улицы Нортсайда", 'loc_northside'),
    },
    enter = function(s, f)
        location_encounter(s)
    end,
    exit = function(s, f)
        objs():del('encounter')
    end,
};

loc_old_shop = room {
    var {
        loc_id = 'old_shop',
        last_encounter = 0
    },
    forcedsc = true,
    nam = "Лавка древностей",
    pic = "",
    dsc = function(s, w)

    end,
    obj = {

    },
    way = {
        wroom("На улицы Нортсайда", 'loc_northside'),
    },
    enter = function(s, f)
        location_encounter(s)
    end,
    exit = function(s, f)
        objs():del('encounter')
    end,    
};


