
loc_miskatonik = room {
    forcedsc = true,
    nam = "Мискатоникский университет",
    pic = "",
    dsc = function(s, w)

    end,
    obj = {
        vway("Администрация", [[ {Администрация} ^ ]], 'loc_misk_admin_building'),
        vway("Библиотека", [[ {Библиотека} ^ ]], 'loc_misk_library'),
        vway("Лабораторный корпус", [[ {Лабораторный корпус} ^ ]], 'loc_misk_science_building'),
    },
    way = {
        wroom("Торговый район", 'loc_merchant'),
        wroom("Френч-Хилл", 'loc_french_hill'),
        wroom("Аптаун", 'loc_uptown'),
    },
};

loc_misk_admin_building = room {
    var {
        loc_id = 'misk_admin_building',
        last_encounter = 0
    },
    forcedsc = true,
    nam = "Администрация",
    pic = "",
    dsc = function(s, w)

    end,
    obj = {

    },
    way = {
        wroom("Во двор университета", 'loc_miskatonik'),
    },
    enter = function(s, f)
        location_encounter(s)
    end,
    exit = function(s, f)
        objs():del('encounter')
    end,     
};

loc_misk_library = room {
    var {
        loc_id = 'misk_library',
        last_encounter = 0
    },
    forcedsc = true,
    nam = "Библиотека",
    pic = "",
    dsc = function(s, w)

    end,
    obj = {

    },
    way = {
        wroom("Во двор университета", 'loc_miskatonik'),
    },
    enter = function(s, f)
        location_encounter(s)
    end,
    exit = function(s, f)
        objs():del('encounter')
    end,     
};

loc_misk_science_building = room {
    var {
        loc_id = 'misk_science_building',
        last_encounter = 0
    },
    forcedsc = true,
    nam = "Лабораторный корпус",
    pic = "",
    dsc = function(s, w)

    end,
    obj = {

    },
    way = {
        wroom("Во двор университета", 'loc_miskatonik'),
    },
    enter = function(s, f)
        location_encounter(s)
    end,
    exit = function(s, f)
        objs():del('encounter')
    end,     
};