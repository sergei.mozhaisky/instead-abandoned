
loc_uptown = room {
    forcedsc = true,
    nam = "Аптаун",
    pic = "",
    dsc = function(s, w)

    end,
    obj = {
        vway("Больница Св. Марии", [[ {Больница Св. Марии} ^ ]], 'loc_st_mary_hospital'),
        vway("Старинная лавка волшбы", [[ {Старинная лавка волшбы} ^ ]], 'loc_old_magic_shop'),
        vway("Леса", [[ {Леса} ^ ]], 'loc_woods'),
    },
    way = {
        wroom("Мискатоникский университет", 'loc_miskatonik'),
        wroom("Саутсайд", 'loc_southside'),
    },
};

loc_st_mary_hospital = room {
    var {
        loc_id = 'st_mary_hospital',
        last_encounter = 0
    },
    forcedsc = true,
    nam = "Больница Св. Марии",
    pic = "",
    dsc = function(s, w)

    end,
    obj = {

    },
    way = {
        wroom("На улицы Аптауна", 'loc_uptown'),
    },
    enter = function(s, f)
        location_encounter(s)
    end,
    exit = function(s, f)
        objs():del('encounter')
    end,     
};

loc_old_magic_shop = room {
    var {
        loc_id = 'old_magic_shop',
        last_encounter = 0
    },
    forcedsc = true,
    nam = "Старинная лавка волшбы",
    pic = "",
    dsc = function(s, w)

    end,
    obj = {

    },
    way = {
        wroom("На улицы Аптауна", 'loc_uptown'),
    },
    enter = function(s, f)
        location_encounter(s)
    end,
    exit = function(s, f)
        objs():del('encounter')
    end,     
};

loc_woods = room {
    var {
        loc_id = 'woods',
        last_encounter = 0
    },
    forcedsc = true,
    nam = "Леса",
    pic = "",
    dsc = function(s, w)

    end,
    obj = {

    },
    way = {
        wroom("На улицы Аптауна", 'loc_uptown'),
    },
    enter = function(s, f)
        location_encounter(s)
    end,
    exit = function(s, f)
        objs():del('encounter')
    end,     
};
