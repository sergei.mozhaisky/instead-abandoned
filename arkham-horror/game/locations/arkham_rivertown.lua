
loc_rivertown = room {
    forcedsc = true,
    nam = "Ривертаун",
    pic = "",
    dsc = function(s, w)

    end,
    obj = {
        vway("Магазин", [[ {Магазин} ^ ]], 'loc_shop'),
        vway("Кладбище", [[ {Кладбище} ^ ]], 'loc_cemetery'),
        vway("Черная пещера", [[ {Черная пещера} ^ ]], 'loc_black_cave'),
    },
    way = {
        wroom("Исттаун", 'loc_easttown'),
        wroom("Торговый район", 'loc_merchant'),
        wroom("Френч-Хилл", 'loc_french_hill'),
    },
};

loc_shop = room {
    var {
        loc_id = 'shop',
        last_encounter = 0
    },
    forcedsc = true,
    nam = "Магазин",
    pic = "",
    dsc = function(s, w)

    end,
    obj = {

    },
    way = {
        wroom("На улицы Ривертауна", 'loc_rivertown'),
    },
    enter = function(s, f)
        location_encounter(s)
    end,
    exit = function(s, f)
        objs():del('encounter')
    end,     
};

loc_cemetery = room {
    var {
        loc_id = 'cemetery',
        last_encounter = 0
    },
    forcedsc = true,
    nam = "Кладбище",
    pic = "",
    dsc = function(s, w)

    end,
    obj = {

    },
    way = {
        wroom("На улицы Ривертауна", 'loc_rivertown'),
    },
    enter = function(s, f)
        location_encounter(s)
    end,
    exit = function(s, f)
        objs():del('encounter')
    end,     
};

loc_black_cave = room {
    var {
        loc_id = 'black_cave',
        last_encounter = 0
    },
    forcedsc = true,
    nam = "Черная пещера",
    pic = "",
    dsc = function(s, w)

    end,
    obj = {

    },
    way = {
        wroom("На улицы Ривертауна", 'loc_rivertown'),
    },
    enter = function(s, f)
        location_encounter(s)
    end,
    exit = function(s, f)
        objs():del('encounter')
    end,     
};