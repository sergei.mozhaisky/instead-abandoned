-- encounters - miskatonik admin building

encounter_misk_admin_building = function ()
    el = {
        enc_misk_admin_building_1,
        enc_misk_admin_building_2,
        enc_misk_admin_building_3,
        enc_misk_admin_building_4,
        enc_misk_admin_building_5,
        enc_misk_admin_building_6,
        enc_misk_admin_building_7,
    }
    return el[rnd(7)]
end

enc_misk_admin_building_1 = obj {
    nam = "encounter",
    dsc = [[]],
    act = function (s)
        if check_skill( me().skills.agility, 0 ) then
            pn [[]]
        else
            pn [[]]
        end
        objs():del(s)
    end
};

enc_misk_admin_building_2 = obj {
    nam = "encounter",
    dsc = [[]],
    act = function (s)
        if check_skill( me().skills.agility, 0 ) then
            pn [[]]
        else
            pn [[]]
        end
        objs():del(s)
    end
};

enc_misk_admin_building_3 = obj {
    nam = "encounter",
    dsc = [[]],
    act = function (s)
        if check_skill( me().skills.agility, 0 ) then
            pn [[]]
        else
            pn [[]]
        end
        objs():del(s)
    end
};

enc_misk_admin_building_4 = obj {
    nam = "encounter",
    dsc = [[]],
    act = function (s)
        if check_skill( me().skills.agility, 0 ) then
            pn [[]]
        else
            pn [[]]
        end
        objs():del(s)
    end
};

enc_misk_admin_building_5 = obj {
    nam = "encounter",
    dsc = [[]],
    act = function (s)
        if check_skill( me().skills.agility, 0 ) then
            pn [[]]
        else
            pn [[]]
        end
        objs():del(s)
    end
};

enc_misk_admin_building_6 = obj {
    nam = "encounter",
    dsc = [[]],
    act = function (s)
        if check_skill( me().skills.agility, 0 ) then
            pn [[]]
        else
            pn [[]]
        end
        objs():del(s)
    end
};

enc_misk_admin_building_7 = obj {
    nam = "encounter",
    dsc = [[]],
    act = function (s)
        if check_skill( me().skills.agility, 0 ) then
            pn [[]]
        else
            pn [[]]
        end
        objs():del(s)
    end
};