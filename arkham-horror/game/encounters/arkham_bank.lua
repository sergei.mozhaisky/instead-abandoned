-- encounters - bank

encounter_bank = function ()
    el = {
        enc_bank_1,
        enc_bank_2,
        enc_bank_3,
        enc_bank_4,
        enc_bank_5,
        enc_bank_6,
        enc_bank_7,
    }
    return el[rnd(7)]
end

enc_bank_1 = obj {
    nam = "encounter",
    dsc = [[]],
    act = function (s)
        if check_skill( me().skills.agility, 0 ) then
            pn [[]]
        else
            pn [[]]
        end
        objs():del(s)
    end
};

enc_bank_2 = obj {
    nam = "encounter",
    dsc = [[]],
    act = function (s)
        if check_skill( me().skills.agility, 0 ) then
            pn [[]]
        else
            pn [[]]
        end
        objs():del(s)
    end
};

enc_bank_3 = obj {
    nam = "encounter",
    dsc = [[]],
    act = function (s)
        if check_skill( me().skills.agility, 0 ) then
            pn [[]]
        else
            pn [[]]
        end
        objs():del(s)
    end
};

enc_bank_4 = obj {
    nam = "encounter",
    dsc = [[]],
    act = function (s)
        if check_skill( me().skills.agility, 0 ) then
            pn [[]]
        else
            pn [[]]
        end
        objs():del(s)
    end
};

enc_bank_5 = obj {
    nam = "encounter",
    dsc = [[]],
    act = function (s)
        if check_skill( me().skills.agility, 0 ) then
            pn [[]]
        else
            pn [[]]
        end
        objs():del(s)
    end
};

enc_bank_6 = obj {
    nam = "encounter",
    dsc = [[]],
    act = function (s)
        if check_skill( me().skills.agility, 0 ) then
            pn [[]]
        else
            pn [[]]
        end
        objs():del(s)
    end
};

enc_bank_7 = obj {
    nam = "encounter",
    dsc = [[]],
    act = function (s)
        if check_skill( me().skills.agility, 0 ) then
            pn [[]]
        else
            pn [[]]
        end
        objs():del(s)
    end
};