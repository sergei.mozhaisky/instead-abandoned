-- encounters - river port

encounter_river_port = function ()
    el = {
        enc_river_port_1,
        enc_river_port_2,
        enc_river_port_3,
        enc_river_port_4,
        enc_river_port_5,
        enc_river_port_6,
        enc_river_port_7,
    }
    return el[rnd(7)]
end

enc_river_port_1 = obj {
    nam = "encounter",
    dsc = [[]],
    act = function (s)
        if check_skill( me().skills.agility, 0 ) then
            pn [[]]
        else
            pn [[]]
        end
        objs():del(s)
    end
};

enc_river_port_2 = obj {
    nam = "encounter",
    dsc = [[]],
    act = function (s)
        if check_skill( me().skills.agility, 0 ) then
            pn [[]]
        else
            pn [[]]
        end
        objs():del(s)
    end
};

enc_river_port_3 = obj {
    nam = "encounter",
    dsc = [[]],
    act = function (s)
        if check_skill( me().skills.agility, 0 ) then
            pn [[]]
        else
            pn [[]]
        end
        objs():del(s)
    end
};

enc_river_port_4 = obj {
    nam = "encounter",
    dsc = [[]],
    act = function (s)
        if check_skill( me().skills.agility, 0 ) then
            pn [[]]
        else
            pn [[]]
        end
        objs():del(s)
    end
};

enc_river_port_5 = obj {
    nam = "encounter",
    dsc = [[]],
    act = function (s)
        if check_skill( me().skills.agility, 0 ) then
            pn [[]]
        else
            pn [[]]
        end
        objs():del(s)
    end
};

enc_river_port_6 = obj {
    nam = "encounter",
    dsc = [[]],
    act = function (s)
        if check_skill( me().skills.agility, 0 ) then
            pn [[]]
        else
            pn [[]]
        end
        objs():del(s)
    end
};

enc_river_port_7 = obj {
    nam = "encounter",
    dsc = [[]],
    act = function (s)
        if check_skill( me().skills.agility, 0 ) then
            pn [[]]
        else
            pn [[]]
        end
        objs():del(s)
    end
};