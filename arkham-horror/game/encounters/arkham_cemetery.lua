-- encounters - independence square

encounter_cemetery = function ()
    el = {
        enc_cemetery_1,
        enc_cemetery_2,
        enc_cemetery_3,
        enc_cemetery_4,
        enc_cemetery_5,
        enc_cemetery_6,
        enc_cemetery_7,
    }
    return el[rnd(7)]
end

enc_cemetery_1 = obj {
    nam = "encounter",
    dsc = [[]],
    act = function (s)
        if check_skill( me().skills.agility, 0 ) then
            pn [[]]
        else
            pn [[]]
        end
        objs():del(s)
    end
};

enc_cemetery_2 = obj {
    nam = "encounter",
    dsc = [[]],
    act = function (s)
        if check_skill( me().skills.agility, 0 ) then
            pn [[]]
        else
            pn [[]]
        end
        objs():del(s)
    end
};

enc_cemetery_3 = obj {
    nam = "encounter",
    dsc = [[]],
    act = function (s)
        if check_skill( me().skills.agility, 0 ) then
            pn [[]]
        else
            pn [[]]
        end
        objs():del(s)
    end
};

enc_cemetery_4 = obj {
    nam = "encounter",
    dsc = [[]],
    act = function (s)
        if check_skill( me().skills.agility, 0 ) then
            pn [[]]
        else
            pn [[]]
        end
        objs():del(s)
    end
};

enc_cemetery_5 = obj {
    nam = "encounter",
    dsc = [[]],
    act = function (s)
        if check_skill( me().skills.agility, 0 ) then
            pn [[]]
        else
            pn [[]]
        end
        objs():del(s)
    end
};

enc_cemetery_6 = obj {
    nam = "encounter",
    dsc = [[]],
    act = function (s)
        if check_skill( me().skills.agility, 0 ) then
            pn [[]]
        else
            pn [[]]
        end
        objs():del(s)
    end
};

enc_cemetery_7 = obj {
    nam = "encounter",
    dsc = [[]],
    act = function (s)
        if check_skill( me().skills.agility, 0 ) then
            pn [[]]
        else
            pn [[]]
        end
        objs():del(s)
    end
};