-- $Name:Openworld$
instead_version "2.2.0"
require "dash"
require "para"
require "quotes"



local WM = {};
WM.fade = function(t) return t*t*t*(t*(t*6-15)+10) end
WM.lerp = function(t,a,b) return a+t*(b-a) end
WM.grad = function(hash,x,y,z)
  local h,u,v,r=hash % 16,nil,nil,nil
  if (h<8) then u=x else u=y end
  if (h<4) then v=y elseif (h==12 or h==14) then v=x else v=z end
  if ((h%2)==0) then r=u else r=-u end
  if ((h%4)==0) then r=r+v else r=r-v end
  return r
end
WM.noise = function(x,y,z)
  local X,Y,Z=math.floor(x%255),math.floor(y%255),math.floor(z%255)
  x=x-math.floor(x)
  y=y-math.floor(y)
  z=z-math.floor(z)
  local u,v,w=WM.fade(x),WM.fade(y),WM.fade(z)
  A=WM.p[X]+Y;AA=WM.p[A]+Z;AB=WM.p[A+1]+Z;B=WM.p[X+1]+Y;BA=WM.p[B]+Z;BB=WM.p[B+1]+Z
  return WM.lerp(w,WM.lerp(v,WM.lerp(u,WM.grad(WM.p[AA],x,y,z),WM.grad(WM.p[BA],x-1,y,z)),WM.lerp(u,WM.grad(WM.p[AB],x,y-1,z),WM.grad(WM.p[BB],x-1,y-1,z))),WM.lerp(v,WM.lerp(u,WM.grad(WM.p[AA+1],x,y,z-1),WM.grad(WM.p[BA+1],x-1,y,z-1)),WM.lerp(u,WM.grad(WM.p[AB+1],x,y-1,z-1),WM.grad(WM.p[BB+1],x-1,y-1,z-1))))
end;
WM.generate = function(xs,ys)
WM.p = {}
local map = {
 scale = 8;
 dx = 190;
 dy = 190;
};
math.randomseed( os.time() )
--math.randomseed( 123456 )
for i=1,256 do
 local r=math.random(256);
 WM.p[i],WM.p[256+i]=r,r
end
local worldmap={}
for x=1,xs do
  worldmap[x]={}
  for y=1,ys do
    d = math.floor(WM.noise(x/map.scale + map.dx, y/map.scale + map.dy,0) * 8 + 3)
    if d > 9  then d = 9 end
    if d <= 0 then d = 0 end
    worldmap[x][y]=d;
  end
end
return worldmap
end;

WM.landtype = function(i)
local land = {
  [0] = "Вода",
  [1] = "Луга",
  [2] = "Рощи",
  [3] = "Рощи",
  [4] = "Холмы",
  [5] = "Лес",
  [6] = "Лес",
  [7] = "Скалы",
  [8] = "Горы",
  [9] = "Горы",
  [10] = "Горы",
};
return land[i]
end

WM.landimg = function(i)
local land = {
  [0] = "#0000FF",
  [1] = "#90EE90",
  [2] = "#8FBC8F",
  [3] = "#006400",
  [4] = "#32CD32",
  [5] = "#008000",
  [6] = "#556B2F",
  [7] = "brown",
  [8] = "brown",
  [9] = "white",
  [10] = "red",
};
return img("box:17x17,"..land[i]..",255")
end



global {
x = 10;
y = 10;
map = WM.generate(20,20);
};


north = room {
    nam = 'N';
    enter = function(s)
        y = y - 1;
        walk(main);
    end;
};

east = room {
    nam = 'E';
    enter = function(s)
        x = x + 1;
        walk(main);
    end;
};

south = room {
    nam = 'S';
    enter = function(s)
        y = y + 1;
        walk(main);
    end;
};

west = room {
    nam = 'W';
    enter = function(s)
        x = x - 1;
        walk(main);
    end;
};


main = room {
    forcedsc = true;
    nam = 'World';
    dsc = function(s)
        pn(WM.landtype(map[x][y]));
        pn("Координаты: x: " .. x .. ", y: " .. y);
        pn("На севере: " .. WM.landtype(map[x][y-1]));
        pn("На востоке: " .. WM.landtype(map[x+1][y]));
        pn("На юге: " .. WM.landtype(map[x][y+1]));
        pn("На западе: " .. WM.landtype(map[x-1][y]));

--full map
--[[	
for ty=1,40 do
  for tx=1,40 do
    tile = map[tx][ty];
    if tx == x and ty == y then tile = 10 end
    pr(WM.landimg(tile));
  end
  pn("");
end
]]--

-- minimap

mapsize=5;
for ty=y-mapsize,y+mapsize do
  for tx=x-mapsize,x+mapsize do
    tile = map[tx][ty];
    if tx == x and ty == y then tile = 10 end
    pr(WM.landimg(tile));
  end
  pn("");
end

	--pn(img('box:8x8,red,255') .. img('box:8x8,blue,255'));
    end;
    way = {
        vroom('Север', north),
        vroom('Восток', east),
        vroom('Юг', south),
        vroom('Запад', west),
    };
};
