-- $Name:Labyrinth$
instead_version "2.2.0"
require "dash"
require "para"
require "quotes"

require "functions"
require "maze"

global {
	loc_player = {
		x = 3;
		y = 3;
    move = 'n';
	};
	loc_monster = {
		x = 2;
		y = 3;
		mm = ''; -- last movement
    nm = ''; -- next movement
    time_to_move = false;
	};
  move_ctrl = {
    F = {},
    B = {},
    L = {},
    R = {}
  };
  _maze_map = {}
};






local Monster = {};

Monster.move = function()
  if loc_monster.time_to_move then
    movement = Monster.get_next_move()
    if movement == 'n' then loc_monster.y = loc_monster.y - 1 end
    if movement == 's' then loc_monster.y = loc_monster.y + 1 end
    if movement == 'e' then loc_monster.x = loc_monster.x + 1 end
    if movement == 'w' then loc_monster.x = loc_monster.x - 1 end
    loc_monster.mm = movement
    Monster.set_next_move()
    loc_monster.time_to_move = false
  else
    loc_monster.time_to_move = true
  end
end

Monster.location = function(x,y)
  if x == loc_monster.x and y == loc_monster.y then
    return true
  else
    return false
  end
end

Monster.get_next_move = function()
  return loc_monster.nm
end

Monster.set_next_move = function()
local op = {
  ['n'] = 's',
  ['s'] = 'n',
  ['w'] = 'e',
  ['e'] = 'w',
};
local mm = loc_monster.mm;
-- get possible ways from current location
local pways = Maze.getways_xy( loc_monster.x, loc_monster.y );
local ppath = {};
if pways[1] and mm ~= op['n'] then table.insert(ppath, 'n') end
if pways[2] and mm ~= op['e'] then table.insert(ppath, 'e') end
if pways[3] and mm ~= op['s'] then table.insert(ppath, 's') end 
if pways[4] and mm ~= op['w'] then table.insert(ppath, 'w') end
if #ppath == 0 then
	movement = op[movement];
else
	movement = trnd(ppath);
end
loc_monster.nm = movement
end






move_north = room {
    nam = 'N';
    enter = function(s)
      loc_player.y = loc_player.y - 1
      loc_player.move = 'n'
      walk(maze_tile)
    end;
};

move_east = room {
    nam = 'E';
    enter = function(s)
      loc_player.x = loc_player.x + 1;
      loc_player.move = 'e'
      walk(maze_tile);
    end;
};

move_south = room {
    nam = 'S';
    enter = function(s)
      loc_player.y = loc_player.y + 1;
      loc_player.move = 's'
      walk(maze_tile)
    end;
};

move_west = room {
    nam = 'W';
    enter = function(s)
      loc_player.x = loc_player.x - 1
      loc_player.move = 'w'
      walk(maze_tile)
    end;
};


playermap = stat {
	nam = 'MAP';
	disp = function(s)
		pn(loc_player.x .. ":" .. loc_player.y)
		Maze.render()
	end;
};

start = function()
	--math.randomseed( 1234 )
  math.randomseed( os.time() )
	Maze.generate()
  Maze.put_player();
	take(playermap)
end





obj_way_F = obj {
  var {
    path = 'maze';
    t = 'door'; -- or 'path'
    w = 'F';
    in_room = false;
    active = false;
  };
  nam = 'obj_way_F';
  dsc = function(s)
    if s.active then
      pn( txtc("{Вперёд}") );
    else
      pn( txtc("Вперёд") );
    end
  end;
  act = function(s)
    walk(s.path);
  end;
}

obj_way_L = obj {
  var {
    path = 'maze';
    t = 'door'; -- or 'path'    
    w = 'L';
    in_room = false;
    active = false;
  };
  nam = 'obj_way_L';
  dsc = function(s)
    if s.active then
      pr( "{Налево}" );
    else
      pr( "Налево" );
    end
  end;
  act = function(s)
    walk(s.path);
  end;
}

obj_way_R = obj {
  var {
    path = 'maze';
    t = 'door'; -- or 'path'
    w = 'R';
    in_room = false;
    active = false;
  };
  nam = 'obj_way_R';
  dsc = function(s)
    if s.active then
      pr( "{Направо}" );
    else
      pr( "Направо" );
    end
  end;
  act = function(s)
    walk(s.path);
  end;
}


obj_way_B = obj {
  var {
    path = 'maze';
    t = 'door'; -- or 'path'
    w = 'B';
    in_room = false;
    active = false;
  };
  nam = 'obj_way_B';
  dsc = function(s)
    if s.active then
      pn( txtc("{Назад}") );
    else
      pn( txtc("Назад") );
    end
  end;
  act = function(s)
    walk(s.path);
  end;
}

movement_controls = obj {
  nam = 'controls',
  dsc = nil,
  obj = {
    obj_way_F, -- way forward
    obj {nam = '\n', dsc = txtc("|^") },
    obj_way_L, -- way left
    obj {nam = '\n', dsc = " -- " },
    obj {nam = '', dsc = " {Ждать} ", act = function(s) walk('maze_tile') end },
    -- obj {nam ='compass', dsc = function(s) p(txtc(img("gfx/compass230.png"))) end },
    obj {nam = '\n', dsc = " -- " },
    obj_way_R, -- way right
    obj {nam = '\n', dsc = txtc("^|^") },
    obj_way_B, -- way back
  }
}




-- max for each room type = 10
main = room {
  forcedsc = true;
  nam = 'GAME';
  dsc = function(s)
    pn 'Start game';
  end;
  way = {
		'maze_tile',
    'benchmark'
  };
}


maze_tile = room {
  var {
    waydescr = '';
    monster_descr = '';
  };
  forcedsc = true;
  nam = 'MAZE';
	entered = function(s)
    local tile = Maze.map( loc_player.x, loc_player.y )
    local dirs = Maze.get_directions(loc_player.move)
    
    s.waydescr = ''
    s.monster_descr = ''
    
    for k, v in pairs(tile['ways']) do
      obj_id = Maze.dir_2_obj( dirs[k] )
      _G[obj_id].active = false
      if v then
        -- enable directions
        _G[obj_id].path = Maze.dir_2_obj( k ) -- room name
        _G[obj_id].w    = dirs[k]             -- path value
        _G[obj_id].t    = v                   -- room name
        _G[obj_id].in_room = Maze.is_room(tile['tile'])
        _G[obj_id].active = true
        s.waydescr = s.waydescr .. Maze.get_way_descr(v, dirs[k], Maze.is_room(tile['tile']))
      end
    end
    
    local nesw = Maze.get_nesw_tiles(loc_player.x, loc_player.y);
    local mnm = Monster.get_next_move()
    for k, v in pairs(nesw) do
      if v then
        if Monster.location(v.x, v.y) then
          s.monster_descr = "МОНСТР где-то рядом -- " .. k .. "|" .. mnm
          if (k == 1 and mnm == 's') or (k == 2 and nmn == 'w') or (k == 3 and mnm == 'n') or (k == 4 and mnm == 'e') then
            s.monster_descr = s.monster_descr .. rtxt("[шум приближается/шаги становятся все громче/звук шагов все ближе]")
          else 
            s.monster_descr = s.monster_descr .. rtxt("[шум удаляется/шаги постепенно затихают/звук шагов удаляется/слышен звук удаляющихся шагов]")
          end
        --s.monster_descr = v
        end
      end
    end
	end;
  
  enter = function()
  --[[
    Monster.move();
    if Monster.location(loc_player.x, loc_player.y) then
      walk(gamedeath);
        -- s.monster_descr = "!!! МОНСТР В КОМНАТЕ !!! -- " .. Monster.get_next_move()
        --s.monster_descr = v
    end
  ]]--
  end;
  dsc = function(s)
		pn(s.monster_descr .. "^");
    
    local tile = Maze.map( loc_player.x, loc_player.y )
		pn(tile['short_descr'])
    pn(s.waydescr)
    
    pn("Дверей " .. tile['doors_num'])
    pn("Проходов " .. tile['paths_num'])

  end;
	obj = {
    movement_controls
	};
  way = {
    move_north,
    move_south,
    move_east,
    move_west,
  };
};

gamedeath = room {
	forcedsc = true;
	nam = "Все кончено";
	dsc = function(s)
    p([[Последнее, что я успел почувствовать - адская боль.]])
	end;
	way = {
		'main'
	};
};



gameover = room {
	forcedsc = true;
	nam = "GAME OVER";
	dsc = function(s)

	end;
	way = {
		'main'
	};
};


benchmark = room {
    forcedsc = true;
    nam = 'BENCHMARK';
    dsc = function(s)
		max_xtypes = {};
	
	for z = 1, 10000 do
		Maze.generate();
		xtypes = {};
		
		for ty=1,5 do
			for tx=1,5 do
				tile = Maze.get_tile(tx,ty);
				if xtypes[tile] == nil then
					xtypes[tile] = 1
				else
					xtypes[tile] = xtypes[tile] + 1
				end
			end
		end	
		for i,v in pairs(xtypes) do
				if max_xtypes[i] == nil then
					max_xtypes[i] = v
				else
					if xtypes[i] > max_xtypes[i] then
					max_xtypes[i] = xtypes[i]
					end
				end
		end;
		
end
		for i,v in pairs(max_xtypes) do
			pn(Maze.display(i) .. " " .. i .. " : " .. v);
		end

    end;
    way = {
        vroom('BACK', main),
    };
};



intro_1 = room {
  forcedsc = true;
  nam = '...';
  dsc = function(s)
    pn([[
    Тишина.^
    Темнота.^
    Я медленно прихожу в себя, лежа на твердом холодном полу. Упасть с высоты нескольких метров ...^
    Где-то здесь должен быть фонарь...
    ]])
  end;
}


