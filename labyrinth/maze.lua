﻿Maze = {}

Maze.rooms_maze = {}

-- work with global 'maze_map'
Maze.map = function(x,y)
  return _maze_map[x][y]
end

-- accessors
Maze.get_tile = function (x,y)
	return _maze_map[x][y]['tile']
end

Maze.set_tile = function (x,y,t)
	_maze_map[x][y]['tile'] = t
end

Maze.set_door = function (x,y,w)
	_maze_map[x][y]['ways'][w] = 'door';
  _maze_map[x][y]['doors_num'] = _maze_map[x][y]['doors_num'] + 1;
end

Maze.set_path = function (x,y,w)
	_maze_map[x][y]['ways'][w] = 'path';
  _maze_map[x][y]['paths_num'] = _maze_map[x][y]['paths_num'] + 1;
end

-- check if it's room
Maze.is_room = function (t)
  if t > 10 and t < 20 then
    return true;
  end
  return false;
end
  
  
Maze.has_path = function(t, dir)
  if dir == 'n' and (t == 1 or t == 13 or t == 22 or t == 24 or t == 26 or t == 31 or t == 32 or t == 34) then
    return true
  end
  if dir == 'e' and (t == 1 or t == 14 or t == 23 or t == 25 or t == 26 or t == 31 or t == 32 or t == 33) then
    return true
  end
  if dir == 's' and (t == 1 or t == 11 or t == 21 or t == 22 or t == 23 or t == 32 or t == 33 or t == 34) then
    return true
  end
  if dir == 'w' and (t == 1 or t == 12 or t == 21 or t == 24 or t == 25 or t == 31 or t == 33 or t == 34) then
    return true
  end
  return false
end


-- get possible ways from current location
Maze.getways = function(n,e,s,w)
  return { Maze.has_path(n, 'n'), Maze.has_path(e, 'e'), Maze.has_path(s, 's'), Maze.has_path(w, 'w') }
end;


-- get adjacent tiles type
Maze.get_nesw = function(x,y)
  if x == 1 then w = 0 else w = Maze.get_tile( x-1, y ) end
  if x == 5 then e = 0 else e = Maze.get_tile( x+1, y ) end
  if y == 1 then n = 0 else n = Maze.get_tile( x, y-1 ) end
  if y == 5 then s = 0 else s = Maze.get_tile( x, y+1 ) end
  return {n,e,s,w}
end

-- get adjacent tiles
Maze.get_nesw_tiles = function(x,y)
  if x == 1 then w = nil else w = _maze_map[x-1][y] end
  if x == 5 then e = nil else e = _maze_map[x+1][y] end
  if y == 1 then n = nil else n = _maze_map[x][y-1] end
  if y == 5 then s = nil else s = _maze_map[x][y+1] end
  return {n,e,s,w}
end


-- get possible ways for given coordinates
Maze.getways_xy = function(x,y)
	return Maze.getways( unpack( Maze.get_nesw(x,y) ) );
end;


-- generate cell for given ways configuration
Maze.generate_cell = function(n,e,s,w)
  cways = Maze.getways(n,e,s,w)
  n_p = cways[1]
  e_p = cways[2]
  s_p = cways[3]
  w_p = cways[4]
  -- if path exists
  pc = 0
  if n_p and e_p and s_p and w_p then
    pc = 1
  elseif e_p and s_p and w_p then
    pc = 31
  elseif n_p and s_p and w_p then
    pc = 32
  elseif n_p and e_p and w_p then
    pc = 33
  elseif n_p and e_p and s_p then
    pc = 34
  elseif n_p and e_p then
    pc = 21
  elseif n_p and s_p then
    pc = 22
  elseif n_p and w_p then
    pc = 23
  elseif e_p and s_p then
    pc = 24
  elseif e_p and w_p then
    pc = 25
  elseif s_p and w_p then
    pc = 26
  --[[
  elseif n_p then
    pc = 11;
  elseif e_p then
    pc = 12;
  elseif s_p then
    pc = 13;
  elseif w_p then
    pc = 14;
  ]]--
  end
  return pc
end


Maze.map_init = function()
  for tx = 1,5 do
    _maze_map[tx] = {}
    for ty = 1,5 do
      _maze_map[tx][ty] = {
        x = tx,
        y = ty,
        tile = 1,
        name = '',
        short_descr = '',
        descr = '',
        ways = { n = false, e = false, s = false, w = false },
        paths_num = 0,
        doors_num = 0,
      }
    end
  end
end

Maze.map_fill = function()

  Maze.set_tile( 1, 1, trnd({12,13}) )
  Maze.set_tile( 5, 1, trnd({13,14}) )
  Maze.set_tile( 1, 5, trnd({11,12}) )
  Maze.set_tile( 5, 5, trnd({11,14}) )
  for k,v in pairs({ {2,2}, {4,2}, {2,4}, {4,4} }) do
    Maze.set_tile( v[1], v[2], trnd({11,12,13,14}) )
  end

  -- remove random rooms
  z = trnd({ {1,1}, {5,1}, {1,5}, {5,5} }); Maze.set_tile( z[1], z[2], 0 )
  z = trnd({ {2,2}, {4,2}, {2,4}, {4,4} }); Maze.set_tile( z[1], z[2], trnd({0,21,22,23,24,25,26,31,32,33,34}) )

  local cells = {
    {2,1}, {4,1}, {1,2}, {5,2},
    {1,4}, {5,4}, {2,5}, {4,5},
    {3,1}, {3,2}, {1,3}, {2,3},
    {5,3}, {4,3}, {3,5}, {3,4},
    {3,3},
  }

  for k,v in pairs(cells) do
    local cell = Maze.generate_cell( unpack( Maze.get_nesw(v[1],v[2]) ) )
    Maze.set_tile( v[1], v[2], cell )
  end

end

Maze.map_doors = function()
  -- set doors and ways
  for tx = 1,5 do
    for ty = 1,5 do
      local nesw  = Maze.get_nesw( tx,ty )      
      if nesw[1] == 13 then
        Maze.set_door(tx, ty, 'n')
      else
        if Maze.has_path(nesw[1], 'n') then
          Maze.set_path(tx, ty, 'n')
        end
      end
      if nesw[2] == 14 then
        Maze.set_door(tx, ty, 'e')
      else
        if Maze.has_path(nesw[2], 'e') then
          Maze.set_path(tx, ty, 'e')
        end
      end
      if nesw[3] == 11 then
        Maze.set_door(tx, ty, 's')
      else
        if Maze.has_path(nesw[3], 's') then
          Maze.set_path(tx, ty, 's')
        end
      end
      if nesw[4] == 12 then
        Maze.set_door(tx, ty, 'w')
      else
        if Maze.has_path(nesw[4], 'w') then
          Maze.set_path(tx, ty, 'w')
        end
      end
    end
  end

end


Maze.generate = function()
  Maze.map_init()   -- init map with default values
  Maze.map_fill()   -- generate maze
  Maze.map_doors()  -- map doors and ways

  local room_list = shuffle({"chemlab","archive","library","storage","medlab","ventchamber","electro","bedroom"})
  local rooms_maze = {"exit"}
  for i = 1,5 do
    table.insert(rooms_maze, table.remove(room_list))
  end
  Maze.rooms_maze = shuffle(rooms_maze)
  local rooms_all = Maze.rooms_maze;
  
  
  for tx = 1,5 do
    for ty = 1,5 do
      local tile = Maze.map(tx,ty)
      if tile['paths_num'] == 4 then
        _maze_map[tx][ty]['short_descr'] = 'Перекресток.'
      elseif tile['paths_num'] == 3 then
        _maze_map[tx][ty]['short_descr'] = 'Развилка.'
      elseif tile['paths_num'] == 2 then
        if (tile['ways']['e'] == 'path' and tile['ways']['w'] == 'path') or (tile['ways']['s'] == 'path' and tile['ways']['n'] == 'path') then
          _maze_map[tx][ty]['short_descr'] = 'Коридор.'
        else
          _maze_map[tx][ty]['short_descr'] = 'Поворот.'
        end
      elseif tile['paths_num'] == 1 then
        if Maze.is_room(tile['tile']) then
          --local room_entry = table.remove(rooms_all)
          --_maze_map[tx][ty]['short_descr'] = 'КОМНАТА. => ' .. room_entry 
        else
          _maze_map[tx][ty]['short_descr'] = 'Тупик.'
        end
      end
    end
  end

end



--[[

Комнаты:

0. Выход - камера с лестницей наверх

1. Химическая лаборатория
2. Архив документов
3. Хранилище
4. Операционная
5. Вентиляционная камера
6. Водоснабжение
7. Электрощитовая, генератор
8. Комната отдыха
9. Комната связи






Камера с высоким куполообразным потолком.

Это большая комната. 

Бетонные стены.

Стены выкрашены зеленоватой краской, облупившейся от времени.
Зеленоватая краска на стенах.
Стены покрыты плесенью.


Доски свалены штабелем у стены.


Пол усеян обломками стекла.
На полу лужи.
Пол зарос мхом.
По полу разбросаны бумаги. Прочесть их не представляется возможным - буквы расплылись от сырости.

Под потолком проложены трубы.

Посреди комнаты стоит ржавая металлическая бочка.
Вдоль стены стоит несколько металлических бочек.
Ржавые железные бочки брошены на полу. 


У одной из стен в несколько рядов стоят деревянные ящики.
На полу валяются остатки разломанной радиоаппаратуры.

У стены стоит пустой стеллаж.
У стены стоит шкаф с химическими препаратами.

Несколько труб выходят из стены и уходят в пол.

Сложная мешанина из труб, ведущих во всех направлениях.

Обгорелый остов какого-то механизма.

Тщательно заваренная дверь.


Дизель-генератор, трубы вентиляции и баки с топливом.

(Два/Три/Четыре) (небольших/больших/огромных) бака, (слегка тронутых/покрытых) ржавчиной.



Вентиляционная установка занимает почти все пространство. 
Толстые квадратные трубы идут от установки и уходят в стены.


В комнате стоит четыре двухъярусные кровати.


У стены стоят щиты с электрооборудованием.

Пульт связи, с двумя телефонами.



]]--



Maze.display = function(i,p,m)
  local tile = {
    [0]  = "gfx/maze_0.png",
    [11] = "gfx/maze_1_n.png",
    [12] = "gfx/maze_1_e.png",
    [13] = "gfx/maze_1_s.png",
    [14] = "gfx/maze_1_w.png",
    [21] = "gfx/maze_2_ne.png",
    [22] = "gfx/maze_2_ns.png",
    [23] = "gfx/maze_2_nw.png",
    [24] = "gfx/maze_2_es.png",
    [25] = "gfx/maze_2_ew.png",
    [26] = "gfx/maze_2_sw.png",
    [31] = "gfx/maze_3_esw.png",
    [32] = "gfx/maze_3_nsw.png",
    [33] = "gfx/maze_3_new.png",
    [34] = "gfx/maze_3_nes.png",
    [1]  = "gfx/maze_4.png",
  }
  
  image = tile[i];
  if p then
    image = image .. ';gfx/player.png@0,0';
  end
  if m then
    image = image .. ';gfx/monster1.png@0,0';
  end
  return img(image);
end

Maze.render = function()
  local is_player;
  local is_monster;
  local tile;
  for ty=1,5 do
    for tx=1,5 do
      is_player = false;
      is_monster = false;
      if loc_player.x == tx and loc_player.y == ty then
        is_player = true;
      end
      if loc_monster.x == tx and loc_monster.y == ty then
        is_monster = true;
      end			
      tile = Maze.get_tile( tx, ty );
      pr(Maze.display(tile,is_player,is_monster));
    end	
    pn("");	
  end
  pn("--")
  for k,v in pairs(Maze.rooms_maze) do
    pn(v)
  end
end


Maze.directions = {
  n = {n = 'F', e = 'R', s = 'B', w = 'L'},
  e = {n = 'L', e = 'F', s = 'R', w = 'B'},
  s = {n = 'B', e = 'L', s = 'F', w = 'R'},
  w = {n = 'R', e = 'B', s = 'L', w = 'F'}
}

Maze.dir2word = {
  F = 'впереди',
  B = 'сзади',
  R = 'справа',
  L = 'слева'
}

Maze.dir2obj = {
  F = 'obj_way_F',
  B = 'obj_way_B',
  R = 'obj_way_R',
  L = 'obj_way_L',
  n = 'move_north',
  e = 'move_east',
  s = 'move_south',
  w = 'move_west',
}

Maze.get_directions = function(dir)
  return Maze.directions[dir]
end

Maze.dir_2_word = function(dir_code)
  return Maze.dir2word[dir_code]
end

Maze.dir_2_obj = function(dir_code)
  return Maze.dir2obj[dir_code]
end


Maze.get_way_descr = function(t, w, is_room)
  local descr = ''
  local wtype = ''
  
  if is_room then
    return "Выход из комнаты за моей спиной."
  end
  
  if t == 'door' then
    wtype = 'дверь'
  else
    wtype = 'коридор'
  end

  if w == 'F' then
    descr = "Впереди " .. wtype .. "."
  elseif w == 'B' then
    descr = "Сзади " .. wtype .. "."
  elseif w == 'L' then
    descr = "Слева " .. wtype .. "."
  elseif w == 'R' then
    descr = "Справа " .. wtype .. "."
  end
  
  return descr
end

-- put player to random location
Maze.put_player = function()
  while true do
    z = trnd({ {3,1}, {1,3}, {5,3}, {3,5} });
    if Maze.get_tile(z[1], z[2]) ~= 0 then
      loc_player.x = z[1]
      loc_player.y = z[2]
      loc_player.move = trnd({ 'n', 'e', 's', 'w' })
      break
    end
  end
end


--[[
Maze.tile_descr = function(tile)
local descr = ''
if tile == 1 then
	descr = rtxt("В этой [комнате/камере] сходится несколько [путей/коридоров/тоннелей].");
elseif tile == 21 or tile == 23 or tile == 24 or tile == 26 then
	-- поворот
	descr = rtxt("Тоннель поворачивает под прямым углом.");
elseif tile == 31 or tile == 32 or tile == 33 or tile == 34 then
	-- развилка
	descr = rtxt("Здесь сходятся три тоннеля.");
end

return descr
end


]]--