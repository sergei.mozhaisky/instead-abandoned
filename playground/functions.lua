-- incr(table, key, value)
-- decr(table, key, value)
-- rs(array_of_strings) : returns random string from array

function incr(t,k,v) t[k]=t[k]+(v or 1) end
function decr(t,k,v) t[k]=t[k]-(v or 1) end
function rs(a) return a[rnd(table.getn(a))] end