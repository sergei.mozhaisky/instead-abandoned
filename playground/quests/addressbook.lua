-- $Name:Addressbook$
instead_version "2.2.0"
require "dash"
require "para"
require "quotes"

adb_loc = function(loc_name, loc_fullname)
	local v = {};
	stead.add_var(v, { active = true })
	stead.add_var(v, { fullname = loc_fullname })
	v.nam = loc_name;
	v.dsc = function(s)
		if s.active then
			pn("{" .. s.fullname .. "}");
		else
			pn(txtst(s.fullname));
		end
	end;
	v.act = function(s)
		s.active = false;
		p("Я отметил адрес на карте.");
	end;
	return obj(v);
end;



addressbook_content_a = obj {
	nam = "addressbook_content_a";
	dsc = function()
		pr(txtc("- А -"));
		pn();
	end;
	obj = {
		adb_loc('C11', 'Аркхемская лечебница имени Святой Марии');
		adb_loc('C17', 'Локсли-Холл');
		adb_loc('С19', 'Мискатоникский университет - Восточное общежитие');
		adb_loc('С23', 'Мискатоникский университет - Библиотека');
		adb_loc('С24', 'Мискатоникский университет - Выставочный зал');
		adb_loc('С25', 'Мискатоникский университет - Администрация');
	}
};

addressbook_content_b = obj {
	nam = "addressbook_content_b";
	dsc = function()
		pr(txtc("- Б -"));
		pn();
	end;
	obj = {
		adb_loc('D9', 'Ресторан "Пчела"');
		adb_loc('D32', 'Аркхемский полицейский участок');
		adb_loc('D45', 'Герберт Корбетт, криминалист');
		adb_loc('D67', 'Мэтью Кин');
		adb_loc('E29', 'Томас Илсли');
	}
};

addressbook_content_v = obj {
	nam = "addressbook_content_v";
	dsc = function()
		pr(txtc("- В -"));
		pn();
	end;
	obj = {
		adb_loc('V1', 'Вайолет, Дженифер');
	}
};

addressbook_content = obj {
	nam = "addressbook_content";
	dsc = "";
	obj = {
		addressbook_content_a:disable();
		addressbook_content_b:disable();
		addressbook_content_v:disable();
	};
};

c_open = function(o)
	pr(' ');
	addressbook_content:disable_all();
	o:enable();
end;

addressbook_index = obj {
	nam = "addressbook_index";
	dsc = txtc(" ");
	obj = {
		obj { nam = 'A', dsc = "{А} . ", act = function() c_open(addressbook_content_a); end };
		obj { nam = 'Б', dsc = "{Б} . ", act = function() c_open(addressbook_content_b); end };
		obj { nam = 'В', dsc = "{В} . ", act = function() c_open(addressbook_content_v); end };
		obj { nam = '1-10', dsc = "{1-10} ^^" };
	};
};


main = room {
	nam = 'Адресная книга';
	dsc = pr(' ');
	obj = { 
		addressbook_index;
		addressbook_content;
	};
};

