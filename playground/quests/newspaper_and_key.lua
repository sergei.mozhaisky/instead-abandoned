-- $Name:Newspaper_and_key$
instead_version "2.2.0"
require "dash"
require "para"
require "quotes"

dofile("quests/objects/screwdriver.lua");
dofile("quests/objects/knife.lua");

key_r10 = obj {
	nam = "Ключ R10";
	dsc = "Ключ от двери R10.";
    inv = function(s)
        p( "Ключ от двери R10." );
    end;
	act = function(s)
        take(s);
        p( "Я взял ключ R10." );
    end;
}:disable();

keyhole_r10 = obj {
	nam = "Замочная скважина";
	dsc = function(s)
		p( "В {замочной скважине} с другой стороны двери торчит ключ." );
	end;
	act = function(s)
		if key_r10:disabled() then
			p( "Мне нужно найти способ достать ключ с той стороны." );
		end
    end;
	used = function(s,w)
		if w == newspaper then
			p( "Вряд ли я смогу просунуть газету в скважину." );
		end
		if w == knife then
			if where(newspaper) == door_r10 then
				p( "Я просунул нож в скважину, выталкивая ключ." );
				key_r10:enable();
				keyhole_r10:disable();
			else
				p( "Если я просуну нож в скважину, ключ выпадет. Вот только достать его через щель под дверью я все равно не смогу. Нужно что-то придумать." );
			end
		end
		if w == screwdriver then
			if where(newspaper) == door_r10 then
				p( "Я просунул отвертку в скважину, выталкивая ключ." );
				key_r10:enable();
				keyhole_r10:disable();
			else
				p( "Если я просуну отвертку в скважину, ключ выпадет. Вот только достать его через щель под дверью я все равно не смогу. Нужно что-то придумать." );
			end
		end
	end;
};

door_r10 = obj {
	var {
		closed = true;
	};
	nam = "Дверь R10";
	dsc = function(s)
		p( "Обычная белая {дверь}" );
		if s.closed then 
			p( " закрыта." );
		else
			p( " открыта." );
		end
	end;
	act = function(s)
		if s.closed then
			if keyhole_r10:disabled() and key_r10:disabled() then
				p( "Дернув за ручку, я обнаружил, что дверь заперта.^" );
				p( "Я наклонился и посмотрел в замочную сважину. Сквозь нее был виден торчащий с той стороны ключ." );
				keyhole_r10:enable();
			else
				p( "Дверь заперта." );
			end
		else
			p( "Дверь открыта." );
		end
    end;
	used = function(s,w)
		if w == newspaper then
			p( "Я разложил газету и просунул ее под дверь." );
			drop(w, s);
		end
		if w == key_r10 then
			p( "Я вставил ключ в замок и открыл дверь." );
			s.closed = false;
			path_to_locked_room:enable();
			key_r10:disable();
		end
	end;
	obj = {
		keyhole_r10:disable();
	};
};

-- Quest objects

newspaper = obj {
	nam = "Газета";
	dsc = function(s)
		if where(s) == door_r10 then
			pr( "^Из-под двери торчит {газета}" );
			if not taken(key_r10) and not key_r10:disabled() then
				pr( ", на которую упал ключ с той стороны" );
			end
			p( "." );
		else
			p( "^На полу лежит {газета}." );
		end
	end;
    inv = function(s)
        p( "Это свежий выпуск 'Аркхем адвертайзер'." );
    end;
	act = function(s)
		if where(s) == door_r10 then
			pr( "Я вытащил газету из-под двери" );
			if not taken(key_r10) and not key_r10:disabled() then
				take(key_r10);
				pr( " вместе с лежащим на ней ключом" );
			end
			p( "." );
		else
			p( "Я подобрал газету." );
		end
        take(s);
    end;
	use = function(s,w)
		if w == knife then
			p( "Резать газету я не стал, решив, что она мне еще пригодится." );
		end
	end;
	used = function(s,w)
		if w == knife then
			p( "Резать газету я не стал, решив, что она мне еще пригодится." );
		end
	end;
};

path_to_locked_room = vroom('В комнату', 'room_locked'):disable();

main = room {
	forcedsc = true;
	nam = 'Newspaper and key';
	dsc = [[Комната.]];
	obj = { 
		door_r10;
		newspaper;
		knife;
		screwdriver;
	};
	way = {
		path_to_locked_room
	}
};

room_locked = room {
	forsedsc = true;
	nam = "Запертая комната";
	dsc = [[Я оказался в комнате.]];
	way = {
		vroom('Обратно', 'main');
	};
};