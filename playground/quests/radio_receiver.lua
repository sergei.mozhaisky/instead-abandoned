-- $Name:Radio receiver$
instead_version "2.2.0"
require "dash"
require "para"
require "quotes"

dofile("quests/objects/screwdriver.lua");
dofile("functions.lua");



radio_receiver = obj {
    var {
        is_on = false;
    };
    nam = "Радиоприемник";
    dsc = function(s)
        p( "Здесь стоит {радиоприемник}." );
        if s.is_on then
            p( " Индикатор питания светится красным." );
        end
    end;
    act = function(s)
        if s.is_on then
            p( "Я выключил приемник." );
            s.is_on = false;
            lifeoff(s);
        else
            p( "Я включил приемник." );
            s.is_on = true;
            lifeon(s);
        end
    end;
    life = function(s)
        if s.is_on then
            p( rs({"Играет негромкая музыка.","Передают выпуск новостей.","За шумом помех ничего не разобрать."}) );
        end
    end;
};


main = room {
    forcedsc = true;
    nam = 'test';
    dsc = function(s)
        p("Комната. ");
    end;
    obj = { 
        radio_receiver;
        screwdriver;
    };
    way = {
        vroom('Туда', 'room2');
    };
    entered = function(s)
        lifeon(radio_receiver);
    end;
    left = function(s)
        lifeoff(radio_receiver);
    end;
};

room2 = room {
    forsedsc = true;
    nam = "Комната";
    dsc = [[Тест.]];
    way = {
        vroom('Обратно', 'main');
    };
};

