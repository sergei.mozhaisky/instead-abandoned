-- $Name:Rope and Hole$
instead_version "2.2.0"
require "dash"
require "para"
require "quotes"

dofile("quests/objects/knife.lua");


floor_hole = obj {
    nam = "Дыра в полу";
    dsc = function(s)
        pr( "В полу {дыра}" );
        if where(rope) == floor_ring then
            pr( ", в которую теперь можно спуститься" );
        end
        p(".");
    end;
    act = function(s)
        if where(rope) == floor_ring then
            p( "Веревка держится крепко. Можно спускаться." );
        else
            p( "Похоже, тут довольно глубоко. Не хотелось бы сломать что-нибудь при прыжке." );
        end
    end;
    used = function(s,w)
        if w == rope then
            p( "Мне нужно закрепить канат перед тем, как бросать его в дыру." );
        end
    end;
};


floor_ring = obj {
    nam = "Дыра в полу";
    dsc = function(s)
        pr( "Недалеко от нее в пол вделано металлическое {кольцо}" );
        if where(rope) ~= s then
            p(".");
        end
    end;
    act = function(s)
        if where(rope) == s then
            p( "Канат крепко привязан к кольцу." );
        else
            p( "Я подергал кольцо. Похоже, оно довольно прочно закреплено." );
        end
    end;
    used = function(s,w)
        if w == rope then
            p( "Я привязал канат к кольцу." );
            drop(rope,floor_ring);
        end
    end;
};



-- Quest objects

rope_hanging = obj {
    nam = "Бельевая веревка";
    dsc = function(s)
        p( "Здесь висит {бельевая веревка}." );
    end;
    act = function(s)
        p( "Веревка довольно длинная и прочная." );
    end;
    used = function(s,w)
        if w == knife then
            p( "Я срезал веревку и забрал." );
            take(rope);
            s:disable(); -- убрать висящую веревку
        end
    end;
};

rope = obj {
    nam = "Веревка";
    dsc = function(s)
        if where(rope) == floor_ring then
            p( "с привязанной к нему {веревкой}." );
        else
            p( "^На полу лежит {кусок веревки}." );
        end
    end;
    inv = function(s)
        p( "Кусок бельевой веревки длиной около десятка футов." );
    end;
    act = function(s)
        if where(s) == floor_ring then
            p( "Я отвязал веревку от кольца и забрал с собой." );
        else
            p( "Я забрал веревку." );
        end
        take(s);
    end;
    used = function(s,w)
        if w == knife and where(s) == floor_ring then
            p( "Я отрезал веревку от кольца и забрал." );
            take(rope);
        end
    end;
};


path_to_down_room = vroom('Спуститься в дыру', 'room_down');

main = room {
    forcedsc = true;
    nam = 'Down';
    dsc = function(s)
        p("Комната. ");
        if where(rope) == floor_ring then
            p( "К кольцу привязана веревка." );
        end
    end;
    obj = { 
        floor_hole;
        floor_ring;
        rope_hanging;
        knife;
        obj { 
            nam = "path_checker", 
            dsc = function(s)
                if where(rope) == floor_ring then
                    path_to_down_room:enable();
                else
                    path_to_down_room:disable();
                end
                pr("");
            end
        };
    };
    way = {
        path_to_down_room
    }
};

room_down = room {
    forsedsc = true;
    nam = "Подвал";
    dsc = [[Я оказался в подвале.]];
    way = {
        vroom('Обратно', 'main');
    };
};
