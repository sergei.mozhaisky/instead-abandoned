screwdriver = obj {
    nam = "Отвертка";
    dsc = function(s)
        p( "^На полу лежит {отвертка}." );
    end;
    inv = function(s)
        p( "Стандартная плоская отвертка." );
    end;
    act = function(s)
        take(s);
        p( "Я подобрал отвертку с пола." );
    end;
};