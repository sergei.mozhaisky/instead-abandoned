knife = obj {
    nam = "Нож";
    dsc = function(s)
        p( "^На полу лежит {складной нож}." );
    end;
    inv = function(s)
        p( "Хороший складной нож с двумя лезвиями и штопором." );
    end;
    act = function(s)
        take(s);
        p( "Я подобрал нож с пола." );
    end;
};