-- $Name:Addressbook$
instead_version "2.2.0"
require "dash"
require "para"
require "quotes"


start = function()

addressbook_content:disable_all();
addr_C23:enable();
addr_LS1:enable();
addr_LS18:enable();
addr_LS4:enable();
addr_U8:enable();
addr_C11:enable();
addr_D45:enable();
addr_C17:enable();
addr_N28:enable();

addr_N34_enable:enable();

end


adb_loc = function(loc_name, loc_fullname, loc_description)
	local v = {};
	stead.add_var(v, { active      = true });
	stead.add_var(v, { fullname    = loc_fullname });
	stead.add_var(v, { description = loc_description });
	v.nam = loc_name;
	v.dsc = function(s)
		if s.active then
			pn("{" .. s.fullname .. "}");
		else
			pn(txtst(s.fullname));
		end
	end;
	v.act = function(s)
		p(s.description);
		p('-');
		s.active = false;
		move(s, addressbook_visited);
	end;
	return obj(v);
end;

adb_add = function(objref, desc, react_txt)
	local vx = {};
	local desc_used = string.gsub(desc, "{", "")
	desc_used = string.gsub(desc_used, "}", ""); 
	stead.add_var(vx, { active = true });
	stead.add_var(vx, { objref = objref });
	stead.add_var(vx, { desc_a = desc });
	stead.add_var(vx, { desc_u = desc_used });
	stead.add_var(vx, { react  = react_txt });
	vx.nam = '-';
	vx.dsc = function(s)
		if s.active then
			pn(s.desc_a);
		else
			pn(s.desc_u);
		end
	end;
	vx.act = function(s)
		p(s.react);
		s.active = false;
		s.objref:enable();
	end;
	return obj(vx);
end



addr_C11  = adb_loc('C11',  'Аркхемская лечебница имени Святой Марии');
addr_C17  = adb_loc('C17',  'Локсли-Холл');
addr_C19  = adb_loc('С19',  'Мискатоникский университет - Восточное общежитие');
addr_C23  = adb_loc('С23',  'Мискатоникский университет - Библиотека');
addr_C24  = adb_loc('С24',  'Мискатоникский университет - Выставочный зал');
addr_C25  = adb_loc('С25',  'Мискатоникский университет - Администрация');
addr_D9   = adb_loc('D9',   'Ресторан "Пчела"');
addr_D32  = adb_loc('D32',  'Аркхемский полицейский участок');
addr_D45  = adb_loc('D45',  'Герберт Корбетт, криминалист');
addr_D67  = adb_loc('D67',  'Мэтью Кин');
addr_E29  = adb_loc('E29',  'Томас Илсли');
addr_FH12 = adb_loc('FH12', 'Старое кладбище Аркхема');
addr_FH14 = adb_loc('FH14', 'Церковь Бэйфрайарс');
addr_FH16 = adb_loc('FH16', 'Общежитие Рени');
addr_FH27 = adb_loc('FH27', 'Роберт Гледдинг');
addr_FH61 = adb_loc('FH61', 'Лоуренс Хетфилд');
addr_LS1  = adb_loc('LS1',  'Историческое общество');
addr_LS4  = adb_loc('LS4',  'Паскаль Фентон, оккультист');
addr_LS14 = adb_loc('LS14', 'Жилища');
addr_LS18 = adb_loc('LS18', 'Себастиан Лайман');
addr_LS26 = adb_loc('LS26', 'Эдвард Хартвелл');
addr_M9   = adb_loc('M9',   'Непосещаемый остров');
addr_M10  = adb_loc('M10',  'Доки');
addr_M42  = adb_loc('M42',  'Кафе Белл');
addr_M50  = adb_loc('M50',  'Старые книги Мередит');
addr_N24  = adb_loc('N24',  '"Редкие книги и карты"');
addr_N28  = adb_loc('N28',  '"Аркхем Адвертайзер"');
addr_N34  = adb_loc('N34',  'Отель "Банкрофт Армс"');
addr_N38  = adb_loc('N38',  'Вокзал');
addr_U8   = adb_loc('U8',   'Доктор Армитадж');
addr_U41  = adb_loc('U41',  'Emperor Manse');
addr_U67  = adb_loc('U67',  'Парк Аптауна');


addr_N34_enable = adb_add( addr_N34, 'Он остановился в {отеле "Банкрофт армс"}', 'Я отметил на карте отель.');



addressbook_content = obj {
	nam = "addressbook_content";
	dsc = "";
	obj = {
addr_N34_enable,

addr_C11,
addr_C17,
addr_C19,
addr_C23,
addr_C24,
addr_C25,
addr_D9,
addr_D32,
addr_D45,
addr_D67,
addr_E29,
addr_FH12,
addr_FH14,
addr_FH16,
addr_FH27,
addr_FH61,
addr_LS1,
addr_LS4,
addr_LS14,
addr_LS18,
addr_LS26,
addr_M9,
addr_M10,
addr_M42,
addr_M50,
addr_N24,
addr_N28,
addr_N34,
addr_N38,
addr_U8,
addr_U41,
addr_U67

	};
};

addressbook_visited = obj {
	nam = "addressbook_visited";
	dsc = "^";
	obj = {};
};



main = room {
	nam = 'Карта';
	dsc = pr(' ');
	obj = {
		addressbook_content;
		addressbook_visited;
	}
};
