-- $Name:Playground$
instead_version "2.2.0"

main = room {
    nam = "...";
    forcedsc = true;
    dsc = "Playground";
    obj = {
        obj { nam = 'q1', dsc = "* {Паззл с газетой и ключом}^", act = code [[ gamefile('quests/newspaper_and_key.lua', true) ]] };
        obj { nam = 'q2', dsc = "* {Паззл с веревкой}^", act = code [[ gamefile('quests/rope_and_hole.lua', true) ]] };
        obj { nam = 'q3', dsc = "* {Радиоприемник}^", act = code [[ gamefile('quests/radio_receiver.lua', true) ]] };
        obj { nam = 'q31', dsc = "* {Адресная книга 1}^", act = code [[ gamefile('quests/addressbook.lua', true) ]] };
        obj { nam = 'q32', dsc = "* {Адресная книга 2}^", act = code [[ gamefile('quests/addressbook2.lua', true) ]] };
        obj { nam = 'qz', dsc = "* {Генератор имен}^", act = code [[ gamefile('quests/namegen.lua', true) ]] };
    }
}

